<?php

namespace Cms\BlogBundle\Services;

use Doctrine\ORM\EntityManager;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\RouterInterface;

class SitemapListener implements SitemapListenerInterface
{

    private $router;
    private $manager;
    private $container;

    public function __construct(RouterInterface $router, EntityManager $manager)
    {
        $this->router = $router;
        $this->manager = $manager;
    }


    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function populateSitemap(SitemapPopulateEvent $event)
    {

        $section = $event->getSection();
        if (is_null($section) || $section == 'default') {

            $locales = $this->container->getParameter('locales');
            $default_locale = $this->container->getParameter('locale');

            // article
            $posts = $this->manager->getRepository('CmsBlogBundle:Article')->findArticle(array(), null, null, null, false);
            foreach ($posts as $post) {
                foreach ($locales as $locale) {
                    if ($post->getTranslation('published', $locale)  or ($locale == $default_locale and $post->getPublished())) {

                        if($locale == $default_locale) {
                            $articleLink = ($post->getCategory()) ? $post->getCategory()->getSlug().'/'.$post->getSlug() : $post->getSlug();
                        } else {
                            $articleLink = ($post->getCategory()) ? $post->getCategory()->getTranslation('slug', $locale).'/'.$post->getTranslation('slug', $locale) : $post->getTranslation('slug', $locale);
                        }


                        $url = $this->router->generate(
                            'blog_article_show',
                            array(
                                'articleLink' => $articleLink,
                                '_locale' => $locale,
                            ),
                            true
                        );
                        $event->getGenerator()->addUrl(new UrlConcrete($url, new \DateTime(), UrlConcrete::CHANGEFREQ_HOURLY, 1), 'blog_article_'.$locale);
                    }
                }

            }

            // tags
            $tags = $this->manager->getRepository('CmsBlogBundle:Tag')->findAll();
            foreach ($tags as $tag) {
                foreach ($locales as $locale) {

                    $url = $this->router->generate(
                        'cms_blog_list_tag',
                        array(
                            'tagSlug' => ($locale == $default_locale) ? $tag->getSlug() : $tag->getTranslation('slug', $locale),
                            'tagId' => $tag->getId(),
                            '_locale' => $locale
                        ),
                        true
                    );
                    $event->getGenerator()->addUrl(new UrlConcrete($url, new \DateTime(), UrlConcrete::CHANGEFREQ_HOURLY, 1), 'blog_tag');
                }
            }

            // categories
            $categories = $this->manager->getRepository('CmsBlogBundle:Category')->findAll();
            foreach ($categories as $category) {
                foreach ($locales as $locale) {
                    $url = $this->router->generate(
                        'cms_blog_list_category',
                        array(
                            'categorySlug' => ($locale == $default_locale) ? $category->getSlug() :  $category->getTranslation('slug', $locale),
                            'categoryId' => $category->getId(),
                            '_locale' => $locale,
                        ),
                        true
                    );
                    $event->getGenerator()->addUrl(new UrlConcrete($url, new \DateTime(), UrlConcrete::CHANGEFREQ_HOURLY, 1), 'blog_category');
                }
            }

        }
    }


}
