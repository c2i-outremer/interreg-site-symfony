<?php

namespace Extra\ServiceBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Field
 *
 * @ORM\Table("extra_service_field")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="Extra\ServiceBundle\Entity\FieldTranslation")
 */
class Field extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    protected $value;

    /**
     * @ORM\OneToMany(targetEntity="Extra\ServiceBundle\Entity\FieldTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @ORM\ManyToOne(targetEntity="Extra\ServiceBundle\Entity\Service", inversedBy="fields")
     * @ORM\JoinColumn(name="service", referencedColumnName="id")
     */
    protected $service;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Field
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Field
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Remove translation
     *
     * @param \Extra\ServiceBundle\Entity\FieldTranslation $translation
     */
    public function removeTranslation(\Extra\ServiceBundle\Entity\FieldTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Set service
     *
     * @param \Extra\ServiceBundle\Entity\Service $service
     *
     * @return Field
     */
    public function setService(\Extra\ServiceBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \Extra\ServiceBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }
}
