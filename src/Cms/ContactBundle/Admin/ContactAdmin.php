<?php

namespace Cms\ContactBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ContactAdmin extends Admin
{

	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'position',
	);

	/**
	 * @param DatagridMapper $datagridMapper
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
            ->add('created', null, array('label' => 'Date d\'envoi'))
            ->add('firstName', null, array('label' => 'Prénom'))
            ->add('lastName', null, array('label' => 'Nom'))
            ->add('email', null, array('label' => 'Email'))
            ->add('subject', null, array('label' => 'Sujet'))
            ->add('optIn', null, array('label' => 'Newsletter'))
            ->add('message', null, array('label' => 'Message'))
            ->add('published', null, array('label' => 'Publication'))
		;
	}

	/**
	 * @param ListMapper $listMapper
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
				->add('published', null, array('label' => 'Publication', 'editable' => true))
				->add('created', null, array('label' => 'Date d\'envoi'))
				->add('firstName', null, array('label' => 'Prénom'))
				->add('lastName', null, array('label' => 'Nom'))
				->add('email', null, array('label' => 'Email'))
				->add('subject', null, array('label' => 'Sujet'))
				->add('optIn', null, array('label' => 'Newsletter'))
				->add('message', null, array('label' => 'Message'))

		;
	}

	/**
	 * @param FormMapper $formMapper
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
				->add('id')
				->add('firstName')
				->add('lastName')
				->add('email')
				->add('message')
				->add('created')
				->add('updated')
				->add('position')
				->add('published')
				->add('name')
				->add('slug')
				->add('optIn')

		;
	}

	/**
	 * @param ShowMapper $showMapper
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
				->add('id')
				->add('created')
				->add('firstName')
				->add('lastName')
				->add('email')
				->add('published')
				->add('optIn')
				->add('message')

		;
	}

	protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
	{
		parent::configureRoutes($collection);
		$collection->add('publish', $this->getRouterIdParameter() . '/publish');
		$collection->remove('create');
	}

	public function getBatchActions()
	{
		$actions = parent::getBatchActions();
		$actions['publish'] = array('label' => 'Publier', 'ask_confirmation' => false);
		return $actions;
	}

}
