<?php

namespace Extra\ServiceBundle\Services;

use Doctrine\ORM\EntityManager;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Symfony\Component\Routing\RouterInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;

class SitemapListener implements SitemapListenerInterface
{

    private $router;
    private $manager;
    private $container;

    public function __construct(RouterInterface $router, EntityManager $manager)
    {
        $this->router = $router;
        $this->manager = $manager;
    }


    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function populateSitemap(SitemapPopulateEvent $event)
    {

        $section = $event->getSection();
        if (is_null($section) || $section == 'default') {

            $locales = $this->container->getParameter('locales');
            $default_locale = $this->container->getParameter('locale');

            $services = $this->manager->getRepository('ExtraServiceBundle:Service')->findAll();

            foreach ($services as $service) {
                foreach ($locales as $locale) {
                    if ($service->getTranslation('published', $locale) or ($locale == $default_locale and $service->getPublished())) {
                        $serviceSlug = ($locale == $default_locale) ? $service->getSlug() : $service->getTranslation('slug', $locale);
                        $url = $this->router->generate('extra_service_single', array('slug' => $serviceSlug, '_locale' => $locale), true);
                        $event->getGenerator()->addUrl(new UrlConcrete($url, new \DateTime(), UrlConcrete::CHANGEFREQ_HOURLY, 1), 'service');
                    }
                }
            }
        }
    }


}
