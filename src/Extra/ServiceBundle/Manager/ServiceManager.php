<?php

namespace Extra\ServiceBundle\Manager;

use Cms\CoreBundle\Entity\Visite;
use Cms\CoreBundle\Manager\BaseManager;


class ServiceManager extends BaseManager
{

    public function getList($_locale)
    {
        return $this->repo->getServices($_locale);
    }

    public function find($slug, $_locale)
    {
        return $this->repo->findService($slug, $_locale);
    }

    public function updateViste($service)
    {
        $service->setNumberViews($service->getNumberViews() + 1);
        $this->persistAndFlush($service);
        $visite = new Visite('service', $service->getSlug(), $service->getId());
        $this->persistAndFlush($visite);
    }

}