<?php

namespace Extra\HouseBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\PublishTranslatable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * House
 *
 * @ORM\Table("extra_house_house")
 * @ORM\Entity(repositoryClass="Extra\HouseBundle\Repository\HouseRepository")
 * @Gedmo\TranslationEntity(class="Extra\HouseBundle\Entity\HouseTranslation")
 */
class House extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use NameTranslatable;
    use PublishTranslatable;
    use Timestampable;
    use Positionable;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\HouseTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var integer
     * @Gedmo\Translatable
     * @ORM\Column(name="numberView", type="integer")
     */
    private $numberView;


    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var boolean
     * @ORM\Column(name="top", type="boolean", nullable=true)
     */
    private $top;

    /**
     * @var boolean
     * @ORM\Column(name="allow_comment", type="boolean", nullable=true)
     */
    private $allowComment;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="cover", referencedColumnName="id", nullable=true)
     */
    protected $cover;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\Content", mappedBy="house", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $contents;

    /**
     * @ORM\ManyToMany(targetEntity="Extra\HouseBundle\Entity\Equipment")
     * @ORM\JoinTable(name="extra_house_house_equipments",
     *      joinColumns={@ORM\JoinColumn(name="house", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="equipment", referencedColumnName="id")}
     *      )
     * */

    protected $equipments;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\Picture", mappedBy="house", cascade={"persist"}, orphanRemoval=true)
     */
    protected $pictures;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\Video", mappedBy="house", cascade={"persist"}, orphanRemoval=true)
     */
    protected $videos;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\File", mappedBy="house", cascade={"persist"}, orphanRemoval=true)
     */
    protected $files;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\Price", mappedBy="house", cascade={"persist"}, orphanRemoval=true)
     */
    protected $prices;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\Reservation", mappedBy="house", cascade={"persist", "remove"})
     */
    protected $reservations;

    public function __construct() {
        parent::__construct();
        $this->contents = new ArrayCollection();
        $this->equipments = new ArrayCollection();
        $this->pictures = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->reservations = new ArrayCollection();
        $this->allowComment = true;
        $this->numberView = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numberView
     *
     * @param integer $numberView
     *
     * @return House
     */
    public function setNumberView($numberView)
    {
        $this->numberView = $numberView;

        return $this;
    }

    /**
     * Get numberView
     *
     * @return integer
     */
    public function getNumberView()
    {
        return $this->numberView;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return House
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }




    /**
     * Set cover
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $cover
     *
     * @return House
     */
    public function setCover(\Application\Sonata\MediaBundle\Entity\Media $cover = null)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getCover()
    {
        return $this->cover;
    }



    /**
     * Add content
     *
     * @param \Extra\HouseBundle\Entity\Content $content
     *
     * @return House
     */
    public function addContent(\Extra\HouseBundle\Entity\Content $content)
    {
        $this->contents[] = $content;
        $content->setHouse($this);
        return $this;
    }

    /**
     * Remove content
     *
     * @param \Extra\HouseBundle\Entity\Content $content
     */
    public function removeContent(\Extra\HouseBundle\Entity\Content $content)
    {
        $this->contents->removeElement($content);
    }

    /**
     * Get contents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * Remove translation
     *
     * @param \Extra\HouseBundle\Entity\HouseTranslation $translation
     */
    public function removeTranslation(\Extra\HouseBundle\Entity\HouseTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Add equipment
     *
     * @param \Extra\HouseBundle\Entity\Equipment $equipment
     *
     * @return House
     */
    public function addEquipment(\Extra\HouseBundle\Entity\Equipment $equipment)
    {
        $this->equipments[] = $equipment;
        return $this;
    }

    /**
     * Remove equipment
     *
     * @param \Extra\HouseBundle\Entity\Equipment $equipment
     */
    public function removeEquipment(\Extra\HouseBundle\Entity\Equipment $equipment)
    {
        $this->equipments->removeElement($equipment);
    }

    /**
     * Get equipments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * Add picture
     *
     * @param \Extra\HouseBundle\Entity\Picture $picture
     *
     * @return House
     */
    public function addPicture(\Extra\HouseBundle\Entity\Picture $picture)
    {
        $this->pictures[] = $picture;
        $picture->setHouse($this);
        return $this;
    }

    /**
     * Remove picture
     *
     * @param \Extra\HouseBundle\Entity\Picture $picture
     */
    public function removePicture(\Extra\HouseBundle\Entity\Picture $picture)
    {
        $this->pictures->removeElement($picture);
    }

    /**
     * Get pictures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * Add video
     *
     * @param \Extra\HouseBundle\Entity\Video $video
     *
     * @return House
     */
    public function addVideo(\Extra\HouseBundle\Entity\Video $video)
    {
        $this->videos[] = $video;
        $video->setHouse($this);
        return $this;
    }

    /**
     * Remove video
     *
     * @param \Extra\HouseBundle\Entity\Video $video
     */
    public function removeVideo(\Extra\HouseBundle\Entity\Video $video)
    {
        $this->videos->removeElement($video);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Add file
     *
     * @param \Extra\HouseBundle\Entity\File $file
     *
     * @return House
     */
    public function addFile(\Extra\HouseBundle\Entity\File $file)
    {
        $this->files[] = $file;
        $file->setHouse($this);
        return $this;
    }

    /**
     * Remove file
     *
     * @param \Extra\HouseBundle\Entity\File $file
     */
    public function removeFile(\Extra\HouseBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set top
     *
     * @param boolean $top
     *
     * @return House
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Get top
     *
     * @return boolean
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * Add price
     *
     * @param \Extra\HouseBundle\Entity\Price $price
     *
     * @return House
     */
    public function addPrice(\Extra\HouseBundle\Entity\Price $price)
    {
        $this->prices[] = $price;
        $price->setHouse($this);
        return $this;
    }

    /**
     * Remove price
     *
     * @param \Extra\HouseBundle\Entity\Price $price
     */
    public function removePrice(\Extra\HouseBundle\Entity\Price $price)
    {
        $this->prices->removeElement($price);
    }

    /**
     * Get prices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Set allowComment
     *
     * @param boolean $allowComment
     *
     * @return House
     */
    public function setAllowComment($allowComment)
    {
        $this->allowComment = $allowComment;

        return $this;
    }

    /**
     * Get allowComment
     *
     * @return boolean
     */
    public function getAllowComment()
    {
        return $this->allowComment;
    }

    /**
     * Add reservation
     *
     * @param \Extra\HouseBundle\Entity\Reservation $reservation
     *
     * @return House
     */
    public function addReservation(\Extra\HouseBundle\Entity\Reservation $reservation)
    {
        $this->reservations[] = $reservation;
        $reservation->setHouse($this);
        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \Extra\HouseBundle\Entity\Reservation $reservation
     */
    public function removeReservation(\Extra\HouseBundle\Entity\Reservation $reservation)
    {
        $this->reservations->removeElement($reservation);
    }

    /**
     * Get reservations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservations()
    {
        return $this->reservations;
    }
}
