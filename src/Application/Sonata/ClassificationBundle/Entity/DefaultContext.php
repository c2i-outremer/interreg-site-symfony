<?php

namespace Application\Sonata\ClassificationBundle\Entity;

use Sonata\ClassificationBundle\Model\Context as SonataContext;

class DefaultContext extends SonataContext
{
	public function getId()
	{
		return \Sonata\ClassificationBundle\Model\ContextInterface::DEFAULT_CONTEXT;
	}

}
