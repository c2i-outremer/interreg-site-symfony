<?php


namespace Cms\NavBundle\Services\Twig;


use Cms\NavBundle\Menu\Provider;
use Doctrine\ORM\EntityManager;

class ShowMenu extends \Twig_Extension
{
    protected $manager;
    protected $twig;
    protected $provider;

    function __construct(EntityManager $manager, \Twig_Environment $twig, Provider $provider)
    {
        $this->manager = $manager;
        $this->twig = $twig;
        $this->provider = $provider;
    }


    public function getName()
    {
        return 'showMenu';
    }

    public function getFunctions()
    {
        return array(
            'showMenu' => new \Twig_Function_Method($this, 'showMenu')
        );
    }

    public function showMenu($slug) {


        return $this->twig->render('CmsNavBundle:Type:knp.html.twig', array(
            'menu' => $this->manager->getRepository('CmsNavBundle:Menu')->getMenuAndType($slug)
        ));

    }
}