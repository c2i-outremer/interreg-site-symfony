<?php

namespace Cms\NavBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Cms\NavBundle\Entity\Menu;

class AdminController extends Controller
{
    //------------------------------------------------------------------------------------------------------------------
    //	getLinksAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     *  get all links from a menu and generate ul-li
     * @param \Cms\NavBundle\Entity\Menu $menu
     * @return type
     */
    public function getLinksAction(Menu $menu)
    {

        $saveThis = $this;
        $em = $this->getDoctrine()->getManager();

        $linkRepo = $em->getRepository('CmsNavBundle:Link');
        $rootLink = $linkRepo->find($menu->getRootLink());

        //  inject data in closure
        $dataNodes = $this->get('cms_nav.BuildMenu')->getDataNodes($rootLink, $menu);

        $nodes = $linkRepo->childrenHierarchy($rootLink, false, array('decorate' => true, 'html' => true, 'representationField' => 'name',
            'rootOpen' =>   function($tree) {
                return (count($tree) && ($tree[0]['lvl'] == 1))
                    ? '<ol class="sortable">'
                    : '<ol>';
            },
            'rootClose' => '</ol>',
            'childOpen' => function($node)  {
                return ($node['lvl'] == 1)
                    ? '<li class="mjs-nestedSortable-branch mjs-nestedSortable-expanded mjs-nestedSortable-leaf" id="menuItem_' . $node['id'] . '">'
                    : '<li class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_' . $node['id'] . '" style="display: list-item;">';
            },
            'childClose' => '</li>',
            'nodeDecorator' => function($node) use ($dataNodes, $saveThis) {
                if ($node)
                {
                    $urlEditLink = $saveThis->generateUrl('sonata_admin_set_object_field_value', array(
                        'context' => 'list',
                        'field' => 'name',
                        'objectId' => $dataNodes[$node['id']]['id'],
                        'code' => 'cms_nav.admin.link',
                    ));

                    $return = '<div class="" data-id="' . $dataNodes[$node['id']]['id'] . '">';
                    $return .= '<span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick"><span></span></span>';
                    $return .= '<span class="x-editable" data-type="text" data-value="' . $dataNodes[$node['id']]['name'] . '" data-title="Name" data-pk="' . $dataNodes[$node['id']]['id'] . '" data-url="' . $urlEditLink . '">' . $dataNodes[$node['id']]['name'] . '</span>';
                    $return .= '<a class="btn btn-danger btn-edit-link" target="_blank" href="' . $dataNodes[$node['id']]['linkEdit'] . '">Editer Le lien</a>';

                    if ($dataNodes[$node['id']]['hasPage'])
                    {
                        $return .= '<a class="btn btn-success btn-edit-link" target="_blank" href="' . $dataNodes[$node['id']]['pageEdit'] . '">Editer La page</a>';
                    }

                    $return .= '</div>';
                    return $return;
                }
            },

        ));

        //<a  href="{{ path('admin_cms_page_page_edit', {'id' : node.page.id, 'menu': menu}) }}" ></a>

        return $this->render('CmsNavBundle:Admin:children_hierarchy.html.twig', array(
            'rootLink' => $rootLink,
            'nodes' => $nodes,
            'menu' => $menu,
        ));
    }

    //------------------------------------------------------------------------------------------------------------------
    //	createPageAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     *  create a new page
     * @param type $menu
     * @return type
     */
    public function createPageAction($menu)
    {
        $em = $this->getDoctrine()->getManager();

        $menuObject = $em->getRepository('CmsNavBundle:Menu')->find($menu);

        $link = new \Cms\NavBundle\Entity\Link;
        $link
            ->setName('Page')
            ->setParent($menuObject->getRootLink())
            ->setLinkType('PAGE')
            ->setRootLink(false)
        ;

        $this->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);

        $page = new \Cms\PageBundle\Entity\Page;
        $page
            ->setName('Page')
            ->setMetaTitle('Page')
            ->setMetaH1('Page')
            ->setPageModel('BUILDER')
        ;

        $link->setPage($page);

        $em->persist($link);
        $em->persist($page);

        $em->flush();

        return $this->redirect($this->generateUrl('admin_cms_nav_menu_edit', array('id' => $menu)));
    }

    //------------------------------------------------------------------------------------------------------------------
    //	createLinkAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     *  create a new link
     * @param type $menu
     * @return type
     */
    public function createLinkAction($menu)
    {
        $em = $this->getDoctrine()->getManager();

        $menuObject = $em->getRepository('CmsNavBundle:Menu')->find($menu);

        $link = new \Cms\NavBundle\Entity\Link;
        $link
            ->setName('Lien Libre')
            ->setLink(null)
            ->setParent($menuObject->getRootLink())
            ->setLinkType('LINK')
            ->setRootLink(false)
        ;

        $em->persist($link);

        $em->flush();

        return $this->redirect($this->generateUrl('admin_cms_nav_menu_edit', array('id' => $menu)));
    }

    //------------------------------------------------------------------------------------------------------------------
    //	updateOrderLinkAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     *  AJAX => Order link after drag & drop
     * @param Request $request
     * @return Response
     */
    public function updateOrderLinkAction(Request $request)
    {
        if ($request->isXmlHttpRequest())
        {
            $em = $this->getDoctrine()->getManager();
            $linkRepo = $em->getRepository('CmsNavBundle:Link');

            $order = $this->getRequest()->request->get('order');


            $menuObject = array();
            foreach ($order as $key => $link)
            {
                $menuObject[$link["item_id"]] = $linkRepo->find((int) $link['item_id']);
            }

            foreach ($order as $key => $link)
            {
                if ($key != 0)
                {
                    $menuItem = $menuObject[$link["item_id"]];
                    $menuItem->setLvl($link["depth"]);
                    $menuItem->setLft($link["left"]);
                    $menuItem->setRgt($link["right"]);
                    if ($link["parent_id"] && $link["parent_id"] != 'none')
                    {
                        $menuItem->setParent($menuObject[$link["parent_id"]]);
                    }
                    else
                    {
                        $menuItem->setParent();
                    }
                }

                $em->flush();
            }
        }
        return new Response('Finish');
    }

    //------------------------------------------------------------------------------------------------------------------
    //	updatePageNameAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     *  AJAX => Update Name with edit-inline
     * @param Request $request
     * @return Response
     */
    public function updatePageNameAction(Request $request)
    {
        if ($request->isXmlHttpRequest())
        {
            $em = $this->getDoctrine()->getManager();

            $id = (int) str_replace('edit-message-', '', $this->getRequest()->request->get('id'));
            $value = $this->getRequest()->request->get('value');

            $link = $em->getRepository('CmsNavBundle:Link')->find($id);
            $link->setName($value);

            $em->flush();
            return new Response($value);
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    //	deleteLinkAction
    //------------------------------------------------------------------------------------------------------------------

    public function deleteLinkAction($linkId, $menuId)
    {
        $em = $this->getDoctrine()->getManager();
        $link = $em->getRepository('CmsNavBundle:Link')->find((int) $linkId);

        $em->remove($link);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_cms_nav_menu_edit', array('id' => (int) $menuId)));
    }

}