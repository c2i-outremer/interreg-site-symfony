<?php

namespace Cms\BlogBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lieu
 *
 * @ORM\Table("cms_blog_lieu")
 * @ORM\Entity(repositoryClass="Cms\BlogBundle\Entity\LieuRepository")
 * @Gedmo\TranslationEntity(class="Cms\BlogBundle\Entity\LieuTranslation")
 */
class Lieu extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use NameTranslatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\LieuTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Remove translation
     *
     * @param \Cms\BlogBundle\Entity\LieuTranslation $translation
     */
    public function removeTranslation(\Cms\BlogBundle\Entity\LieuTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }
}
