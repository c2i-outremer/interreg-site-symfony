<?php

namespace Extra\HouseBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Equipment
 *
 * @ORM\Table("extra_house_equipment")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="Extra\HouseBundle\Entity\EquipmentTranslation")
 */
class Equipment extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use NameTranslatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\EquipmentTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

