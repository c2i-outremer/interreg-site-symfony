<?php

namespace Cms\BlogBundle\Services;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cms\BlogBundle\Entity\Article;

class AutoSetMetaDescription
{

	protected $container;

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function postPersist(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();

		if (!$entity instanceof Article)
		{
			return;
		}
		
		$this->convertString($entity);
	}

	public function postUpdate(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();

		if (!$entity instanceof Article)
		{
			return;
		}

		$this->convertString($entity);
	}

	protected function convertString($entity)
	{
		$entity->setMetaDescription(html_entity_decode(strip_tags($entity->getShortContent(), ENT_QUOTES)));
		$em = $this->container->get('doctrine')->getManager();
		$em->persist($entity);
		$em->flush();
	}

}
