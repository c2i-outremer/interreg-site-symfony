<?php

namespace Cms\PageBundle\Services;

use Doctrine\ORM\EntityManager;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\Routing\RouterInterface;

class SitemapListener implements SitemapListenerInterface
{

    private $router;
    private $manager;
    private $container;

    public function __construct(RouterInterface $router, EntityManager $manager, $container)
    {
        $this->router = $router;
        $this->manager = $manager;
        $this->container = $container;
    }


    public function populateSitemap(SitemapPopulateEvent $event)
    {
        $section = $event->getSection();
        if (is_null($section) || $section == 'default') {

            $locales = $this->container->getParameter('locales');
            $default_locale = $this->container->getParameter('locale');
            $baseUrl = $this->container->getParameter('server.url');

            $url = $this->router->generate('page_homepage', array(), true);
            $event->getGenerator()->addUrl(new UrlConcrete($url, new \DateTime(), UrlConcrete::CHANGEFREQ_HOURLY, 1), 'page');

            $links = $this->manager->getRepository('CmsNavBundle:Link')->findAll();
            foreach ($links as $link) {
                if (!$link->getRootLink() and $link->getLink() != '/') {

                    foreach ($locales as $locale) {
                        if ($link->getTranslation('published', $locale) or ($locale == $default_locale and $link->getPublished())) {

                            $url = false;

                            if ($link->getLinkType() == 'PAGE') { // PAGE
                                $page = $link->getPage();

                                if ($locale == $default_locale) {
                                    $pageLink = ($page->getFolder()) ?
                                        $page->getFolder()->getSlug().'/'.$page->getSlug() :
                                        $page->getSlug();
                                } else {
                                    $pageLink = ($page->getFolder()) ?
                                        $page->getFolder()->getTranslation('slug', $locale).'/'.$page->getTranslation('slug', $locale) :
                                        $page->getTranslation('slug', $locale);
                                }

                                $url = $this->router->generate('page_show', array('pageLink' => $pageLink, '_locale' => $locale), true);
                            }

                            if ($link->getLinkType() == 'LINK') { // LINK
                                if (!$link->getExternal()) {
                                    $url = $baseUrl.'/'.$link->getLink();
                                }
                            }

                            if ($link->getLinkType() == 'ROUTE') { // ROUTE
                                $url = $this->router->generate($link->getRouteName(), $link->getDataArray($link->getRouteArgs()), true);
                            }


                            $event->getGenerator()->addUrl(new UrlConcrete($url, new \DateTime(), UrlConcrete::CHANGEFREQ_HOURLY, 1), 'page');


                        }

                    }
                }
            }
        }
    }


}
