<?php

namespace Cms\GroupBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class GroupAdmin extends Admin
{

	/**
	 * @param DatagridMapper $datagridMapper
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
				->add('name')
				->add('slug')
		;
	}

	/**
	 * @param ListMapper $listMapper
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
				->add('published')
				->add('name')
				->add('slug')
				->add('groupType')
				->add('_action', 'actions', array(
					'actions' => array(
						'show' => array(),
						'edit' => array(),
						'delete' => array(),
					)
				))
		;
	}

	/**
	 * @param FormMapper $formMapper
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
				->add('published')
				->add('name', null, array('label' => 'Nom', 'attr' => array('autofocus' => 'autofocus')))
				->add('slug')
				->add('groupType')
				->add('items', 'sonata_type_collection', array('label' => 'Elements du groupe',	'required' => false,'by_reference' => false), array('edit' => 'inline','inline' => 'table','sortable' => 'position',))
		;
	}

	/**
	 * @param ShowMapper $showMapper
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
				->add('published')
				->add('name')
				->add('slug')
		;
	}

}
