$(function () {
//------------------------------------------------------------------------------------------------------------------
//	lazyload
//------------------------------------------------------------------------------------------------------------------
    $("img.lazy").lazyload({
        effect: "fadeIn"
    });
//------------------------------------------------------------------------------------------------------------------
//	blog-show-more
//------------------------------------------------------------------------------------------------------------------
    $('.blog').on('click', '.blog-show-more', function () {
        var saveThis = $(this);

        var data = {
            bloc: saveThis.closest('.block').data('block'),
            nbrPosts: saveThis.closest('.blog').find('.show-post').length,
            lang: $('html').attr('lang')
        };

        saveThis.closest('.blog').find('.ajax-loader').show();
        $.ajax({
            type: 'POST',
            url: Routing.generate('cms_blog_show_more') + '/' + data.lang,
            data: data,
            dataType: 'json'
        }).done(function (json) {
            saveThis.closest('.blog').find('.ajax-loader').hide();
            saveThis.closest('.blog').find('.posts').append(json['content']);
            if (json['stopShowMore']) {
                saveThis.html(json['stopShowMoreText']);
                saveThis.removeClass('blog-show-more');
            }
        });
    });

//------------------------------------------------------------------------------------------------------------------
//	blog-tag-show-more
//------------------------------------------------------------------------------------------------------------------
    $('#tags').on('click', '.blog-show-more', function () {
        var saveThis = $(this);
        var data = {
            nbrPosts: saveThis.closest('#tags').find('.show-post').length,
            tag: saveThis.closest('#tags').data('tag'),
            lang: $('html').attr('lang')
        };
        saveThis.closest('#tags').find('.ajax-loader').show();
        $.ajax({
            type: 'POST',
            url: Routing.generate('cms_blog_tag_show_more') + '/' + data.lang,
            data: data,
            dataType: 'json'
        }).done(function (json) {
            saveThis.closest('#tags').find('.ajax-loader').hide();
            $('#tags .list-tags').append(json['content']);
            if (json['stopShowMore']) {
                saveThis.html(json['stopShowMoreText']);
                saveThis.removeClass('blog-tag-show-more');
            }
        });
    });

//------------------------------------------------------------------------------------------------------------------
//	blog-category-show-more
//------------------------------------------------------------------------------------------------------------------
    $('#categories').on('click', '.blog-show-more', function () {
        var saveThis = $(this);
        var data = {
            nbrPosts: saveThis.closest('#categories').find('.show-post').length,
            category: saveThis.closest('#categories').data('category'),
            lang: $('html').attr('lang')
        };
        saveThis.closest('#categories').find('.ajax-loader').show();
        $.ajax({
            type: 'POST',
            url: Routing.generate('cms_blog_category_show_more') + '/' + data.lang,
            data: data,
            dataType: 'json'
        }).done(function (json) {
            saveThis.closest('#categories').find('.ajax-loader').hide();
            $('#categories .list-categories').append(json['content']);
            if (json['stopShowMore']) {
                saveThis.html(json['stopShowMoreText']);
                saveThis.removeClass('blog-category-show-more');
            }
        });
    });


//------------------------------------------------------------------------------------------------------------------
//	same-category equalheight
//------------------------------------------------------------------------------------------------------------------

    $('.same-category').equalHeights();

//------------------------------------------------------------------------------------------------------------------
//	fancybox
//------------------------------------------------------------------------------------------------------------------
    $(".fancybox").fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: false,
        helpers: {
            title: {type: 'inside'},
            buttons: {},
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });

    $('.fancybox-media').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });

});
