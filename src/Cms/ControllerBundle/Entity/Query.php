<?php

namespace Cms\ControllerBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Doctrine\ORM\Mapping as ORM;

/**
 * query
 *
 * @ORM\Table("cms_controller_query")
 * @ORM\Entity(repositoryClass="Cms\ControllerBundle\Entity\QueryRepository")
 */
class Query
{
    use Nameable;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToMany(targetEntity="Cms\CoreBundle\Entity\Attribute", cascade={"persist", "remove"})
	 * @ORM\JoinTable(name="cms_controller_query_argument",
	 * 	joinColumns={@ORM\JoinColumn(name="query", referencedColumnName="id", onDelete="CASCADE")},
	 * 	inverseJoinColumns={@ORM\JoinColumn(name="argument", referencedColumnName="id", onDelete="CASCADE")})
	 */
	protected $arguments;


	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->blocs = new \Doctrine\Common\Collections\ArrayCollection();
		$this->arguments = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * Add arguments
	 *
	 * @param \Cms\CoreBundle\Entity\Attribute $arguments
	 * @return Query
	 */
	public function addArgument(\Cms\CoreBundle\Entity\Attribute $arguments)
	{
		$this->arguments[] = $arguments;
		return $this;
	}

	/**
	 * Remove arguments
	 *
	 * @param \Cms\CoreBundle\Entity\Attribute $arguments
	 */
	public function removeArgument(\Cms\CoreBundle\Entity\Attribute $arguments)
	{
		$this->arguments->removeElement($arguments);
	}

	/**
	 * Get arguments
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getArguments()
	{
		return $this->arguments;
	}

}
