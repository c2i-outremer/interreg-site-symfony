<?php

namespace CmS\BlogBundle\Manager;

use Cms\BlogBundle\Entity\Inscription;
use Cms\CoreBundle\Manager\BaseManager;

class InscriptionManager extends BaseManager
{
    /**
     * @param Inscription $inscription
     */
    public function save($inscription)
    {
        $this->persistAndFlush($inscription);
    }

}