<?php

namespace Cms\BlocBundle\Manager;

use Cms\CoreBundle\Manager\BaseManager;

class BlocManager extends BaseManager
{

    public function getBlocs(array $params, $findBySlug = true)
    {

        $r = array();

        $blocs = $this->repo->getBlocs($params, $findBySlug);

        if ($findBySlug) {
            foreach ($blocs as $bloc) {
                $r[$bloc->getSlug()] = $bloc;
            }
        } else {
            foreach ($blocs as $bloc) {
                $r[$bloc->getId()] = $bloc;
            }
        }


        return $r;


    }

}