<?php

namespace Cms\ContactBundle\Services\Twig;

use Doctrine\ORM\EntityManager;

class CountPublishNewsletter extends \Twig_Extension
{

	public $manager;

	public function __construct(EntityManager $manager)
	{
		$this->manager = $manager;
	}

	public function getName()
	{
		return 'CountPublishNewsletter';
	}

	public function getFunctions()
	{
		return array(
			'CountPublishNewsletter' => new \Twig_Function_Method($this, 'CountPublishNewsletter'),
		);
	}
	
	public function CountPublishNewsletter()
	{
		return count($this->manager->getRepository('CmsContactBundle:Newsletter')->findBy(array('published' => true)));
	}

}
