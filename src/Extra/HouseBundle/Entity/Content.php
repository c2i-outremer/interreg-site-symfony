<?php

namespace Extra\HouseBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Content
 *
 * @ORM\Table("extra_house_content")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="Extra\HouseBundle\Entity\ContentTranslation")
 */
class Content extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use NameTranslatable;
    use Positionable;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\ContentTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;


    /**
     * @ORM\ManyToOne(targetEntity="Extra\HouseBundle\Entity\House", inversedBy="contents")
     * @ORM\JoinColumn(name="house", referencedColumnName="id", nullable=true)
     */
    private $house;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Content
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set house
     *
     * @param \Extra\HouseBundle\Entity\House $house
     *
     * @return Content
     */
    public function setHouse(\Extra\HouseBundle\Entity\House $house = null)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return \Extra\HouseBundle\Entity\House
     */
    public function getHouse()
    {
        return $this->house;
    }
}
