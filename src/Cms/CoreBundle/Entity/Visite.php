<?php

namespace Cms\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Visite
 *
 * @ORM\Table("cms_core_visite")
 * @ORM\Entity(repositoryClass="Cms\CoreBundle\Entity\VisiteRepository")
 */
class Visite
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var \DateTime
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(name="created", type="datetime", nullable=true)
	 */
	protected $created;

	/**
	 * @var string
	 * @ORM\Column(name="data_type", type="string", length=255)
	 */
	protected $dataType;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	protected $name;

	/** 	 
	 *  @var type 
	 *  @ORM\Column(name="type_id", type="integer")
	 */
	protected $typeId;

	public function __construct($dataType, $name, $id)
	{
		$this->dataType = $dataType;
		$this->name = $name;
		$this->typeId = $id;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	public function getDataType()
	{
		return $this->dataType;
	}

	public function setDataType($dataType)
	{
		$this->dataType = $dataType;
		return $this;
	}

	function getCreated()
	{
		return $this->created;
	}

	function getName()
	{
		return $this->name;
	}

	function setCreated(\DateTime $created)
	{
		$this->created = $created;
		return $this;
	}

	function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	function getTypeId()
	{
		return $this->typeId;
	}

	function setTypeId($typeId)
	{
		$this->typeId = $typeId;
		return $this;
	}

}
