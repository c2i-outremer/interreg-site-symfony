<?php

namespace Cms\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Cms\BlogBundle\Entity\ArticleType;
use Cms\BlogBundle\Entity\BlogType;

class LoadBlogType implements FixtureInterface
{

	public function load(ObjectManager $manager)
	{
		$article = array(
			'name' => 'Defaut',
			'templatePath' => ':Blog:Single/default.html.twig',
		);

		$blog = array(
			'name' => 'Blog par defaut',
			'templatePath' => ':Blog:Blog/default.html.twig',
		);

		$articleType = new ArticleType;
		$articleType->setName($article['name']);
		$articleType->setTemplatePath($article['templatePath']);
		$manager->persist($articleType);
		
		$blogType = new BlogType;
		$blogType->setName($blog['name']);
		$blogType->setTemplatePath($blog['templatePath']);
		$manager->persist($blogType);

		$manager->flush();
	}

}
