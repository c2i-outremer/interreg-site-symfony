<?php

namespace Application\Sonata\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

	public function getByGroup($group)
	{
		$qb = $this
				->createQueryBuilder('user')
				->leftJoin('user.groups', 'groups')->addSelect('groups')
				->andWhere('groups.name = :groups')->setParameter('groups', $group)
				->orWhere('user.roles LIKE :role')->setParameter('role', '%ROLE_SUPER_ADMIN%');
		;

		return $qb
						->getQuery()
						->getResult()
		;
	}

}
