<?php

namespace Cms\PageBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\Publishable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;


/**
 * Folder
 *
 * @ORM\Table("cms_page_folder")
 * @ORM\Entity(repositoryClass="Cms\PageBundle\Entity\FolderRepository")
 * @Gedmo\TranslationEntity(class="Cms\PageBundle\Entity\FolderTranslation")
 */
class Folder extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use Timestampable;
    use NameTranslatable;
    use Publishable;
    use Positionable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\PageBundle\Entity\FolderTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\PageBundle\Entity\Page", mappedBy="folder")
	 * */
	private $pages;

	function __construct()
	{
        parent::__construct();
		$this->pages = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Add page
	 *
	 * @param \Cms\PageBundle\Entity\Page $page
	 * @return Page
	 */
	public function addPage(\Cms\PageBundle\Entity\Page $page)
	{
		$this->pages[] = $page;
		$page->setFolder($this);
		return $this;
	}

	/**
	 * Remove page
	 *
	 * @param \Cms\PageBundle\Entity\Page $page
	 */
	public function removePage(\Cms\PageBundle\Entity\Page $page)
	{
		$this->pages->removeElement($page);
	}

	/**
	 * Get page
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getPages()
	{
		return $this->pages;
	}

}
