<?php

namespace Cms\CollectionBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Cms\CoreBundle\Entity\Traits\Publishable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Collection
 *
 * @ORM\Table("cms_collection_collection")
 * @ORM\Entity(repositoryClass="Cms\CollectionBundle\Entity\CollectionRepository")
 */
class Collection
{

    use Timestampable;
    use Nameable;
    use Publishable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\CollectionItem", mappedBy="collection", cascade={"persist"}, orphanRemoval=true)
     */
    private $collectionItems;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\CollectionBundle\Entity\CollectionType")
     * @ORM\JoinColumn(name="collection_type", referencedColumnName="id", nullable=false)
     */
    private $collectionType;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->collectionItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add collectionItem
     *
     * @param \Cms\CollectionBundle\Entity\CollectionItem $collectionItem
     *
     * @return Collection
     */
    public function addCollectionItem(\Cms\CollectionBundle\Entity\CollectionItem $collectionItem)
    {
        $this->collectionItems[] = $collectionItem;
        $collectionItem->setCollection($this);
        return $this;
    }

    /**
     * Remove collectionItem
     *
     * @param \Cms\CollectionBundle\Entity\CollectionItem $collectionItem
     */
    public function removeCollectionItem(\Cms\CollectionBundle\Entity\CollectionItem $collectionItem)
    {
        $this->collectionItems->removeElement($collectionItem);
    }

    /**
     * Get collectionItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollectionItems()
    {
        return $this->collectionItems;
    }

    /**
     * Set collectionType
     *
     * @param \Cms\CollectionBundle\Entity\CollectionType $collectionType
     *
     * @return Collection
     */
    public function setCollectionType(\Cms\CollectionBundle\Entity\CollectionType $collectionType)
    {
        $this->collectionType = $collectionType;

        return $this;
    }

    /**
     * Get collectionType
     *
     * @return \Cms\CollectionBundle\Entity\CollectionType
     */
    public function getCollectionType()
    {
        return $this->collectionType;
    }
}
