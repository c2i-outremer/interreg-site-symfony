<?php

namespace Cms\GroupBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Cms\GroupBundle\Entity\GroupType;

class LoadGroupType implements FixtureInterface
{

	public function load(ObjectManager $manager)
	{
		$type = array(
			'name' => 'Groupe par defaut',
			'templatePath' => ':Group:default.html.twig',
		);

		$GroupType = new GroupType;
		$GroupType->setName($type['name']);
		$GroupType->setTemplatePath($type['templatePath']);
		
		$manager->persist($GroupType);

		$manager->flush();
	}

}
