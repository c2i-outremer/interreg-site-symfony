<?php

namespace Cms\ConfigBundle\Services;

use Doctrine\ORM\EntityManager;

class Online extends \Twig_Extension
{

	protected $manager;

	public function __construct(EntityManager $manager)
	{
		$this->manager = $manager;
	}

	public function getName()
	{
		return 'Online';
	}

	public function getFunctions()
	{
		return array(
			'isOnline' => new \Twig_Function_Method($this, 'isOnline')
		);
	}

	public function isOnline()
	{
		$config = $this->manager->getRepository('CmsConfigBundle:Configuration')->find(1);
		return $config->getOnline();
	}

}
