<?php

namespace Cms\BlogBundle\Form\Handler;

use CmS\BlogBundle\Manager\InscriptionManager;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class InscriptionHandler
{

    protected $form;
    protected $request;
    protected $manager;
    protected $mailer;

    public function __construct(Form $form, Request $request, InscriptionManager $manager, $mailer)
    {
        $this->form = $form;
        $this->request = $request;
        $this->manager = $manager;
        $this->mailer = $mailer;
    }

    public function process()
    {
        $this->form->handleRequest($this->request);
        if($this->request->isMethod('post') && $this->form->isValid()) {
            return true;
        }
        return false;
    }

    protected function onSuccess()
    {

    }

    public function getForm() {
        return $this->form;
    }

}