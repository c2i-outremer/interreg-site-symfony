<?php

namespace Extra\ServiceBundle\DataFixtures\ORM;

use Application\Sonata\UserBundle\Entity\Group;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadGroupData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $types = array('EDIT', 'LIST', 'CREATE', 'VIEW', 'DELETE', 'EXPORT', 'OPERATOR');

        $datas = array(
            array(
                'name' => 'Service',
                'roles' => array(
                    'ROLE_EXTRA_SERVICE_ADMIN_SERVICE',
                    'ROLE_EXTRA_SERVICE_ADMIN_FIELD',
                    'ROLE_EXTRA_SERVICE_ADMIN_PICTURE',
                    'ROLE_EXTRA_SERVICE_ADMIN_FILE',
                    'ROLE_EXTRA_SERVICE_ADMIN_VIDEO',
                )
            ),
        );

        foreach ($datas as $data) {
            $roles = array();
            $roles[] = 'ROLE_ADMIN';

            foreach ($data['roles'] as $role) {
                foreach ($types as $type) {
                    $roles[] = $role.'_'.$type;
                }
            }

            if (isset($data['extraRoles'])) {
                foreach ($data['extraRoles'] as $role) {
                    $roles[] = $role;
                }
            }

            $group = new Group($data['name'], $roles);
            $manager->persist($group);
            $manager->flush();
        }
    }

}
