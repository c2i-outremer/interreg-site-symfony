<?php

namespace Cms\CommentBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CommentAdmin extends Admin
{

	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'createdAt',
	);

	/**
	 * @param DatagridMapper $datagridMapper
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
				->add('id')
				->add('thread.id', null, array('label' => 'Origine'))
				->add('createdAt', null, array('label' => 'Création'))
                ->add('author', null, array('label' => 'Pseudo'))
				->add('body', null, array('label' => 'Message'))
				->add('published', null, array('label' => 'Publication'))
		;
	}

	/**
	 * @param ListMapper $listMapper
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
				->add('id')
				->add('createdAt', null, array('label' => 'Création'))
				->add('thread.id', null, array('label' => 'Origine'))
                ->add('author', null, array('label' => 'Pseudo'))
				->add('body', null, array('label' => 'Message', 'editable' => true))
				->add('published', null, array('label' => 'Publication', 'editable' => true))
				->add('_action', 'actions', array(
					'actions' => array(
						'show' => array(),
						'edit' => array(),
						'delete' => array(),
					)
				))
		;
	}

	/**
	 * @param FormMapper $formMapper
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
				->add('body')
				->add('ancestors')
				->add('depth')
				->add('createdAt')
				->add('state')
				->add('id')
				->add('published')
		;
	}

	/**
	 * @param ShowMapper $showMapper
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
				->add('body')
				->add('ancestors')
				->add('depth')
				->add('createdAt')
				->add('state')
				->add('id')
				->add('published')
		;
	}

	protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
	{
		parent::configureRoutes($collection);
		$collection->add('publish', $this->getRouterIdParameter() . '/publish');
        $collection->remove('create');
	}

	public function getBatchActions()
	{
		$actions = parent::getBatchActions();
		$actions['publish'] = array('label' => 'Publier','ask_confirmation' => false);
		return $actions;
	}

}
