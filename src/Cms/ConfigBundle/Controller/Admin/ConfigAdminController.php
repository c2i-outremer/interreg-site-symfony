<?php

namespace Cms\ConfigBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;

class ConfigAdminController extends CRUDController
{

    public function preCreate(Request $request, $object) {
        return $this->redirect($this->generateUrl('admin_cms_config_config_edit', array('id' => 1)));
    }

}
