<?php

namespace AppBundle\Entity\Project;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Attribute
 *
 * @ORM\Table("app_project_attribute")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\Project\AttributeTranslation")
 */
class Attribute extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use Nameable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Project\AttributeTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Attribute
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Remove translation
     *
     * @param \AppBundle\Entity\Project\AttributeTranslation $translation
     */
    public function removeTranslation(\AppBundle\Entity\Project\AttributeTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }
}
