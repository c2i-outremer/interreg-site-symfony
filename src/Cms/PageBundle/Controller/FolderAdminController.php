<?php

namespace Cms\PageBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;

class FolderAdminController extends CRUDController
{
    public function preCreate(Request $request, $object)
    {
        $this->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);
    }

    public function preEdit(Request $request, $object)
    {
        $this->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);
    }

}
