<?php

namespace Extra\HouseBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Price
 *
 * @ORM\Table("extra_house_price")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="Extra\HouseBundle\Entity\PriceTranslation")

 */
class Price extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Extra\HouseBundle\Entity\PriceTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    private $end;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="price", type="string", length=255, nullable=true)
     */
    private $price;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Extra\HouseBundle\Entity\House", inversedBy="prices")
     * @ORM\JoinColumn(name="house", referencedColumnName="id")
     */

    protected $house;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Price
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return Price
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Price
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Remove translation
     *
     * @param \Extra\HouseBundle\Entity\PriceTranslation $translation
     */
    public function removeTranslation(\Extra\HouseBundle\Entity\PriceTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Set house
     *
     * @param \Extra\HouseBundle\Entity\House $house
     *
     * @return Price
     */
    public function setHouse(\Extra\HouseBundle\Entity\House $house = null)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return \Extra\HouseBundle\Entity\House
     */
    public function getHouse()
    {
        return $this->house;
    }
}
