<?php

namespace Cms\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlogController extends Controller
{


    //------------------------------------------------------------------------------------------------------------------
    //	tagAction
    //------------------------------------------------------------------------------------------------------------------


    public function tagAction(Request $request, $tagSlug, $_locale)
    {
        $em = $this->getDoctrine()->getManager();

        if (!in_array($_locale, $this->container->getParameter('locales'))) {
            throw new NotFoundHttpException($this->get('translator')->trans('core.not_found.langage'));
        }

        $request->setLocale($_locale);

        $tag = $em->getRepository('CmsBlogBundle:Tag')->findTag($_locale, $tagSlug);

        if (!$tag or $tag->getSlug() != $tagSlug) {
            throw new NotFoundHttpException($this->get('translator')->trans('blog.not_found.tag').' : '.$tagSlug);
        }

        $nbrArticles = (int) $this->get('cms_config.GetConfig')->getConfig()->getNbrArticles();
        $posts = $em->getRepository('CmsBlogBundle:Article')->findArticle(null, $tag, 0, $nbrArticles);

        return $this->render(
            ':Blog:tag.html.twig',
            array(
                'localSwitcherTag' => true,
                'tagSearch' => $tag,
                'posts' => $posts,
            )
        );

    }

    //------------------------------------------------------------------------------------------------------------------
    //	CategoryAction
    //------------------------------------------------------------------------------------------------------------------

    public function categoryAction(Request $request, $categorySlug, $_locale)
    {
        if (!in_array($_locale, $this->container->getParameter('locales'))) {
            throw new NotFoundHttpException($this->get('translator')->trans('core.not_found.langage'));
        }

        $request->setLocale($_locale);

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('CmsBlogBundle:Category')->findCategory($_locale, $categorySlug);

        if (!$category or $category->getSlug() != $categorySlug) {
            throw new NotFoundHttpException($this->get('translator')->trans('blog.not_found.category').' : '.$categorySlug);
        }

        $nbrArticles = (int)$this->get('cms_config.GetConfig')->getConfig()->getNbrArticles();
        $posts = $em->getRepository('CmsBlogBundle:Article')->findArticle(array($category), null, 0, $nbrArticles, $_locale);

        return $this->render(
            ':Blog:category.html.twig',
            array(
                'localSwitcherCategory' => true,
                'category' => $category,
                'posts' => $posts,
            )
        );
    }

    //------------------------------------------------------------------------------------------------------------------
    //	showMoreAction
    //------------------------------------------------------------------------------------------------------------------


    public function showMoreAction(Request $request, $_locale)
    {
        if ($request->isXmlHttpRequest()) {

            if (!in_array($_locale, $this->container->getParameter('locales'))) {
                return false;
            }

            $request->setLocale($_locale);

            $em = $this->getDoctrine()->getManager();

            $offset = (int)$request->request->get('nbrPosts');

            $bloc = $em->getRepository('CmsBlocBundle:Bloc')->find((int)$request->request->get('bloc'));

            $nbrArticles = (int)$this->get('cms_config.GetConfig')->getConfig()->getNbrArticles();

            $posts = $em->getRepository('CmsBlogBundle:Article')->findArticle($bloc->getBlogCategorys()->toArray(), null, $offset, $nbrArticles, $_locale);

            $content = $this->render(
                ':Blog:loop.html.twig',
                array(
                    'posts' => $posts
                )
            );

            $stopShowMore = false;

            if (trim($content->getContent()) == null) {
                $stopShowMore = true;
            }

            return new JsonResponse(
                array(
                    'offset' => $offset,
                    'content' => $content->getContent(),
                    'stopShowMore' => $stopShowMore,
                    'stopShowMoreText' => $this->get('translator')->trans('blog.no_more'),
                )
            );
        }
    }



    //------------------------------------------------------------------------------------------------------------------
    //	tagShowMoreAction
    //------------------------------------------------------------------------------------------------------------------

    public function tagShowMoreAction(Request $request, $_locale)
    {
        if ($request->isXmlHttpRequest()) {

            if (!in_array($_locale, $this->container->getParameter('locales'))) {
                return false;
            }

            $request->setLocale($_locale);

            $em = $this->getDoctrine()->getManager();

            $offset = (int)$request->request->get('nbrPosts');

            $tag = $em->getRepository('CmsBlogBundle:Tag')->find((int)$request->request->get('tag'));

            $nbrArticles = (int)$this->get('cms_config.GetConfig')->getConfig()->getNbrArticles();

            $posts = $em->getRepository('CmsBlogBundle:Article')->findArticle(null, $tag, $offset, $nbrArticles, $_locale);

            $content = $this->render(
                ':Blog:loop.html.twig',
                array(
                    'posts' => $posts
                )
            );

            $stopShowMore = false;

            if (trim($content->getContent()) == null) {
                $stopShowMore = true;
            }

            return new JsonResponse(
                array(
                    'content' => $content->getContent(),
                    'stopShowMore' => $stopShowMore,
                    'stopShowMoreText' => $this->get('translator')->trans('blog.no_more'),
                )
            );
        }
    }


    //------------------------------------------------------------------------------------------------------------------
    //	CategoryShowMoreAction
    //------------------------------------------------------------------------------------------------------------------

    public function categoryShowMoreAction(Request $request, $_locale)
    {
        if ($request->isXmlHttpRequest()) {

            if (!in_array($_locale, $this->container->getParameter('locales'))) {
                return false;
            }

            $request->setLocale($_locale);

            $em = $this->getDoctrine()->getManager();

            $offset = (int)$request->request->get('nbrPosts');

            $category = $em->getRepository('CmsBlogBundle:Category')->find((int)$request->request->get('category'));

            $nbrArticles = (int)$this->get('cms_config.GetConfig')->getConfig()->getNbrArticles();

            $posts = $em->getRepository('CmsBlogBundle:Article')->findArticle(array($category), null, $offset, $nbrArticles, $_locale);

            $content = $this->render(
                ':Blog:loop.html.twig',
                array(
                    'posts' => $posts
                )
            );

            $stopShowMore = false;

            if (trim($content->getContent()) == null) {
                $stopShowMore = true;
            }

            return new JsonResponse(
                array(
                    'content' => $content->getContent(),
                    'stopShowMore' => $stopShowMore,
                    'stopShowMoreText' => $this->get('translator')->trans('blog.no_more'),
                )
            );
        }
    }


}
