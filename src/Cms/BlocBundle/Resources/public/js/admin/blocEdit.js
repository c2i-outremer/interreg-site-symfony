function showCategoryId(categoryId) {


	if (categoryId == 1) {
		$("div[id$='_contentText']").show();
		$("div[id$='_contentHtml']").hide();
		$("div[id$='_group']").hide();
		$("div[id$='_controller']").hide();
		$("div[id$='_query']").hide();
		$("div[id$='_menu']").hide();
		$("div[id$='_collection']").hide();
		$('.blog').hide();

	}
	if (categoryId == 2) {
		$("div[id$='_contentHtml']").show();
		$("div[id$='_contentText']").hide();
		$("div[id$='_group']").hide();
		$("div[id$='_controller']").hide();
		$("div[id$='_query']").hide();
		$("div[id$='_menu']").hide();
		$("div[id$='_collection']").hide();
		$('.blog').hide();

	}
	if (categoryId == 3) {
		$("div[id$='_controller']").show();
		$("div[id$='_query']").show();
		$("div[id$='_contentHtml']").hide();
		$("div[id$='_contentText']").hide();
		$("div[id$='_group']").hide();
		$("div[id$='_menu']").hide();
		$("div[id$='_collection']").hide();
		$('.blog').hide();

	}
	if (categoryId == 4) {
		$("div[id$='_group']").show();
		$("div[id$='_contentHtml']").hide();
		$("div[id$='_contentText']").hide();
		$("div[id$='_controller']").hide();
		$("div[id$='_query']").hide();
		$("div[id$='_menu']").hide();
		$("div[id$='_collection']").hide();
		$('.blog').hide();

	}
	if (categoryId == 5) {
		$("div[id$='_menu']").show();
		$("div[id$='_group']").hide();
		$("div[id$='_contentHtml']").hide();
		$("div[id$='_contentText']").hide();
		$("div[id$='_controller']").hide();
		$("div[id$='_query']").hide();
		$("div[id$='_collection']").hide();
		$('.blog').hide();

	}
	if (categoryId == 6) {
		$("div[id$='_group']").hide();
		$("div[id$='_contentHtml']").hide();
		$("div[id$='_contentText']").hide();
		$("div[id$='_controller']").hide();
		$("div[id$='_query']").hide();
		$("div[id$='_menu']").hide();
		$("div[id$='_collection']").show();
		$('.blog').hide();
	}
	if (categoryId == 7) {
		$("div[id$='_group']").hide();
		$("div[id$='_contentHtml']").hide();
		$("div[id$='_contentText']").hide();
		$("div[id$='_controller']").hide();
		$("div[id$='_query']").hide();
		$("div[id$='_menu']").hide();
		$("div[id$='_collection']").hide();
		$('.blog').show();
	}
}

$(function() {
	//get original category 
	if (typeof blocCategoryId !== 'undefined') {
		showCategoryId(blocCategoryId);
	}
// new category 
	$("body").on('change click', "[id$='_blocCategory']", function() {
		showCategoryId($(this).val());
	});
});

