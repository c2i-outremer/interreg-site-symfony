<?php

namespace Cms\CollectionBundle\Admin;

use Cms\CollectionBundle\Entity\NameTranslation;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class NameAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('position')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('position')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('position')
            ->add('name')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('position')
        ;
    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $locales = $container->getParameter('locales');
        $defaultLocale = $container->getParameter('locale');
        $em = $container->get('doctrine.orm.entity_manager');

        $fields = array(
            'published' => 0,
        );

        foreach ($locales as $locale) {
            foreach ($fields as $key => $value) {
                if ($locale != $defaultLocale) {
                    $translation = new NameTranslation($locale, $key, $value);
                    $translation->setObject($object);
                    $em->persist($translation);
                }
            }
        }

        $em->flush();

    }
}
