<?php

namespace Cms\BlogBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\PublishTranslatable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;


/**
 * Article
 *
 * @ORM\Table("cms_blog_article")
 * @ORM\Entity(repositoryClass="Cms\BlogBundle\Entity\ArticleRepository")
 * @Gedmo\TranslationEntity(class="Cms\BlogBundle\Entity\ArticleTranslation")
 */
class Article extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use Timestampable;
    use NameTranslatable;
    use PublishTranslatable;
    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_event", type="datetime", nullable=true)
     */
    protected $dateEvent;

    /**
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\ArticleTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @ORM\Column(name="numberView", type="integer")
     */
    protected $numberView;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     * */
    protected $user;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\Picture", mappedBy="article", cascade={"persist"}, orphanRemoval=true)
     */
    protected $pictures;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\Video", mappedBy="article", cascade={"persist"}, orphanRemoval=true)
     */
    protected $videos;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\File", mappedBy="article", cascade={"persist"}, orphanRemoval=true)
     */
    protected $files;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\Field", mappedBy="article", cascade={"persist"}, orphanRemoval=true)
     */
    protected $fields;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\BlogBundle\Entity\ArticleType")
     * @ORM\JoinColumn(name="article_type", referencedColumnName="id", nullable=true)
     * */
    protected $articleType;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\BlogBundle\Entity\Category", inversedBy="articles")
     * @ORM\JoinColumn(name="category", referencedColumnName="id", nullable=true)
     * */
    protected $category;

    /**
     * @ORM\ManyToMany(targetEntity="Cms\BlogBundle\Entity\Tag")
     * @ORM\JoinTable(name="cms_blog_article_tags",
     *      joinColumns={@ORM\JoinColumn(name="article", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag", referencedColumnName="id")}
     *      )
     * */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity="Cms\BlogBundle\Entity\Lieu")
     * @ORM\JoinTable(name="cms_blog_article_lieux",
     *      joinColumns={@ORM\JoinColumn(name="article", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="lieu", referencedColumnName="id")}
     *      )
     * */
    private $lieux;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="cover", referencedColumnName="id", nullable=true)
     */
    protected $cover;

    /**
     * @ORM\Column(name="top", type="boolean", nullable=true)
     */
    protected $top;

    /**
     * @ORM\Column(name="allowInscriptions", type="boolean", nullable=true)
     */
    protected $allowInscriptions;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\Inscription", mappedBy="article", cascade={"persist", "remove"})
     */
    protected $inscriptions;

    /**
     * @ORM\Column(name="enable_comment", type="boolean", nullable=true)
     */
    protected $enableComment;

    /**
     * @var \DateTime
     * @ORM\Column(name="publish_start_date", type="datetime", nullable=true)
     */
    private $publishStartDate;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="short_content", type="text", nullable=true)
     */
    protected $shortContent;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="extra_content", type="text", nullable=true)
     */
    protected $extraContent;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="text", nullable=true)
     */
    protected $author;

    public function __construct()
    {
        $this->setPublished(false);
        $this->setEnableComment(true);
        $this->setPublishStartDate(new \DateTime);
        $this->numberView = 0;
        $this->top = false;
        $this->pictures = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->lieux = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->fields = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articleType
     *
     * @param \Cms\BlogBundle\Entity\ArticleType $articleType
     * @return Article
     */
    public function setArticleType(\Cms\BlogBundle\Entity\ArticleType $articleType)
    {
        $this->articleType = $articleType;

        return $this;
    }

    /**
     * Get articleType
     *
     * @return \Cms\BlogBundle\Entity\ArticleType
     */
    public function getArticleType()
    {
        return $this->articleType;
    }

    /**
     * Set category
     *
     * @param \Cms\BlogBundle\Entity\Category $category
     * @return Article
     */
    public function setCategory(\Cms\BlogBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Cms\BlogBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set shortContent
     *
     * @param string $shortContent
     * @return Article
     */
    public function setShortContent($shortContent)
    {
        $this->shortContent = $shortContent;

        return $this;
    }

    /**
     * Get shortContent
     *
     * @return string
     */
    public function getShortContent()
    {
        return $this->shortContent;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set extraContent
     *
     * @param string $extraContent
     * @return Article
     */
    public function setExtraContent($extraContent)
    {
        $this->extraContent = $extraContent;

        return $this;
    }

    /**
     * Get extraContent
     *
     * @return string
     */
    public function getExtraContent()
    {
        return $this->extraContent;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Article
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set cover
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $cover
     * @return Article
     */
    public function setCover(\Application\Sonata\MediaBundle\Entity\Media $cover = null)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set top
     *
     * @param boolean $top
     * @return Article
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Get top
     *
     * @return boolean
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * Set numberView
     *
     * @param integer $numberView
     * @return Article
     */
    public function setNumberView($numberView)
    {
        $this->numberView = $numberView;

        return $this;
    }

    /**
     * Get numberView
     *
     * @return integer
     */
    public function getNumberView()
    {
        return $this->numberView;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Article
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


	/**
	 *
	 * @return type
	 */
	public function getArticleLink()
	{
		if ($this->getCategory())
		{
			return $this->getCategory()->getSlug() . '/' . $this->getSlug();
		}

		return $this->getSlug();
	}


    /**
     * Set enableComment
     *
     * @param boolean $enableComment
     * @return Article
     */
    public function setEnableComment($enableComment)
    {
        $this->enableComment = $enableComment;

        return $this;
    }

    /**
     * Get enableComment
     *
     * @return boolean
     */
    public function getEnableComment()
    {
        return $this->enableComment;
    }

    /**
     * Set publishStartDate
     *
     * @param \DateTime $publishStartDate
     * @return Article
     */
    public function setPublishStartDate($publishStartDate)
    {
        $this->publishStartDate = $publishStartDate;

        return $this;
    }

    /**
     * Get publishStartDate
     *
     * @return \DateTime
     */
    public function getPublishStartDate()
    {
        return $this->publishStartDate;
    }


    /**
     * Add picture
     *
     * @param \Cms\BlogBundle\Entity\Picture $picture
     *
     * @return Article
     */
    public function addPicture(\Cms\BlogBundle\Entity\Picture $picture)
    {
        $this->pictures[] = $picture;
        $picture->setArticle($this);

        return $this;
    }

    /**
     * Remove picture
     *
     * @param \Cms\BlogBundle\Entity\Picture $picture
     */
    public function removePicture(\Cms\BlogBundle\Entity\Picture $picture)
    {
        $this->pictures->removeElement($picture);
    }

    /**
     * Get pictures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * Add video
     *
     * @param \Cms\BlogBundle\Entity\Video $video
     *
     * @return Article
     */
    public function addVideo(\Cms\BlogBundle\Entity\Video $video)
    {
        $this->videos[] = $video;
        $video->setArticle($this);

        return $this;
    }

    /**
     * Remove video
     *
     * @param \Cms\BlogBundle\Entity\Video $video
     */
    public function removeVideo(\Cms\BlogBundle\Entity\Video $video)
    {
        $this->videos->removeElement($video);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Add file
     *
     * @param \Cms\BlogBundle\Entity\File $file
     *
     * @return Article
     */
    public function addFile(\Cms\BlogBundle\Entity\File $file)
    {
        $this->files[] = $file;
        $file->setArticle($this);

        return $this;
    }

    /**
     * Remove file
     *
     * @param \Cms\BlogBundle\Entity\File $file
     */
    public function removeFile(\Cms\BlogBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }


    /**
     * Add tag
     *
     * @param \Cms\BlogBundle\Entity\Tag $tag
     *
     * @return Article
     */
    public function addTag(\Cms\BlogBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \Cms\BlogBundle\Entity\Tag $tag
     */
    public function removeTag(\Cms\BlogBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add field
     *
     * @param \Cms\BlogBundle\Entity\Field $field
     *
     * @return Article
     */
    public function addField(\Cms\BlogBundle\Entity\Field $field)
    {
        $this->fields[] = $field;
        $field->setArticle($this);

        return $this;
    }

    /**
     * Remove field
     *
     * @param \Cms\BlogBundle\Entity\Field $field
     */
    public function removeField(\Cms\BlogBundle\Entity\Field $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set dateEvent
     *
     * @param \DateTime $dateEvent
     *
     * @return Article
     */
    public function setDateEvent($dateEvent)
    {
        $this->dateEvent = $dateEvent;

        return $this;
    }

    /**
     * Get dateEvent
     *
     * @return \DateTime
     */
    public function getDateEvent()
    {
        return $this->dateEvent;
    }

    /**
     * Remove translation
     *
     * @param \Cms\BlogBundle\Entity\ArticleTranslation $translation
     */
    public function removeTranslation(\Cms\BlogBundle\Entity\ArticleTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Set allowInscriptions
     *
     * @param boolean $allowInscriptions
     *
     * @return Article
     */
    public function setAllowInscriptions($allowInscriptions)
    {
        $this->allowInscriptions = $allowInscriptions;

        return $this;
    }

    /**
     * Get allowInscriptions
     *
     * @return boolean
     */
    public function getAllowInscriptions()
    {
        return $this->allowInscriptions;
    }

    /**
     * Add inscription
     *
     * @param \Cms\BlogBundle\Entity\Inscription $inscription
     *
     * @return Article
     */
    public function addInscription(\Cms\BlogBundle\Entity\Inscription $inscription)
    {
        $this->inscriptions[] = $inscription;

        return $this;
    }

    /**
     * Remove inscription
     *
     * @param \Cms\BlogBundle\Entity\Inscription $inscription
     */
    public function removeInscription(\Cms\BlogBundle\Entity\Inscription $inscription)
    {
        $this->inscriptions->removeElement($inscription);
    }

    /**
     * Get inscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscriptions()
    {
        return $this->inscriptions;
    }

    /**
     * Add lieux
     *
     * @param \Cms\BlogBundle\Entity\Lieu $lieux
     *
     * @return Article
     */
    public function addLieux(\Cms\BlogBundle\Entity\Lieu $lieux)
    {
        $this->lieux[] = $lieux;

        return $this;
    }

    /**
     * Remove lieux
     *
     * @param \Cms\BlogBundle\Entity\Lieu $lieux
     */
    public function removeLieux(\Cms\BlogBundle\Entity\Lieu $lieux)
    {
        $this->lieux->removeElement($lieux);
    }

    /**
     * Get lieux
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLieux()
    {
        return $this->lieux;
    }
}
