<?php

namespace Cms\BlogBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table("cms_blog_category")
 * @ORM\Entity(repositoryClass="Cms\BlogBundle\Entity\CategoryRepository")
 * @Gedmo\TranslationEntity(class="Cms\BlogBundle\Entity\CategoryTranslation")
 */
class Category extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use NameTranslatable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\CategoryTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="label_class", type="text", nullable=true)
	 */
	protected $labelClass;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\Article", mappedBy="category")
	 * */
	protected $articles;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->articles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
		$this->labelClass = "label-default";
	}
	
	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	public function getLabelClass()
	{
		return $this->labelClass;
	}

	public function setLabelClass($labelClass)
	{
		$this->labelClass = $labelClass;
		return $this;
	}

	/**
	 * Add articles
	 *
	 * @param \Cms\BlogBundle\Entity\Article $articles
	 * @return Category
	 */
	public function addArticle(\Cms\BlogBundle\Entity\Article $articles)
	{
		$this->articles[] = $articles;
		$articles->setCategory($this);
		return $this;
	}

	/**
	 * Remove articles
	 *
	 * @param \Cms\BlogBundle\Entity\Article $articles
	 */
	public function removeArticle(\Cms\BlogBundle\Entity\Article $articles)
	{
		$this->articles->removeElement($articles);
	}

	/**
	 * Get articles
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getArticles()
	{
		return $this->articles;
	}

}
