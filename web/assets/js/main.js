$(function() {
    $(".p-must-be-equal p").equalHeights();

    $(window).resize(function(){
        $('.p-must-be-equal p').css('height','100%');
        $('.p-must-be-equal p').equalHeights();
    });

    var icons = {
        header: "ui-icon-circle-arrow-e",
        activeHeader: "ui-icon-circle-arrow-s"
    };

    $( ".accordion" ).accordion({
        collapsible: true,
        active: false,
        icons: icons,
        heightStyle: "content",
        header: "h2"
    });
    $( ".sub-accordion" ).accordion({
        collapsible: true,
        active: false,
        icons: icons,
        heightStyle: "content",
        header: "h3"
    });
});