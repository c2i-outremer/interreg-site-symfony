<?php

namespace Cms\NavBundle\Manager;


use Cms\CoreBundle\Manager\BaseManager;

class LinkManager extends BaseManager
{

    public function getNodes($rootLink)
    {
        return $this->repo->getNodes($rootLink);
    }

}