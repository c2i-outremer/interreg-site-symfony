<?php

namespace Cms\PrehomeBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PrehomeAdmin extends Admin
{

	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'position',
	);

	/**
	 * @param DatagridMapper $datagridMapper
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
				->add('name')
				->add('slug')
				->add('published')

		;
	}

	/**
	 * @param ListMapper $listMapper
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
				->add('name')
				->add('slug')
				->add('published')
				->add('start')
				->add('end')
				->add('_action', 'actions', array(
					'actions' => array(
						'show' => array(),
						'edit' => array(),
						'delete' => array(),
					)
				))
		;
	}

	/**
	 * @param FormMapper $formMapper
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
                ->with('Identification', array('class' => 'col-md-4'))
                    ->add('name', null, array('label' => 'Nom', 'attr' => array('autofocus' => 'autofocus'), 'help' => 'trans'))
                    ->add('slug', null, array('disabled' => true, 'help' => 'trans'))
                ->end()
                ->with('Publication', array('class' => 'col-md-4'))
				    ->add('published', null, array('label' => 'Publication', 'label_attr' => array('class' => 'help-trans')))
                ->end()
                ->with('Lien', array('class' => 'col-md-4'))
                    ->add('link', null, array('label' => 'Adresse du lien', 'help' => 'trans'))
                    ->add('linkAnchor', null, array('label' => 'Texte du lien', 'help' => 'trans'))
                ->end()
                ->with('Date', array('class' => 'col-md-6'))
                    ->add('start', null, array('label' => 'Début'))
                    ->add('end', null, array('label' => 'Fin'))
                ->end()
                ->with('Image', array('class' => 'col-md-6'))
				    ->add('cover', 'sonata_type_model_list', array('required' => false, 'label' => 'Couverture'))
                ->end()
                ->with('Contenu', array('class' => 'col-md-12'))
                    ->add('shortContent', 'ckeditor', array('required' => false, 'label' => 'Chapeau', 'help' => 'trans'))
                    ->add('content', 'ckeditor', array('required' => false, 'label' => 'Contenu', 'help' => 'trans'))
                ->end()
		;
	}

	/**
	 * @param ShowMapper $showMapper
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
				->add('created')
				->add('updated')
				->add('position')
				->add('published')
				->add('name')
				->add('slug')
				->add('id')
				->add('start')
				->add('end')
				->add('shortContent')
				->add('content')
				->add('link')
		;
	}

}
