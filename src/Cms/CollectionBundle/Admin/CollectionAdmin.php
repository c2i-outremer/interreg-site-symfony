<?php

namespace Cms\CollectionBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CollectionAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('created')
            ->add('updated')
            ->add('name')
            ->add('slug')
            ->add('published')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array('label' => 'Nom'))
            ->add('slug', null, array('label' => 'Slug'))
            ->add('published', null, array('label' => 'Publication', 'editable' => true))
            ->add('collectionType')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Identification', array('class' => 'col-sm-4'))
                ->add('name', null, array('label' => 'Nom'))
                ->add('slug', null, array('disabled' => true))
            ->end()
            ->with('Publication', array('class' => 'col-sm-4'))
                ->add('published', null, array('label' => ' '))
            ->end()
            ->with('Modèle', array('class' => 'col-sm-4'))
                ->add('collectionType', null, array('label' => false))
            ->end()
            ->with('Items', array('class' => 'col-sm-12'))
                ->add('collectionItems', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)), array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            ->end()

        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('created')
            ->add('updated')
            ->add('name')
            ->add('slug')
            ->add('published')
        ;
    }
}
