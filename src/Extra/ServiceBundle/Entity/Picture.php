<?php

namespace Extra\ServiceBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\ORM\Mapping as ORM;

/**
 * File
 *
 * @ORM\Table("extra_service_picture")
 * @ORM\Entity
 */
class Picture
{
    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="media", referencedColumnName="id", nullable=true)
     */
    protected $media;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Extra\ServiceBundle\Entity\Service", inversedBy="pictures")
     * @ORM\JoinColumn(name="service", referencedColumnName="id")
     */

    protected $service;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     *
     * @return Picture
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set service
     *
     * @param \Extra\ServiceBundle\Entity\Service $service
     *
     * @return Picture
     */
    public function setService(\Extra\ServiceBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \Extra\ServiceBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }
}
