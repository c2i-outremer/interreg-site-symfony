<?php

namespace Cms\BlocBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlocAdminController extends CRUDController
{
    public function cloneAction()
    {
        $object = $this->admin->getSubject();

//        ld($object);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $object->getId()));
        }

        $clonedObject = clone $object;

        $clonedObject->setName($object->getName() .  ' clone:' . uniqid());

//        ld($clonedObject);

        $this->admin->create($clonedObject);

        $this->addFlash('sonata_flash_success', 'Cloned successfully');

        return $this->redirect($this->admin->generateUrl('list'));
    }
}
