<?php

namespace Extra\HouseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start', 'birthday', array('widget' => 'single_text','input' => 'string','format' => 'dd/MM/yyyy', 'attr' => array('class' => 'date'),))
            ->add('end', 'birthday', array('widget' => 'single_text','input' => 'string','format' => 'dd/MM/yyyy', 'attr' => array('class' => 'date'),))
            ->add('nbrAdult', 'integer')
            ->add('nbrChild', 'integer')
            ->add('firstname', 'text')
            ->add('lastname', 'text')
            ->add('email', 'email')
            ->add('mobile', 'text')
            ->add('message', 'textarea')
            ->add('save', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Extra\HouseBundle\Entity\Reservation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'extra_housebundle_reservation_form';
    }
}
