<?php

namespace Cms\PageBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\PublishTranslatable;
use Cms\CoreBundle\Entity\Traits\Timestampable;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * page
 *
 * @ORM\Table("cms_page_page")
 * @ORM\Entity(repositoryClass="Cms\PageBundle\Entity\PageRepository")
 * @Gedmo\TranslationEntity(class="Cms\PageBundle\Entity\PageTranslation")
 */
class Page extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use Timestampable;
    use NameTranslatable;
    use PublishTranslatable;
    use Positionable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\PageBundle\Entity\PageTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;


	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="meta_title", type="text")
	 */
	private $metaTitle;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="meta_h1", type="text")
	 */
	private $metaH1;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="meta_description", type="text", nullable=true)
	 */
	private $metaDescription;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="meta_keyword", type="text", nullable=true)
	 */
	private $metaKeyword;

	/**
	 * @var string
	 * @ORM\Column(name="other_meta", type="text", nullable=true)
	 */
	private $otherMeta;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="anonymous", type="boolean", nullable=true)
	 */
	private $anonymous;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="show_metaH1", type="boolean", nullable=true)
	 */
	private $showMetaH1;

    /**
     * @var string
     * @ORM\Column(name="page_model", type="string", length=255, nullable=true)
     * @NotBlank()
     */
    protected $pageModel;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\ControllerBundle\Entity\Controller")
     * @ORM\JoinColumn(name="controller", referencedColumnName="id")
     * */
    private $controller;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\ControllerBundle\Entity\Query")
     * @ORM\JoinColumn(name="query", referencedColumnName="id")
     * */
    private $query;

	/**
     * @var integer
	 *
	 * @ORM\Column(name="nbr_views", type="integer")
	 */
	private $nbrViews;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\NavBundle\Entity\Link", mappedBy="page")
	 * */
	private $links;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\ConfigBundle\Entity\Lang", inversedBy="pages")
	 * @ORM\JoinColumn(name="lang", referencedColumnName="id", nullable=true)
	 * */
	private $lang;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\PageBundle\Entity\Folder", inversedBy="pages")
	 * @ORM\JoinColumn(name="folder", referencedColumnName="id", nullable=true)
	 * */
	private $folder;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\PageBundle\Entity\PageType", inversedBy="pages")
	 * @ORM\JoinColumn(name="page_type", referencedColumnName="id", nullable=true)
	 * */
	private $pageType;

	/**
	 * @ORM\OrderBy({"position" = "ASC"})
	 * @ORM\OneToMany(targetEntity="Cms\PageBundle\Entity\PageBloc", mappedBy="page", cascade={"persist"}, orphanRemoval=true)

	 * */
	private $pageBlocs;

	function __construct()
	{
		$this->anonymous = true;
		$this->nbrViews = 0;
		$this->links = new ArrayCollection();
		$this->pageBlocs = new ArrayCollection();
        $this->translations = new ArrayCollection();
	}

	function getNbrViews()
	{
		return $this->nbrViews;
	}

	function setNbrViews($nbrViews)
	{
		$this->nbrViews = $nbrViews;
		return $this;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set metaTitle
	 *
	 * @param string $metaTitle
	 * @return Page
	 */
	public function setMetaTitle($metaTitle)
	{
		$this->metaTitle = $metaTitle;

		return $this;
	}

	/**
	 * Get metaTitle
	 *
	 * @return string 
	 */
	public function getMetaTitle()
	{
		return $this->metaTitle;
	}

	public function getMetaH1()
	{
		return $this->metaH1;
	}

	public function setMetaH1($metaH1)
	{
		$this->metaH1 = $metaH1;
		return $this;
	}

	/**
	 * Set metaDescription
	 *
	 * @param string $metaDescription
	 * @return Page
	 */
	public function setMetaDescription($metaDescription)
	{
		$this->metaDescription = $metaDescription;

		return $this;
	}

	/**
	 * Get metaDescription
	 *
	 * @return string 
	 */
	public function getMetaDescription()
	{
		return $this->metaDescription;
	}

	/**
	 * Set metaKeyword
	 *
	 * @param string $metaKeyword
	 * @return Page
	 */
	public function setMetaKeyword($metaKeyword)
	{
		$this->metaKeyword = $metaKeyword;

		return $this;
	}

	/**
	 * Get metaKeyword
	 *
	 * @return string 
	 */
	public function getMetaKeyword()
	{
		return $this->metaKeyword;
	}

	/**
	 * Set anonymous
	 *
	 * @param boolean $anonymous
	 * @return Page
	 */
	public function setAnonymous($anonymous)
	{
		$this->anonymous = $anonymous;

		return $this;
	}

	/**
	 * Get anonymous
	 *
	 * @return boolean 
	 */
	public function getAnonymous()
	{
		return $this->anonymous;
	}



	/**
	 * Add links
	 *
	 * @param \Cms\NavBundle\Entity\Link $links
	 * @return Page
	 */
	public function addLink(\Cms\NavBundle\Entity\Link $links)
	{
		$this->links[] = $links;
		$links->setPage($this);
		return $this;
	}

	/**
	 * Remove links
	 *
	 * @param \Cms\NavBundle\Entity\Link $links
	 */
	public function removeLink(\Cms\NavBundle\Entity\Link $links)
	{
		$this->links->removeElement($links);
	}

	/**
	 * Get links
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getLinks()
	{
		return $this->links;
	}

	/**
	 * Set lang
	 *
	 * @param \Cms\ConfigBundle\Entity\Lang $lang
	 * @return Page
	 */
	public function setLang(\Cms\ConfigBundle\Entity\Lang $lang = null)
	{
		$this->lang = $lang;

		return $this;
	}

	/**
	 * Get lang
	 *
	 * @return \Cms\ConfigBundle\Entity\Lang 
	 */
	public function getLang()
	{
		return $this->lang;
	}

	/**
	 * Set folder
	 *
	 * @param \Cms\PageBundle\Entity\Folder $folder
	 * @return Page
	 */
	public function setFolder(\Cms\PageBundle\Entity\Folder $folder = null)
	{
		$this->folder = $folder;

		return $this;
	}

	/**
	 * Get lang
	 *
	 * @return \\Cms\PageBundle\Entity\Folder 
	 */
	public function getFolder()
	{
		return $this->folder;
	}

	/**
	 * Set pageType
	 *
	 * @param \Cms\PageBundle\Entity\PageType $pageType
	 * @return Page
	 */
	public function setPageType(\Cms\PageBundle\Entity\PageType $pageType = null)
	{
		$this->pageType = $pageType;

		return $this;
	}

	/**
	 * Get pageType
	 *
	 * @return \Cms\PageBundle\Entity\PageType 
	 */
	public function getPageType()
	{
		return $this->pageType;
	}

	/**
	 * Add pageBlocs
	 *
	 * @param \Cms\PageBundle\Entity\PageBloc $pageBlocs
	 * @return Page
	 */
	public function addPageBloc(\Cms\PageBundle\Entity\PageBloc $pageBlocs)
	{
		$this->pageBlocs[] = $pageBlocs;
		$pageBlocs->setPage($this);
		return $this;
	}

	/**
	 * Remove pageBlocs
	 *
	 * @param \Cms\PageBundle\Entity\PageBloc $pageBlocs
	 */
	public function removePageBloc(\Cms\PageBundle\Entity\PageBloc $pageBlocs)
	{
		$this->pageBlocs->removeElement($pageBlocs);
	}

	/**
	 * Get pageBlocs
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getPageBlocs()
	{
		return $this->pageBlocs;
	}

	/**
	 *  getPageLink
	 * @return type
	 */
	public function getPageLink()
	{
		$r = '';
		if ($this->getFolder())
		{
			$r .= $this->getFolder()->getSlug() . '/';
		}
		$r .= $this->getSlug();
		return $r;
	}

	/**
	 * Set otherMeta
	 *
	 * @param string $otherMeta
	 * @return Page
	 */
	public function setOtherMeta($otherMeta)
	{
		$this->otherMeta = $otherMeta;

		return $this;
	}

	/**
	 * Get otherMeta
	 *
	 * @return string 
	 */
	public function getOtherMeta()
	{
		return $this->otherMeta;
	}



    /**
     * Set controller
     *
     * @param \Cms\ControllerBundle\Entity\Controller $controller
     *
     * @return Page
     */
    public function setController(\Cms\ControllerBundle\Entity\Controller $controller = null)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return \Cms\ControllerBundle\Entity\Controller
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Set query
     *
     * @param \Cms\ControllerBundle\Entity\Query $query
     *
     * @return Page
     */
    public function setQuery(\Cms\ControllerBundle\Entity\Query $query = null)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get query
     *
     * @return \Cms\ControllerBundle\Entity\Query
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set pageModel
     *
     * @param string $pageModel
     *
     * @return Page
     */
    public function setPageModel($pageModel)
    {
        $this->pageModel = $pageModel;

        return $this;
    }

    /**
     * Get pageModel
     *
     * @return string
     */
    public function getPageModel()
    {
        return $this->pageModel;
    }


    /**
     * Set showMetaH1
     *
     * @param boolean $showMetaH1
     *
     * @return Page
     */
    public function setShowMetaH1($showMetaH1)
    {
        $this->showMetaH1 = $showMetaH1;

        return $this;
    }

    /**
     * Get showMetaH1
     *
     * @return boolean
     */
    public function getShowMetaH1()
    {
        return $this->showMetaH1;
    }

    /**
     * Remove translation
     *
     * @param \Cms\PageBundle\Entity\PageTranslation $translation
     */
    public function removeTranslation(\Cms\PageBundle\Entity\PageTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }
}
