<?php

namespace Cms\PageBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;

class PageAdminController extends CRUDController
{
    public function preCreate(Request $request, $object)
    {
        $this->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);
    }

    public function preEdit(Request $request, $object)
    {
        $this->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);
    }



    public function dashboardAction() {

        $pages = $this->getDoctrine()->getManager()->getRepository('CmsPageBundle:Page')->findBy(
            array(),
            array('created' => 'desc'),
            30,
            0
        );
        return $this->render('CmsPageBundle:Admin:page_dashboard_bloc.html.twig', array(
            'pages' => $pages,
        ));
    }

	//------------------------------------------------------------------------------------------------------------------
	//	statAction
	//------------------------------------------------------------------------------------------------------------------

	/**
	 * 
	 * @param type $id
	 * @return type
	 */
	public function statAction($id)
	{
		return $this->render('CmsCoreBundle:Admin:stat_date.html.twig', array(
					'id' => (int) $id,
                    'path' => 'admin_cms_page_page_result'
		));
	}

	//------------------------------------------------------------------------------------------------------------------
	//	resultAction
	//------------------------------------------------------------------------------------------------------------------

	/**
	 * 
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param type $id
	 * @return type
	 */
	public function resultAction(Request $request, $id)
	{

		$em = $this->getDoctrine()->getManager();
		$visiteRepo = $em->getRepository('CmsCoreBundle:Visite');
		
		$start = $this->convertDateToObject($request->request->get('start'));
		$end = $this->convertDateToObject($request->request->get('end'));
		$periodValue = $request->request->get('periodValue');
		
		$dateInterval = \DateInterval::createFromDateString($periodValue);
		$datePeriod = new \DatePeriod($start, $dateInterval, $end);

		$days = array();

		foreach ($datePeriod as $day)
		{
			$data = array();
			$data['start'] = $day;
			$start = clone $day;
			$end = $start->add(\DateInterval::createFromDateString($periodValue));
			$data['end'] = $end;
			$data['cpt'] = count($visiteRepo->getStatsOfDay(array('dataType' => 'page', 'typeId' => (int) $id, 'start' => $day, 'end' => $end)));
			$days[] = $data;
		}

		return $this->render('CmsCoreBundle:Admin:stat_result.html.twig', array(
					'days' => $days,
		));
	}

	//------------------------------------------------------------------------------------------------------------------
	//	convertDateToObject
	//------------------------------------------------------------------------------------------------------------------

	/**
	 * 
	 * @param type $string
	 * @return \DateTime
	 */
	private function convertDateToObject($string)
	{		
		$date = explode('-', $string);
		$object = new \DateTime();
		$object->setDate($date[0], $date[1], $date[2]);
		$object->setTime(0, 0, 0);
		return $object;
	}

}
