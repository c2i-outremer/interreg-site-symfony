<?php

namespace Cms\CollectionBundle\Admin;

use Cms\CollectionBundle\Entity\ContentTranslation;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ContentAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('content')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('content')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('position')
            ->add('content', 'ckeditor', array('label' => 'Contenu'))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('content')
        ;
    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $locales = $container->getParameter('locales');
        $defaultLocale = $container->getParameter('locale');
        $em = $container->get('doctrine.orm.entity_manager');

        $fields = array(
            'published' => 0,
        );

        foreach ($locales as $locale) {
            foreach ($fields as $key => $value) {
                if ($locale != $defaultLocale) {
                    $translation = new ContentTranslation($locale, $key, $value);
                    $translation->setObject($object);
                    $em->persist($translation);
                }
            }
        }

        $em->flush();

    }
}
