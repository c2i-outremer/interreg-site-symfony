<?php

namespace Cms\CoreBundle\Services;

use Cms\CoreBundle\Entity\CKTemplate;
use Doctrine\ORM\EntityManager;


class CKTemplateChain
{

	protected $templates;
	protected $manager;
	protected $twig;

	public function __construct(EntityManager $manager, $twig)
	{
		$this->manager = $manager;
		$this->templates = $manager->getRepository('CmsCoreBundle:CKTemplate')->findAll();
		$this->twig = $twig;
	}


	public function getTemplates()
	{

		$templates = array();
		foreach ($this->templates as $ckTemplate)
		{
			$templates[] = array(
				'title' => $ckTemplate->getTitle(),
				'image' => $ckTemplate->getImage(),
				'description' => $ckTemplate->getDescription(),
				'html' => $this->twig->render($ckTemplate->getHtml()),
			);
		}

		return array(
			'CKTemplate_templates' => array(
				'imagesPath' => '',
				'templates' => $templates,
			),
		);
	}

}
