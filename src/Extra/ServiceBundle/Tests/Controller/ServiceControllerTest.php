<?php

namespace Extra\ServiceBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServiceControllerTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        self::bootKernel();
        $container = static::$kernel->getContainer();

        $arr = array();

        foreach($container->getParameter('locales') as $key => $val)
        {
            $arr[] = array('/' . $val . '/service');
        }

        return $arr;
    }
}
