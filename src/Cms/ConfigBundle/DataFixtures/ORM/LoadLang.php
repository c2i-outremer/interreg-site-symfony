<?php

namespace Cms\ConfigBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Cms\ConfigBundle\Entity\Lang;

class LoadLang implements FixtureInterface
{

	public function load(ObjectManager $manager)
	{
		$names = array(
			'fr',
			'en',
			'es',
		);

		foreach ($names as $name)
		{
			$item = new Lang();
			$item->setName($name);
			$manager->persist($item);
		}

		$manager->flush();
	}

}
