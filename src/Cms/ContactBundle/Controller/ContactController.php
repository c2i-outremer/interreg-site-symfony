<?php

namespace Cms\ContactBundle\Controller;

use Cms\ContactBundle\Entity\Contact;
use Cms\ContactBundle\Entity\Newsletter;
use Cms\ContactBundle\Form\ContactType;
use Cms\CoreBundle\Entity\Visite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Uneak\MailjetBundle\Model\EmailUser;

class ContactController extends Controller
{

    public function contactAction(Request $request, $_locale, $standalone = false)
    {
        if (!in_array($_locale, $this->container->getParameter('locales'))) {
            throw new NotFoundHttpException($this->get('translator')->trans('core.not_found.langage'));
        }

        $request->setLocale($_locale);

        $contact = new Contact();
        $form = $this->get('form.factory')->create(new ContactType, $contact);
        $em = $this->getDoctrine()->getManager();

        // get data page
        $contactPage = $em->getRepository('CmsPageBundle:Page')->find(2);

        if ($form->handleRequest($request)->isValid()) {

            $contact->setPublished(false);

            if ($contact->getOptIn()) {
                $newsletter = new Newsletter();
                $newsletter->setFirstname($contact->getFirstName());
                $newsletter->setLastname($contact->getLastName());
                $newsletter->setEmail($contact->getEmail());
                $newsletter->setPublished(true);
                $em->persist($newsletter);
            }

            $em->persist($contact);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', $this->get('translator')->trans('contact.form.well'));

            //------------------------------------------------------------------------------------------------------------------
            //	mail send
            //------------------------------------------------------------------------------------------------------------------
            $config = $this->get('cms_config.GetConfig')->getConfig();

            if ($config->getMailStatus()) {

                $users = $em->getRepository('ApplicationSonataUserBundle:User')->getByGroup('Contact');

                $emails = array();
                foreach ($users as $user) {
                    $emails[] = $user->getEmail();
                }


                $message = \Swift_Message::newInstance()
                    ->setSubject($contact->getSubject())
                    ->setFrom(array($config->getMailSender() => $config->getMailName()))
                    ->setTo($emails)
                    ->setBody($this->renderView(':Contact:send_email.html.twig', array('contact' => $contact)), 'text/html');

                $this->get('mailer')->send($message);
            }

            return $this->redirect($this->generateUrl('cms_contact_contact', array('_locale' => $_locale)));
        }

        // stats
        $page = $em->getRepository('CmsPageBundle:Page')->find(2);
        $page->setNbrViews($page->getNbrViews() + 1);
        $visite = new Visite('page', '/contact', 2);
        $em->persist($visite);
        $em->flush();

        $template = ($standalone) ? ':Contact:contact_standalone.html.twig' : ':Contact:contact.html.twig';

        return $this->render(
            $template,
            array(
                'localSwitcherContact' => true,
                'page' => $contactPage,
                'form' => $form->createView(),
            )
        );
	}

}
