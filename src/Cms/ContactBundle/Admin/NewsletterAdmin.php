<?php

namespace Cms\ContactBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class NewsletterAdmin extends Admin
{

	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'position',
	);

	/**
	 * @param DatagridMapper $datagridMapper
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
				->add('firstname')
				->add('lastname')
				->add('email')
				->add('phone')
				->add('adress')
				->add('cp')
				->add('city')
				->add('country')
				->add('birthdate')
				->add('published')
		;
	}

	/**
	 * @param ListMapper $listMapper
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
				->add('created')
				->add('_action', 'actions', array(
					'actions' => array(
						'edit' => array(),
					)
				))
				->add('published', null, array('editable' => true))
				->add('firstname')
				->add('lastname')
				->add('email')
				->add('phone')
				->add('adress')
				->add('cp')
				->add('city')
				->add('country')
				->add('birthdate')
		;
	}

	/**
	 * @param FormMapper $formMapper
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
				->add('firstname')
				->add('lastname')
				->add('email')
				->add('phone')
				->add('adress')
				->add('cp')
				->add('city')
				->add('country')
				->add('birthdate', 'birthday', array('label' => 'Anniversaire', 'required' => false))
				->add('published')

		;
	}

	/**
	 * @param ShowMapper $showMapper
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
				->add('id')
				->add('firstname')
				->add('lastname')
				->add('email')
				->add('phone')
				->add('adress')
				->add('cp')
				->add('city')
				->add('country')
				->add('birthdate')
				->add('created')
				->add('updated')
				->add('position')
				->add('published')
				->add('name')
				->add('slug')
		;
	}

}
