<?php

namespace Cms\NavBundle\Manager;

use Cms\CoreBundle\Manager\BaseManager;

class MenuManager extends BaseManager
{

    public function findSlugs()
    {
        $slugs = array();
        $menus = $this->repo->findAll();
        foreach ($menus as $menu) {
            $slugs[] = $menu->getSlug();
        }

        return $slugs;
    }

    public function find($slug)
    {
        return $this->repo->findOneBySlug($slug);
    }

}