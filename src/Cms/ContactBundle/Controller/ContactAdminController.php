<?php

namespace Cms\ContactBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;


class ContactAdminController extends CRUDController
{

	public function batchActionPublish(ProxyQueryInterface $query)
	{
		$em = $this->getDoctrine()->getEntityManager();

		foreach ($query->getQuery()->iterate() as $entity)
		{
			$entity[0]->setPublished(!$entity[0]->getPublished());
			$em->persist($entity[0]);
		}
		$em->flush();
		return $this->redirect($this->admin->generateUrl('list'));
	}

}
