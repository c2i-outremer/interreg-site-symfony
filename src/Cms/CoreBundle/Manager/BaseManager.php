<?php

namespace Cms\CoreBundle\Manager;

use Doctrine\ORM\EntityManager;

class BaseManager
{

    protected $repo;
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function persistAndFlush($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function setRepository($repository)
    {
        $this->repo = $this->em->getRepository($repository);
    }

}