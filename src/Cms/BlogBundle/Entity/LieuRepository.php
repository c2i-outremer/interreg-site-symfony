<?php

namespace Cms\BlogBundle\Entity;

use Doctrine\ORM\EntityRepository;

class LieuRepository extends EntityRepository
{

    public function findLieu($_locale, $slug)
    {
        $qb = $this
            ->createQueryBuilder('lieu')
            ->andWhere('lieu.slug = :slug')->setParameter('slug', $slug)
        ;

        $query = $qb->getQuery();

        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $_locale);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK,1);

        return $query->getOneOrNullResult();
    }
}