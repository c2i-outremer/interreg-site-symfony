<?php

namespace Cms\ConfigBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ConfigAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('siteName')
            ->add('siteOnline')
            ->add('editBloc')
            ->add('theme')
            ->add('nbrArticles')
            ->add('googleAnalytics')
            ->add('allowBlogComment')
            ->add('nbrComments')
            ->add('mailStatus')
            ->add('mailSender')
            ->add('mailName')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('siteName')
            ->add('siteOnline')
            ->add('editBloc')
            ->add('theme')
            ->add('nbrArticles')
            ->add('googleAnalytics')
            ->add('allowBlogComment')
            ->add('nbrComments')
            ->add('mailStatus')
            ->add('mailSender')
            ->add('mailName')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Général', array('class' => 'col-md-6'))
                ->add('siteName', null, array('label' => 'Nom du site'))
                ->add('siteOnline', null, array('label' => 'Site en ligne'))
            ->end()
            ->with('Interface', array('class' => 'col-md-6'))
                ->add('editBloc', null, array('label' => 'Editer les blocs en front-office'))
                ->add('theme', null, array('label' => 'Thème'))
            ->end()
            ->with('Blog', array('class' => 'col-md-6'))
                ->add('nbrArticles', null, array('label' => 'Nombre d\'articles dans le blog'))
                ->add('allowBlogComment', null, array('label' => 'Autoriser les commentaires dans les blogs'))
                ->add('nbrComments', null, array('label'=> 'Nombre de commentaires dans les blogs'))
                ->add('allowPublish', null, array('label'=> 'Autoriser les journalistes à publier leur articles'))
            ->end()
            ->with('Communication', array('class' => 'col-md-6'))
                ->add('mailStatus', null, array('label' => 'Activer les mails'))
                ->add('mailSender', null, array('label' => 'Emmeteur du mail'))
                ->add('mailName', null, array('label' => 'Nom de l\'emmeteur'))
            ->end()
            ->with('Indexation', array('class' => 'col-md-12'))
                ->add('googleAnalytics', null, array('label' => 'Code suivi google analytics'))
                ->add('metaRobot', null, array('label' => 'Empecher l\'indexation dans les moteurs de recherche'))
            ->end()

        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('siteName')
            ->add('siteOnline')
            ->add('editBloc')
            ->add('theme')
            ->add('nbrArticles')
            ->add('googleAnalytics')
            ->add('allowBlogComment')
            ->add('nbrComments')
            ->add('mailStatus')
            ->add('mailSender')
            ->add('mailName')
        ;
    }

    public function getTemplate($name)
    {
        switch ($name)
        {
            case 'edit':
                return 'CmsConfigBundle:Admin:config_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }


    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->remove('create');
    }
}
