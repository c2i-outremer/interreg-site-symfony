<?php

namespace Application\Sonata\MediaBundle\Admin;

use Sonata\AdminBundle\Admin\AdminExtension;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class OverrideMediaAdmin extends AdminExtension
{

	public function configureFormFields(FormMapper $formMapper)
	{
		$formMapper->add('tags', null, array('label' => 'Tags'));
		$formMapper->add('description', null, array('label' => 'Description'));
	}
	
	public function configureListFields(ListMapper $listMapper)
	{
		$listMapper->add('tags', null, array('label' => 'Tags'));
	}
	
	public function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper->add('tags', null, array('label' => 'Tags'));
	}

}
