<?php

namespace Cms\ContactBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('firstName', 'text', array('label' => 'contact.form.firstname'))
				->add('lastName', 'text', array('label' => 'contact.form.lastname'))
				->add('email', 'text', array('label' => 'contact.form.email'))
				->add('subject', 'text', array('label' => 'contact.form.object'))
				->add('message', 'textarea', array('label' => 'contact.form.message'))
				//->add('optIn', 'checkbox', array('label' => 'contact.form.offer'))
				->add('save', 'submit', array('label' => 'contact.form.send'))
		;
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Cms\ContactBundle\Entity\Contact'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'cms_contactbundle_contact';
	}

}
