<?php

namespace Cms\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Comment as BaseComment;

/**
 * @ORM\Entity
 * @ORM\Table("cms_comment_comment")

 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Comment extends BaseComment
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * Thread of this comment
	 *
	 * @var Thread
	 * @ORM\ManyToOne(targetEntity="Cms\CommentBundle\Entity\Thread")
	 */
	protected $thread;

	/**
	 * @var boolean
	 * @ORM\Column(name="published", type="boolean", nullable=true)
	 */
	protected $published;

    /**
     * @ORM\Column(name="author", type="string", length=255, nullable=false)
     * @var string
     *
     */

    protected $author;

	public function __construct()
	{
		parent::__construct();
		$this->setPublished(false);
	}

	public function getPublished()
	{
		return $this->published;
	}

	public function setPublished($published)
	{
		$this->published = $published;
		return $this;
	}

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

}
