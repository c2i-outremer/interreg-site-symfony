<?php

namespace Cms\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class CKTemplateCompilerPass implements CompilerPassInterface {

	public function process(ContainerBuilder $container) {		
		
		if ($container->hasDefinition('ivory_ck_editor.form.type') === false) {
			return;
		}

		$ivory_ck_editor_form_type_definition = $container->getDefinition('ivory_ck_editor.form.type');
		$ivory_ck_editor_form_type_definition->addMethodCall('setTemplateManager', array(new Reference('cms_core.CKTemplateManager')));
		
	}

}
