<?php

namespace Cms\CoreBundle\Controller;

use Cms\BlogBundle\Entity\Article;
use Cms\BlogBundle\Entity\Category;
use Cms\BlogBundle\Entity\Field;
use Cms\BlogBundle\Entity\Tag;
use Doctrine\ORM\EntityManager;
use joshtronic\LoremIpsum;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class PopulateDatabaseController extends Controller
{
    private $lipsum;

    public function __construct()
    {
        $this->lipsum = new LoremIpsum();
        $this->lipsum->word();

        //LOREM
        //Noms
        define("L_N_MIN", "1");
        define("L_N_MAX", "1");

        //Titres
        define("L_T_MIN", "1");
        define("L_T_MAX", "5");

        //Paragraphes
        //SMALL
        define("L_P_S_MIN", "1");
        define("L_P_S_MAX", "2");
        //MEDIUM
        define("L_P_M_MIN", "2");
        define("L_P_M_MAX", "3");
        //LARGE
        define("L_P_L_MIN", "2");
        define("L_P_L_MAX", "4");

        //ENTITES
        define("MIN_NB_FIELDS", "1");
        define("MAX_NB_FIELDS", "4");

        define("MIN_NB_M_C", "1");
        define("NB_M_C", "10");

        define("NB_CAT", "5");

        define("NB_ART", "7");
    }

    public function populateAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        //Génération de catégories
        $categories = array();
        for ($i = 0; $i < NB_CAT; $i++) {
            $categories[$i] = $this->generateCategory($em);
        }

        //Génération de mots clés
        $tags = array();
        for ($i = 0; $i < NB_M_C; $i++) {
            $tags[$i] = $this->generateTag($em);
        }

        //Génération d'articles
        $articles = array();
        for ($i = 0; $i < NB_ART; $i++) {
            ld($articles[$i] = $this->generateArticle($em, $categories, $tags));
        }

        return $this->render('CmsCoreBundle:PopulateDatabase:result.html.twig', array());
    }

    private function generateName($locale)
    {
        return $locale . ' ' . $this->lipsum->words(rand(L_N_MIN, L_N_MAX));
    }
    private function generateTitle($locale)
    {
        return $locale . ' ' . $this->lipsum->words(rand(L_T_MIN, L_T_MAX));
    }

    private function generateShortContent($locale)
    {
        return $this->lipsum->paragraphs(L_P_S_MIN, '<p>' . $locale . ' $1</p>');
    }

    private function generateContent($locale)
    {
        return $this->lipsum->paragraphs(L_P_M_MIN, '<p>' . $locale . ' $1</p>');
    }

    private function persistAndFlush(EntityManager $em, $object)
    {
        $em->persist($object);
        $em->flush($object);
    }

    private function generateCategory(EntityManager $em)
    {
        $category = new Category();

        $locales = $this->container->getParameter('locales');
        foreach ($locales as $locale) {
            $this->generateCategoryTranlation($em, $category, $locale);
        }
        return $category;
    }

    private function generateTag(EntityManager $em)
    {
        $tag = new Tag();

        $locales = $this->container->getParameter('locales');
        foreach ($locales as $locale) {
            $this->generateTagTranlation($em, $tag, $locale);
        }
        return $tag;
    }

    private function generateArticle(EntityManager $em, $categories, $tags)
    {
        $article = new Article();
        $article->setEnableComment(true);

        $nbFields = rand(MIN_NB_FIELDS, MAX_NB_FIELDS);
        for ($i = 0; $i < $nbFields; $i++)
        {
            $article->addField($this->generateField($em));
        }

        $article->setCategory($categories[rand(0, NB_CAT - 1)]);

        $nbTags = rand(MIN_NB_M_C, NB_M_C);
        $deb = rand(0, NB_CAT - 1);
        for($i = 0; $i < $nbTags; $i++)
        {
            $article->addTag($tags[ ($i + $deb) % NB_M_C]);
        }

        $article->setAuthor($this->generateName(''));

        $locales = $this->container->getParameter('locales');
        foreach ($locales as $locale) {
            $this->generateArticleTranlation($em, $article, $locale);
        }

        return $article;
    }

    private function generateField(EntityManager $em)
    {
        $field = new Field();

        $locales = $this->container->getParameter('locales');
        foreach ($locales as $locale) {
            $this->generateFieldTranlation($em, $field, $locale);
        }

        return $field;
    }

    private function generateCategoryTranlation(EntityManager $em, Category $categorie, $locale)
    {
        $categorie->setLocale($locale);
        $categorie->setName($this->generateName($locale));
        echo $categorie->getName() . '</br>';

        echo '</br>';

        $this->persistAndFlush($em, $categorie);
    }

    private function generateTagTranlation(EntityManager $em, Tag $tag, $locale)
    {
        $tag->setLocale($locale);
        $tag->setName($this->generateName($locale));
        echo $tag->getName() . '</br>';

        echo '</br>';

        $this->persistAndFlush($em, $tag);
    }

    private function generateArticleTranlation(EntityManager $em, Article $article, $locale)
    {
        $article->setLocale($locale);
        $article->setPublished(true);
        $article->setName($this->generateTitle($locale));
        echo $article->getName() . '</br>';
        $article->setShortContent($this->generateShortContent($locale));
        echo $article->getShortContent() . '</br>';
        $article->setContent($this->generateContent($locale));
        echo $article->getContent() . '</br>';

        echo '</br>';

        $this->persistAndFlush($em, $article);
    }

    private function generateFieldTranlation(EntityManager $em, Field $field, $locale)
    {
        $field->setLocale($locale);
        $field->setName($this->generateTitle($locale));
        echo $field->getName() . '</br>';
        $field->setValue($this->generateShortContent($locale));
        echo $field->getValue() . '</br>';

        echo '</br>';

        $this->persistAndFlush($em, $field);
    }

}
