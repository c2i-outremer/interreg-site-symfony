<?php

namespace Cms\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FeedController extends Controller
{

	public function rssAction(Request $request, $_locale)
	{
        if (!in_array($_locale, $this->container->getParameter('locales'))) {
            throw new NotFoundHttpException($this->get('translator')->trans('core.not_found.langage'));
        }

        $request->setLocale($_locale);

		$posts = $this->getDoctrine()->getRepository('CmsBlogBundle:Article')->findArticle(array(), null, 0, 100, $_locale, false);


        $pubDate = new \DateTime();
		
		return $this->render('CmsBlogBundle:Feed:rss.xml.twig', array(
            'link' => $this->generateUrl('cms_blog_feed',array('_locale' => $_locale),true),
            'pubDate' => $pubDate,
			'posts' => $posts,
            '_locale' => $_locale,
		));
	}

}
