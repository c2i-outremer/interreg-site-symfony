<?php

namespace Cms\CollectionBundle\Services\Twig;


use Symfony\Component\DependencyInjection\ContainerInterface;

class showCollection extends \Twig_Extension
{
    protected $container;
    protected $templating;

    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    public function getName()
    {
        return 'showCollection';
    }

    public function getFunctions()
    {
        return array(
            'showCollection' => new \Twig_Function_Method($this, 'showCollection'),
        );
    }

    public function showCollection($slug)
    {

        $collection = $this->container->get('doctrine.orm.entity_manager')->getRepository('CmsCollectionBundle:Collection')->findCollection($slug);

        $items = array();

        if (!$collection) {
            return false;
        }


        foreach ($collection->getCollectionItems()->toArray() as $collectionItems) {
            $items[] = $collectionItems->getItem();
        }

        if (!$collection->getCollectionType()->getDynamic()) {
            return $this->container->get('templating')->render(
                $collection->getCollectionType()->getTemplatePath(),
                array(
                    'collection' => $collection,
                    'items' => $items
                )
            );
        }


        return $this->container->get('templating')->render(
            'CmsCollectionBundle::collection.html.twig',
            array(
                'collection' => $collection,
                'items' => $items
            )
        );

    }
}