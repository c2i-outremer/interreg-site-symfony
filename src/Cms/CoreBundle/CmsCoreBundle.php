<?php

namespace Cms\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Cms\CoreBundle\DependencyInjection\Compiler\CKTemplateCompilerPass;

class CmsCoreBundle extends Bundle
{
		public function build(ContainerBuilder $container) {
		parent::build($container);
		$container->addCompilerPass(new CKTemplateCompilerPass());
	}
}
