<?php

namespace Cms\GroupBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\Publishable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * group
 *
 * @ORM\Table("cms_group_group")
 * @ORM\Entity(repositoryClass="Cms\GroupBundle\Entity\GroupRepository")
 */
class Group
{

    use Timestampable;
    use Nameable;
    use Publishable;
    use Positionable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\BlocBundle\Entity\Bloc", mappedBy="group")

	 * */
	private $blocs;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\GroupBundle\Entity\GroupType", inversedBy="groups")
	 * @ORM\JoinColumn(name="groupType", referencedColumnName="id", nullable=false)
	 * */
	private $groupType;

	/**
	 * @ORM\OrderBy({"position" = "ASC"})
	 * @ORM\OneToMany(targetEntity="Cms\GroupBundle\Entity\Item", mappedBy="group", cascade={"persist"}, orphanRemoval=true)
	 * */
	private $items;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->blocs = new \Doctrine\Common\Collections\ArrayCollection();
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Add blocs
	 *
	 * @param \Cms\BlocBundle\Entity\Bloc $blocs
	 * @return Group
	 */
	public function addBloc(\Cms\BlocBundle\Entity\Bloc $blocs)
	{
		$this->blocs[] = $blocs;
		$blocs->setGroup($this);
		return $this;
	}

	/**
	 * Remove blocs
	 *
	 * @param \Cms\BlocBundle\Entity\Bloc $blocs
	 */
	public function removeBloc(\Cms\BlocBundle\Entity\Bloc $blocs)
	{
		$this->blocs->removeElement($blocs);
	}

	/**
	 * Get blocs
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getBlocs()
	{
		return $this->blocs;
	}

	/**
	 * Set groupType
	 *
	 * @param \Cms\GroupBundle\Entity\GroupType $groupType
	 * @return Group
	 */
	public function setGroupType(\Cms\GroupBundle\Entity\GroupType $groupType)
	{
		$this->groupType = $groupType;

		return $this;
	}

	/**
	 * Get groupType
	 *
	 * @return \Cms\GroupBundle\Entity\GroupType 
	 */
	public function getGroupType()
	{
		return $this->groupType;
	}

	/**
	 * Add items
	 *
	 * @param \Cms\GroupBundle\Entity\Item $items
	 * @return Group
	 */
	public function addItem(\Cms\GroupBundle\Entity\Item $items)
	{
		$this->items[] = $items;
		$items->setGroup($this);
		return $this;
	}

	/**
	 * Remove items
	 *
	 * @param \Cms\GroupBundle\Entity\Item $items
	 */
	public function removeItem(\Cms\GroupBundle\Entity\Item $items)
	{
		$this->items->removeElement($items);
	}

	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}

}
