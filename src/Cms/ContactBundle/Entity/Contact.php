<?php

namespace Cms\ContactBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\Publishable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contact
 *
 * @ORM\Table("cms_contact_contact")
 * @ORM\Entity(repositoryClass="Cms\ContactBundle\Entity\ContactRepository")
 */
class Contact
{

    use Timestampable;
    use Publishable;


    /**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="first_name", type="string", length=255)
	 * @Assert\NotNull()
	 */
	private $firstName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="last_name", type="string", length=255)
	 * @Assert\NotNull()
	 */
	private $lastName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255)
	 * @Assert\NotNull()
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="subject", type="string", length=255)
	 * @Assert\NotNull()
	 */
	private $subject;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="message", type="text")
	 * @Assert\NotNull()
	 */
	private $message;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="opt_in", type="boolean", nullable=true)
	 */
	private $optIn;
	
	public function __construct()
	{
		$this->optIn = true;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set firstName
	 *
	 * @param string $firstName
	 * @return Contact
	 */
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;

		return $this;
	}

	/**
	 * Get firstName
	 *
	 * @return string 
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * Set lastName
	 *
	 * @param string $lastName
	 * @return Contact
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;

		return $this;
	}

	/**
	 * Get lastName
	 *
	 * @return string 
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 * @return Contact
	 */
	public function setEmail($email)
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string 
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set message
	 *
	 * @param string $message
	 * @return Contact
	 */
	public function setMessage($message)
	{
		$this->message = $message;

		return $this;
	}

	/**
	 * Get message
	 *
	 * @return string 
	 */
	public function getMessage()
	{
		return $this->message;
	}

	public function getSubject()
	{
		return $this->subject;
	}

	public function setSubject($subject)
	{
		$this->subject = $subject;
		return $this;
	}

	function getOptIn()
	{
		return $this->optIn;
	}

	function setOptIn($optIn)
	{
		$this->optIn = (bool) $optIn;
		return $this;
	}

}
