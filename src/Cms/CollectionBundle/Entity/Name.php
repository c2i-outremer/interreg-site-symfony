<?php

namespace Cms\CollectionBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Name
 *
 * @ORM\Table("cms_collection_name")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="Cms\CollectionBundle\Entity\NameTranslation")
 */
class Name extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\NameTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Cms\CollectionBundle\Entity\Item", inversedBy="names")
     * @ORM\JoinColumn(name="item", referencedColumnName="id")
     */

    protected $item;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Name
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set item
     *
     * @param \Cms\CollectionBundle\Entity\Item $item
     *
     * @return Name
     */
    public function setItem(\Cms\CollectionBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Cms\CollectionBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
