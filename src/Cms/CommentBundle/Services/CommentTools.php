<?php

namespace Cms\CommentBundle\Services;


use FOS\CommentBundle\Entity\CommentManager;
use FOS\CommentBundle\Entity\ThreadManager;
use Symfony\Component\HttpFoundation\RequestStack;

class CommentTools
{

    protected $commentManager;
    protected $threadManager;
    protected $request;

    function __construct(CommentManager $commentManager, ThreadManager $threadManager, RequestStack $request)
    {
        $this->commentManager = $commentManager;
        $this->threadManager = $threadManager;
        $this->request = $request;
    }

    public function getThreadComment($id)
    {

        $thread = $this->threadManager->findThreadById($id );
        if (null === $thread) {
            $thread = $this->threadManager->createThread();
            $thread->setId($id);
            $thread->setPermalink($this->request->getCurrentRequest()->getUri());
            $this->threadManager->saveThread($thread);
        }
        $comments = $this->commentManager->findCommentTreeByThread($thread);

        return array(
            'thread' => $thread,
            'comments' => $comments
        );
    }


}