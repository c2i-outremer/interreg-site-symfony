<?php

namespace Cms\PageBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class PageAdmin extends Admin
{

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('anonymous')
            ->add('published')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array('label' => 'Nom', 'editable' => true))
            ->add('anonymous', null, array('editable' => true, 'label' => 'Anonyme'))
            ->add('published', null, array('editable' => true, 'label' => 'Publication'))
            ->add('pageModel', null, array('label' => 'Type'))
            ->add('nbrViews', null, array('label' => 'Visite'))
            ->add('language', 'string', array('template' => 'CmsCoreBundle:Admin:selected_lang.html.twig', 'label' => 'Langue'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'stat' => array('template' => 'CmsCoreBundle:Admin:button_stat.html.twig'),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $allowAccess = $container->get('cms_core.allow_access')->check('ROLE_CMS_PAGE_ADMIN_PAGE_MASTER');

        $formMapper
            ->tab('Général')
                ->with('Identification', array('class' => 'col-md-6'))
                    //identification
                    ->add('name', 'text', array('label' => 'Nom', 'help' => 'trans', 'attr' => array('autofocus' => 'autofocus')))
                    ->add('slug', 'text', array('label' => 'Slug', 'required' => false, 'help' => 'trans'))
                ->end()
                ->with('Status', array('class' => 'col-md-6'))
                    // status
                    ->add('published', 'checkbox', array('label' => 'Publication', 'required' => false, 'attr'=> array('data-sonata-select2'=>'false'),'label_attr' => array('class' => 'help-trans')))
                    ->add('anonymous', 'checkbox', array('label' => 'Accesible aux anonymes', 'required' => false))
                    ->add('nbrViews', 'text', array('label' => 'Visite', 'disabled' => true, 'required' => false))
                ->end()
            ->end()
            ->tab('Référencement')
            // Référencement
                ->with('Référencement', array('class' => 'col-md-12'))
                    ->add('folder', 'sonata_type_model_list', array('required' => false, 'label' => 'Dossier de stockage virtuel'))
                    ->add('metaTitle', null, array('help' => 'trans'))
                    ->add('metaH1', null, array('help' => 'trans'))
                    ->add('showMetaH1', null, array('label' => 'Afficher la meta H1'))
                    ->add('metaDescription', null, array('help' => 'trans'))
                    ->add('metaKeyword', null, array('help' => 'trans'))
                    ->add('otherMeta', 'textarea', array('label' => 'Autre Meta données', 'required' => false))
                ->end()
            ->end();
        if($allowAccess) {
            $formMapper
                ->tab('Type de page')
                    ->with('Type', array('class' => 'col-md-12'))
                        ->add('pageModel', 'choice', array(
                            'label' => 'Type de page',
                            'attr'=> array('data-sonata-select2'=>'false'),
                            'choices' => array(
                                '' =>               '----',
                                'BUILDER' =>        'Page Builder',
                                'CONTROLLER' =>     'Contrôleur',
                                'TEMPLATE' =>       'Template',
                            )
                        ))
                    ->end()
                    ->with('Page Builder', array('class' => 'page-builder col-md-12'))
                        ->add('pageBlocs', 'sonata_type_collection', array('label' => 'Bloc de page','required' => false,'by_reference' => false,'type_options' => array('delete' => true,)), array('edit' => 'inline','inline' => 'table','sortable' => 'position',))
                    ->end()
                    ->with('Contrôleur', array('class' => 'page-controller col-md-12'))
                        ->add('controller', 'sonata_type_model_list', array('label' => 'Type de contrôleur', 'required' => false))
                        ->add('query', 'sonata_type_model_list', array('label' => 'Argument de contrôleur', 'required' => false))
                    ->end()
                    ->with('Template', array('class' => 'page-template col-md-12'))
                        ->add('pageType', 'sonata_type_model_list', array('required' => false, 'label' => 'Template'))
                    ->end()
                ->end()
            ;
        }


    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('content')
            ->add('script')
            ->add('css')
            ->add('metaTitle')
            ->add('metaDescription')
            ->add('metaKeyword')
            ->add('anonymous')
            ->add('created')
            ->add('updated')
            ->add('position')
            ->add('published')
            ->add('name')
            ->add('slug');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('stat', $this->getRouterIdParameter() . '/stat');
        $collection->add('result', $this->getRouterIdParameter() . '/result');
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CmsPageBundle:Admin:page_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
