<?php

namespace Extra\ServiceBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ServiceAdmin extends Admin
{
    public $last_position = 0;
    private $positionService;

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'position',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('cover', null, array('label' => 'Couverture'))
            ->add('name', null, array('label' => 'Nom'))
            ->add('top', null, array('label' => 'Service à la une'))
            ->add('published', null, array('label' => 'Publication'))
            ->add('created', null, array('label' => 'Date de création'))
            ->add('updated', null, array('label' => 'Date de mise à jour'))
            ->add('numberViews', null, array('label' => 'Nombre de vues'));
            //->add('language', null, array( 'label' => 'Langue(s) de publication'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('cover', null, array('template' => 'CmsCoreBundle:Admin:list_image.html.twig', 'label' => 'Couverture'))
            ->add('name', null, array('label' => 'Nom', 'editable' => true))
            ->add('published', null, array('label' => 'Publication', 'editable' => true))
            ->add('top', null, array('label' => 'Service à la une', 'editable' => true))
            ->add('created', null, array('label' => 'Date de création'))
            ->add('updated', null, array('label' => 'Date de mise à jour'))
            ->add('numberViews', null, array('label' => 'Nombre de vues'))
            ->add('language', 'string', array('template' => 'CmsCoreBundle:Admin:selected_lang.html.twig', 'label' => 'Langue(s) de publication'))
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                        'move' => array('template' => 'CmsCoreBundle:Admin:sort_reverse.html.twig'),
                    )
                )
            );
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Contenu', array('class' => 'col-md-8 left-col'))
                ->add('name', 'text', array('label' => 'Nom du service', 'help' => 'trans', 'attr' => array('autofocus' => 'autofocus')))
                ->add('shortcontent', 'ckeditor', array('required' => false, 'help' => 'trans', 'label' => 'Chapeau'))
                ->add('content', 'ckeditor', array('required' => false, 'help' => 'trans', 'label' => 'Contenu principal'))
                ->add('fields','sonata_type_collection',array('required' => false, 'help' => 'trans', 'by_reference' => false, 'label' => 'Contenu(s) supplémentaire(s)', 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            ->end()
            ->with('Propriétés', array('class' => 'col-md-4 right-col'))
                ->add('cover', 'sonata_type_model_list',array('required' => false, 'label' => false),array('link_parameters' => array('context' => 'picture', 'provider' => 'sonata.media.provider.image')))
                ->add('html', 'sonata_type_model_list',array('required' => false, 'label' => false))
                ->add('top', null, array('label' => 'Service à la une', 'required' => false))
                ->add('published', null, array('label' => 'Publication', 'label_attr' => array('class' => 'help-trans')))
                ->add('slug', null, array( 'help' => 'trans','attr' => array('placeholder' => 'Laissez vide par défaut')))
                ->add('created', 'sonata_type_date_picker', array('label' => 'Date de création', 'disabled' => true, 'required' => false))
                ->add('updated', 'sonata_type_date_picker', array('label' => 'Date de mise à jour', 'disabled' => true, 'required' => false))
                ->add('numberViews', null, array('label' => 'Nombre de vues', 'disabled' => true, 'required' => false))
                //->add('language', null, array( 'label' => 'Langue(s) de publication'))
                ->add('pictures', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
                ->add('videos','sonata_type_collection',array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
                ->add('files','sonata_type_collection',array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            ->end();
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name', 'text', array('label' => 'Nom du service'))
            ->add('shortcontent', 'ckeditor', array('label' => 'Chapeau'))
            ->add('content', 'ckeditor', array('label' => 'Contenu principal'))
            //->add('fields','sonata_type_collection',array('by_reference' => false, 'label' => 'Contenu(s) supplémentaire(s)'))
            ->add('cover', 'sonata_type_model_list',array('label' => 'Image à la une'),array('link_parameters' => array('context' => 'picture', 'provider' => 'sonata.media.provider.image')))
            ->add('top', null, array('label' => 'Service à la une'))
            ->add('published', null, array('label' => 'Publication'))
            ->add('slug')
            ->add('created', null, array('label' => 'Date de création'))
            ->add('updated', null, array('label' => 'Date de mise à jour'))
            ->add('numberViews', null, array('label' => 'Nombre de vues'));
            //->add('language', null, array( 'label' => 'Langue(s) de publication'));
            //->add('pictures', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => 'Images', 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            //->add('videos','sonata_type_collection',array('required' => false, 'by_reference' => false, 'label' => 'Vidéos', 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            //->add('files','sonata_type_collection',array('required' => false, 'by_reference' => false, 'label' => 'Fichiers', 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',));
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'ExtraServiceBundle:Admin:service_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }





}
