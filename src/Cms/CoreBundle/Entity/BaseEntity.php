<?php

namespace Cms\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/** @ORM\MappedSuperclass */
abstract class BaseEntity
{

	/**
	 * @var \DateTime
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(name="created", type="datetime", nullable=true)
	 */
	private $created;

	/**
	 * @var \DateTime
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(name="updated", type="datetime", nullable=true)
	 */
	private $updated;

	/**
	 * @var integer
	 * @Gedmo\SortablePosition
	 * @ORM\Column(name="position", type="integer", nullable=true)
	 */
	private $position;

	/**
	 * @var boolean
	 * @ORM\Column(name="published", type="boolean", nullable=true)
	 */
	private $published;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 * @Gedmo\Slug(fields={"name"})
	 * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=true)
	 */
	private $slug;


	
	/**
	 * constructor
	 */

	public function __construct()
	{
		$this->published = true;
	}
	
	/**
	 * toString
	 * @return type
	 */
	
	public function __toString()
	{
		return $this->getName().' ('.$this->getId().')';
	}

	/**
	 * Set created
	 *
	 * @param \DateTime $created
	 * @return menu
	 */
	public function setCreated($created)
	{
		$this->created = $created;

		return $this;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime 
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * Set updated
	 *
	 * @param \DateTime $updated
	 * @return menu
	 */
	public function setUpdated($updated)
	{
		$this->updated = $updated;

		return $this;
	}

	/**
	 * Get updated
	 *
	 * @return \DateTime 
	 */
	public function getUpdated()
	{
		return $this->updated;
	}

	/**
	 * Set position
	 *
	 * @param integer $position
	 * @return menu
	 */
	public function setPosition($position)
	{
		$this->position = $position;

		return $this;
	}

	/**
	 * Get position
	 *
	 * @return integer 
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * Set published
	 *
	 * @param boolean $published
	 * @return menu
	 */
	public function setPublished($published)
	{
		$this->published = $published;

		return $this;
	}

	/**
	 * Get published
	 *
	 * @return boolean 
	 */
	public function getPublished()
	{
		return $this->published;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return menu
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set slug
	 *
	 * @param string $slug
	 * @return menu
	 */
	public function setSlug($slug)
	{
		$this->slug = $slug;

		return $this;
	}

	/**
	 * Get slug
	 *
	 * @return string 
	 */
	public function getSlug()
	{
		return $this->slug;
	}
}
