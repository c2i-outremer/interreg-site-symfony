<?php

namespace Cms\ControllerBundle\Services;

class GetDataQuery
{

	public function getDataQuery($query)
	{
		$r = array();
		foreach ($query as $item)
		{
			$r[$item->getName()] = $item->getValue();
		}
		return $r;
	}

}
