<?php

namespace AppBundle\Entity\Project;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Axe
 *
 * @ORM\Table("app_project_axe")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Project\AxeRepository")
 * @Gedmo\TranslationEntity(class="AppBundle\Entity\Project\AxeTranslation")
 */
class Axe extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use NameTranslatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Project\AxeTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Project\Project", mappedBy="axe")
     * */
    private $projects;
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->projects = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Axe
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Remove translation
     *
     * @param \AppBundle\Entity\Project\AxeTranslation $translation
     */
    public function removeTranslation(\AppBundle\Entity\Project\AxeTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Add project
     *
     * @param \AppBundle\Entity\Project\Project $project
     *
     * @return Axe
     */
    public function addProject(\AppBundle\Entity\Project\Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \AppBundle\Entity\Project\Project $project
     */
    public function removeProject(\AppBundle\Entity\Project\Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }
}
