<?php

namespace Cms\PrehomeBundle\Services\Twig;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;

class ShowPrehome extends \Twig_Extension
{
    protected $manager;
    protected $twig;
    protected $request;

    function __construct(EntityManager $manager, \Twig_Environment $twig, RequestStack $request)
    {
        $this->manager = $manager;
        $this->twig = $twig;
        $this->request = $request;
    }


    public function getName()
    {
        return 'showPrehome';
    }

    public function getFunctions()
    {
        return array(
            'showPrehome' => new \Twig_Function_Method($this, 'showPrehome')
        );
    }


    public function showPrehome()
    {

        $session = $this->request->getCurrentRequest()->getSession();

        $prehomes = $this->manager->getRepository('CmsPrehomeBundle:Prehome')->getFirstPrehome();

        if (!empty($prehomes) and !$session->get('showPrehome'))
        {
            $session->set('showPrehome', true);

            return $this->twig->render(':Prehome:prehome.html.twig', array(
                'prehome' => $prehomes[0],
            ));
        }
        else
        {
            return $this->twig->render(':Prehome:prehome.html.twig', array(
            ));
        }
    }

}