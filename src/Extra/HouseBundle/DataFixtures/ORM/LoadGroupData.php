<?php

namespace Extra\HouseBundle\DataFixtures\ORM;

use Application\Sonata\UserBundle\Entity\Group;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadGroupData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $types = array('EDIT', 'LIST', 'CREATE', 'VIEW', 'DELETE', 'EXPORT', 'OPERATOR');

        $datas = array(
            array(
                'name' => 'Logement',
                'roles' => array(
                    'ROLE_EXTRA_HOUSE_ADMIN_HOUSE',
                    'ROLE_EXTRA_HOUSE_ADMIN_CONTENT',
                    'ROLE_EXTRA_HOUSE_ADMIN_EQUIPMENT',
                    'ROLE_EXTRA_HOUSE_ADMIN_PICTURE',
                    'ROLE_EXTRA_HOUSE_ADMIN_FILE',
                    'ROLE_EXTRA_HOUSE_ADMIN_VIDEO',
                    'ROLE_EXTRA_HOUSE_ADMIN_RESERVATION',
                    'ROLE_EXTRA_HOUSE_ADMIN_PRICE',
                )
            ),
        );

        foreach ($datas as $data) {
            $roles = array();
            $roles[] = 'ROLE_ADMIN';

            foreach ($data['roles'] as $role) {
                foreach ($types as $type) {
                    $roles[] = $role.'_'.$type;
                }
            }

            if (isset($data['extraRoles'])) {
                foreach ($data['extraRoles'] as $role) {
                    $roles[] = $role;
                }
            }

            $group = new Group($data['name'], $roles);
            $manager->persist($group);
            $manager->flush();
        }
    }

}
