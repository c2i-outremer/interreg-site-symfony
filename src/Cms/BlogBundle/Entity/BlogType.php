<?php

namespace Cms\BlogBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Doctrine\ORM\Mapping as ORM;

/**
 * BlogType
 *
 * @ORM\Table("cms_blog_blog_type")
 * @ORM\Entity(repositoryClass="Cms\BlogBundle\Entity\BlogTypeRepository")
 */
class BlogType
{

    use Nameable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="template_path", type="text")
	 */
	protected $templatePath;

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set templatePath
	 *
	 * @param string $templatePath
	 * @return BlocType
	 */
	public function setTemplatePath($templatePath)
	{
		$this->templatePath = $templatePath;

		return $this;
	}

	/**
	 * Get templatePath
	 *
	 * @return string 
	 */
	public function getTemplatePath()
	{
		return $this->templatePath;
	}

}
