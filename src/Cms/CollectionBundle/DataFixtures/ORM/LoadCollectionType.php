<?php

namespace Cms\CollectionBundle\DataFixtures\ORM;

use Cms\CollectionBundle\Entity\CollectionType;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCollectionType implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    public function load(ObjectManager $manager)
    {
        $templating = $this->container->get('templating');

        $types = array(
            array(
                'name' => 'Default',
                'collectionContent' => $templating->render('CmsCollectionBundle:Fixtures/Default:collection_content.html.twig'),
                'itemContent' => null,
                'script' => null,
                'style' => null,
            ),
            array(
                'name' => 'Carroussel Bootstrap',
                'collectionContent' => $templating->render('CmsCollectionBundle:Fixtures/BootstrapCarrousel:collection_content.html.twig'),
                'itemContent' => $templating->render('CmsCollectionBundle:Fixtures/BootstrapCarrousel:item_content.html.twig'),
                'script' => null,
                'style' => null,
            ),
            array(
                'name' => 'Carroussel OWL',
                'collectionContent' => $templating->render('CmsCollectionBundle:Fixtures/OwlCarroussel:collection_content.html.twig'),
                'itemContent' => $templating->render('CmsCollectionBundle:Fixtures/OwlCarroussel:item_content.html.twig'),
                'script' => $templating->render('CmsCollectionBundle:Fixtures/OwlCarroussel:script.html.twig'),
                'style' => null,
            ),
        );

        foreach ($types as $type) {
            $data = new CollectionType;
            $data->setName($type['name']);
            $data->setCollectionContent($type['collectionContent']);
            $data->setItemContent($type['itemContent']);
            $data->setScript($type['script']);
            $data->setStyle($type['style']);
            $manager->persist($data);
        }

        $manager->flush();
    }

}
