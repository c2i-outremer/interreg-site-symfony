<?php

namespace Cms\BlogBundle\Services;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Cms\BlogBundle\Entity\Article;

class Feed
{

	protected $container;
	protected $nbr_post_feed;

	public function __construct(ContainerInterface $container, $nbr_post_feed)
	{
		$this->container = $container;
		$this->nbr_post_feed = $nbr_post_feed;
	}

	public function postPersist(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();

		if (!$entity instanceof Article)
		{
			return;
		}

		$this->writeFeed();
	}

	public function postUpdate(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();

		if (!$entity instanceof Article)
		{
			return;
		}

		$this->writeFeed();
	}

	public function writeFeed()
	{
		$em = $this->container->get('doctrine')->getManager();
		$posts = $em->getRepository('CmsBlogBundle:Article')->findArticle(array(), 0, $this->nbr_post_feed);

		$pubDate = new \DateTime();

		$content = $this->container->get('templating')->renderResponse('CmsBlogBundle:Feed:rss.xml.twig', array(
			'posts' => $posts,
			'pubDate' => $pubDate,
		));

		file_put_contents(dirname(__FILE__) . '/../../../../web/uploads/xml/feed.xml', $content->getContent());
	}

}
