<?php
/**
 * User: ed
 * Date: 20/07/15
 * Time: 14:07
 */

namespace Cms\CoreBundle\Services;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;


class allowAccess
{

    protected $checker;

    public function __construct(AuthorizationChecker $checker)
    {
        $this->checker = $checker;
    }

    public function check($string)
    {
        return ($this->checker->isGranted($string) || $this->checker->isGranted('ROLE_SUPER_ADMIN'));
    }

}