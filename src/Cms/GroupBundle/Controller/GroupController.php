<?php

namespace Cms\GroupBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GroupController extends Controller
{
	//------------------------------------------------------------------------------------------------------------------
	//	showAction
	//------------------------------------------------------------------------------------------------------------------

	/**
	 *  show a group
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param type $slug
	 * @param type $otherParams
	 * @return type
	 */
	public function showAction(Request $request, $slug, $otherParams = null)
	{
		$em = $this->getDoctrine()->getManager();
		// get data Group
		$group = $em->getRepository('CmsGroupBundle:Group')->getGroup($slug);

        return $this->render($group->getGroupType()->getTemplatePath(), array(
					'items' => $group->getItems()->toArray(),
					'getQuery' => $request->query,
					'otherParams' => $otherParams,
		));
	}
	
	//------------------------------------------------------------------------------------------------------------------
	//	showInblocAction
	//------------------------------------------------------------------------------------------------------------------
	
	/**
	 *  show a group in a bloc
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param array $bloc
	 * @param type $otherParams
	 * @return type
	 */

	public function showInblocAction(Request $request, $bloc, $otherParams= null)
	{
        $bloc = $this->getDoctrine()->getManager()->getRepository('CmsBlocBundle:Bloc')->getBloc((int) $bloc);
		return $this->showAction($request, $bloc->getGroup()->getSlug(), $otherParams);
	}

}
