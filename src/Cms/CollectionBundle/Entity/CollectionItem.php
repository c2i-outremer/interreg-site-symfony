<?php

namespace Cms\CollectionBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\ORM\Mapping as ORM;

/**
 * CollectionItem
 *
 * @ORM\Table("cms_collection_collection_item")
 * @ORM\Entity
 */
class CollectionItem
{

    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\CollectionBundle\Entity\Collection", inversedBy="collectionItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $collection;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\CollectionBundle\Entity\Item", inversedBy="collectionItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set collection
     *
     * @param \Cms\CollectionBundle\Entity\Collection $collection
     *
     * @return CollectionItem
     */
    public function setCollection(\Cms\CollectionBundle\Entity\Collection $collection)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get collection
     *
     * @return \Cms\CollectionBundle\Entity\Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set item
     *
     * @param \Cms\CollectionBundle\Entity\Item $item
     *
     * @return CollectionItem
     */
    public function setItem(\Cms\CollectionBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Cms\CollectionBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
