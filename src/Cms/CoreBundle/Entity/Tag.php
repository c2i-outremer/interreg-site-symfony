<?php

namespace Cms\CoreBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Doctrine\ORM\Mapping as ORM;

/**
 * CKTemplate
 *
 * @ORM\Table("cms_core_tag")
 * @ORM\Entity()
 */
class Tag
{

    use Nameable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	public function getId()
	{
		return $this->id;
	}

}
