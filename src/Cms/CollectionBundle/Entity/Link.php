<?php

namespace Cms\CollectionBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Link
 *
 * @ORM\Table("cms_collection_link")
 * @ORM\Entity
 */
class Link
{
    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="text", nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="anchor", type="string", length=255, nullable=true)
     */
    private $anchor;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=255, nullable=true)
     */
    private $class;

    /**
     * @var boolean
     *
     * @ORM\Column(name="target", type="boolean", nullable=true)
     */
    private $target = true;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Cms\CollectionBundle\Entity\Item", inversedBy="links")
     * @ORM\JoinColumn(name="item", referencedColumnName="id")
     */

    protected $item;

    public function __construct() {
        $this->target = true;
        $this->class = 'btn btn-default';
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Link
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set anchor
     *
     * @param string $anchor
     *
     * @return Link
     */
    public function setAnchor($anchor)
    {
        $this->anchor = $anchor;

        return $this;
    }

    /**
     * Get anchor
     *
     * @return string
     */
    public function getAnchor()
    {
        return $this->anchor;
    }

    /**
     * Set class
     *
     * @param string $class
     *
     * @return Link
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set target
     *
     * @param boolean $target
     *
     * @return Link
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return boolean
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set item
     *
     * @param \Cms\CollectionBundle\Entity\Item $item
     *
     * @return Link
     */
    public function setItem(\Cms\CollectionBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Cms\CollectionBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
