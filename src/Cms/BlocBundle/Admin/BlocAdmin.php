<?php

namespace Cms\BlocBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class BlocAdmin extends Admin
{

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('blocCategory')
            ->add('published');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('blocCategory', null, array('label' => 'Catégorie'))
            ->add('name', null, array('editable' => true, 'label' => 'Nom'))
            ->add('slug', null, array('label' => 'Slug'))
            ->add('blocType')
            ->add('published', null, array('editable' => true))
            ->add('language', 'string', array('template' => 'CmsCoreBundle:Admin:selected_lang.html.twig', 'label' => 'Langue'))
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                        'clone' => array('template' => 'CmsCoreBundle:Admin:list__action_clone.html.twig')
                    )
                )
            );
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $authorisationChecker = $this->getConfigurationPool()->getContainer()->get('cms_core.allow_access')->check('ROLE_CMS_BLOC_ADMIN_BLOC_MASTER');

        $formMapper
            ->with('Identification', array('class' => 'col-md-6'))
            ->add('name', 'text', array('label' => 'Nom', 'attr' => array('autofocus' => 'autofocus')))
            ->add('slug', 'text', array('label' => 'Slug', 'required' => false, 'disabled' => true))
            ->end()
            ->with('Propriété', array('class' => 'col-md-6'))
            ->add('published', 'checkbox', array('label' => 'Publication', 'required' => false, 'label_attr' => array('class' => 'help-trans')));
        if ($authorisationChecker) {

            $formMapper->add('allowEditFront', 'checkbox', array('label' => 'Activer l\'édition rapide', 'required' => false));
        }
        $formMapper->end()
            ->with('Contenu', array('class' => 'col-md-12'));
        if ($authorisationChecker) {
            $formMapper->add('blocCategory', 'entity', array('class' => 'CmsBlocBundle:BlocCategory', 'label' => 'Type de bloc', 'required' => true,));
        }
        $formMapper->add('contentText', 'textarea', array('label' => 'Texte', 'required' => false, 'help' => 'trans'))
            ->add('contentHtml', 'ckeditor', array('label' => 'WYSIWYG', 'required' => false, 'help' => 'trans'))
            ->add('group', 'entity', array('class' => 'CmsGroupBundle:Group', 'label' => 'Groupe d\'éléments', 'required' => false,))
            ->add('controller', 'entity', array('class' => 'CmsControllerBundle:Controller', 'label' => 'Chemin vers le contrôleur', 'required' => false,))
            ->add('query', 'entity', array('class' => 'CmsControllerBundle:Query', 'label' => 'Argument du contrôleur', 'required' => false,))
            ->add('menu', 'entity', array('class' => 'CmsNavBundle:Menu', 'label' => 'Menu', 'required' => false,))
            ->add('collection', 'entity', array('class' => 'CmsCollectionBundle:Collection', 'label' => 'Collection', 'required' => false,))
            ->end()
            ->with('Blog', array('class' => 'col-md-12 blog'))
            ->add('blogCategorys', 'sonata_type_model', array('multiple' => true, 'expanded' => false, 'label' => 'Catégories de blogs', 'required' => false, 'btn_add' => false))
            ->add('blogType', 'entity', array('class' => 'CmsBlogBundle:BlogType', 'label' => 'Type de blog', 'required' => true,))
            ->end();
        if ($authorisationChecker) {
            $formMapper
                ->with('Identifiant', array('class' => 'col-md-6 col-xs-12'))
                ->add('identifier', 'text', array('label' => 'Identifiant', 'required' => false))
                ->end()
                ->with('Class', array('class' => 'col-md-6 col-xs-12'))
                ->add('class', 'text', array('label' => 'Class', 'required' => false))->end()
                ->end()
                ->with('Style', array('class' => 'col-md-6 col-xs-12'))
                ->add('css', 'textarea', array('label' => 'Feuille de style aditionnel', 'required' => false))
                ->end()
                ->with('JavaScript', array('class' => 'col-md-6 col-xs-12'))
                ->add('script', 'textarea', array('label' => 'Javascript aditionnel', 'required' => false))->end()
                ->end()
                ->with('Col', array('class' => 'col-md-6 col-xs-12'))
                ->add(
                    'colXs',
                    'choice',
                    array(
                        'choices' => array(
                            'col-xs-1' => '1',
                            'col-xs-2' => '2',
                            'col-xs-3' => '3',
                            'col-xs-4' => '4',
                            'col-xs-5' => '5',
                            'col-xs-6' => '6',
                            'col-xs-7' => '7',
                            'col-xs-8' => '8',
                            'col-xs-9' => '9',
                            'col-xs-10' => '10',
                            'col-xs-11' => '11',
                            'col-xs-12' => '12',
                        ),
                        'label' => 'col-xs-',
                        'required' => false
                    )
                )
                ->add(
                    'colSm',
                    'choice',
                    array(
                        'choices' => array(
                            'col-sm-1' => '1',
                            'col-sm-2' => '2',
                            'col-sm-3' => '3',
                            'col-sm-4' => '4',
                            'col-sm-5' => '5',
                            'col-sm-6' => '6',
                            'col-sm-7' => '7',
                            'col-sm-8' => '8',
                            'col-sm-9' => '9',
                            'col-sm-10' => '10',
                            'col-sm-11' => '11',
                            'col-sm-12' => '12',
                        ),
                        'label' => 'col-sm-',
                        'required' => false
                    )
                )
                ->add(
                    'colMd',
                    'choice',
                    array(
                        'choices' => array(
                            'col-md-1' => '1',
                            'col-md-2' => '2',
                            'col-md-3' => '3',
                            'col-md-4' => '4',
                            'col-md-5' => '5',
                            'col-md-6' => '6',
                            'col-md-7' => '7',
                            'col-md-8' => '8',
                            'col-md-9' => '9',
                            'col-md-10' => '10',
                            'col-md-11' => '11',
                            'col-md-12' => '12',
                        ),
                        'label' => 'col-md-',
                        'required' => false
                    )
                )
                ->add(
                    'colLg',
                    'choice',
                    array(
                        'choices' => array(
                            'col-lg-1' => '1',
                            'col-lg-2' => '2',
                            'col-lg-3' => '3',
                            'col-lg-4' => '4',
                            'col-lg-5' => '5',
                            'col-lg-6' => '6',
                            'col-lg-7' => '7',
                            'col-lg-8' => '8',
                            'col-lg-9' => '9',
                            'col-lg-10' => '10',
                            'col-lg-11' => '11',
                            'col-lg-12' => '12',
                        ),
                        'label' => 'col-lg-',
                        'required' => false
                    )
                )
                ->end()->end()
                ->with('Offset', array('class' => 'col-md-6 col-xs-12'))
                ->add(
                    'offsetXs',
                    'choice',
                    array(
                        'choices' => array(
                            'col-xs-offset-0' => '0',
                            'col-xs-offset-1' => '1',
                            'col-xs-offset-2' => '2',
                            'col-xs-offset-3' => '3',
                            'col-xs-offset-4' => '4',
                            'col-xs-offset-5' => '5',
                            'col-xs-offset-6' => '6',
                            'col-xs-offset-7' => '7',
                            'col-xs-offset-8' => '8',
                            'col-xs-offset-9' => '9',
                            'col-xs-offset-10' => '10',
                            'col-xs-offset-11' => '11',
                            'col-xs-offset-12' => '12',
                        ),
                        'label' => 'col-xs-offset-',
                        'required' => false
                    )
                )
                ->add(
                    'offsetSm',
                    'choice',
                    array(
                        'choices' => array(
                            'col-sm-offset-0' => '0',
                            'col-sm-offset-1' => '1',
                            'col-sm-offset-2' => '2',
                            'col-sm-offset-3' => '3',
                            'col-sm-offset-4' => '4',
                            'col-sm-offset-5' => '5',
                            'col-sm-offset-6' => '6',
                            'col-sm-offset-7' => '7',
                            'col-sm-offset-8' => '8',
                            'col-sm-offset-9' => '9',
                            'col-sm-offset-10' => '10',
                            'col-sm-offset-11' => '11',
                            'col-sm-offset-12' => '12',
                        ),
                        'label' => 'col-sm-offset-',
                        'required' => false
                    )
                )
                ->add(
                    'offsetMd',
                    'choice',
                    array(
                        'choices' => array(
                            'col-md-offset-0' => '0',
                            'col-md-offset-1' => '1',
                            'col-md-offset-2' => '2',
                            'col-md-offset-3' => '3',
                            'col-md-offset-4' => '4',
                            'col-md-offset-5' => '5',
                            'col-md-offset-6' => '6',
                            'col-md-offset-7' => '7',
                            'col-md-offset-8' => '8',
                            'col-md-offset-9' => '9',
                            'col-md-offset-10' => '10',
                            'col-md-offset-11' => '11',
                            'col-md-offset-12' => '12',
                        ),
                        'label' => 'col-md-offset-',
                        'required' => false
                    )
                )
                ->add(
                    'offsetLg',
                    'choice',
                    array(
                        'choices' => array(
                            'col-lg-offset-0' => '0',
                            'col-lg-offset-1' => '1',
                            'col-lg-offset-2' => '2',
                            'col-lg-offset-3' => '3',
                            'col-lg-offset-4' => '4',
                            'col-lg-offset-5' => '5',
                            'col-lg-offset-6' => '6',
                            'col-lg-offset-7' => '7',
                            'col-lg-offset-8' => '8',
                            'col-lg-offset-9' => '9',
                            'col-lg-offset-10' => '10',
                            'col-lg-offset-11' => '11',
                            'col-lg-offset-12' => '12',
                        ),
                        'label' => 'col-lg-offset-',
                        'required' => false
                    )
                )
                ->end()
                ->end();
        };
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('contentText')
            ->add('contentHtml')
            ->add('created')
            ->add('updated')
            ->add('position')
            ->add('published')
            ->add('name')
            ->add('slug');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('clone', $this->getRouterIdParameter().'/clone');
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CmsBlocBundle:Admin:bloc_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
