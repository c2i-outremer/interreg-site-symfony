<?php

namespace Cms\ContactBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\Publishable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Newsletter
 *
 * @ORM\Table("cms_contact_newsletter")
 * @ORM\Entity(repositoryClass="Cms\ContactBundle\Entity\NewsletterRepository")
 */
class Newsletter
{

    use Timestampable;
    use Nameable;
    use Publishable;
    use Positionable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="firstname", type="string", length=255)
	 */
	private $firstname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastname", type="string", length=255)
	 */
	private $lastname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="phone", type="string", length=255, nullable=true)
	 */
	private $phone;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="adress", type="text", nullable=true)
	 */
	private $adress;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cp", type="string", length=255, nullable=true)
	 */
	private $cp;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="city", type="string", length=255, nullable=true)
	 */
	private $city;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="country", type="string", length=255, nullable=true)
	 */
	private $country;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="birthdate", type="datetime", nullable=true)
	 */
	private $birthdate;
	
	public function __construct()
	{
		$this->name = $this->getFirstname().' '.$this->getLastname();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set firstname
	 *
	 * @param string $firstname
	 * @return Newsletter
	 */
	public function setFirstname($firstname)
	{
		$this->firstname = $firstname;

		return $this;
	}

	/**
	 * Get firstname
	 *
	 * @return string 
	 */
	public function getFirstname()
	{
		return $this->firstname;
	}

	/**
	 * Set lastname
	 *
	 * @param string $lastname
	 * @return Newsletter
	 */
	public function setLastname($lastname)
	{
		$this->lastname = $lastname;

		return $this;
	}

	/**
	 * Get lastname
	 *
	 * @return string 
	 */
	public function getLastname()
	{
		return $this->lastname;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 * @return Newsletter
	 */
	public function setEmail($email)
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string 
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set phone
	 *
	 * @param string $phone
	 * @return Newsletter
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;

		return $this;
	}

	/**
	 * Get phone
	 *
	 * @return string 
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * Set adress
	 *
	 * @param string $adress
	 * @return Newsletter
	 */
	public function setAdress($adress)
	{
		$this->adress = $adress;

		return $this;
	}

	/**
	 * Get adress
	 *
	 * @return string 
	 */
	public function getAdress()
	{
		return $this->adress;
	}

	/**
	 * Set cp
	 *
	 * @param string $cp
	 * @return Newsletter
	 */
	public function setCp($cp)
	{
		$this->cp = $cp;

		return $this;
	}

	/**
	 * Get cp
	 *
	 * @return string 
	 */
	public function getCp()
	{
		return $this->cp;
	}

	/**
	 * Set city
	 *
	 * @param string $city
	 * @return Newsletter
	 */
	public function setCity($city)
	{
		$this->city = $city;

		return $this;
	}

	/**
	 * Get city
	 *
	 * @return string 
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * Set country
	 *
	 * @param string $country
	 * @return Newsletter
	 */
	public function setCountry($country)
	{
		$this->country = $country;

		return $this;
	}

	/**
	 * Get country
	 *
	 * @return string 
	 */
	public function getCountry()
	{
		return $this->country;
	}

	/**
	 * Set birthdate
	 *
	 * @param string $birthdate
	 * @return Newsletter
	 */
	public function setBirthdate($birthdate)
	{
		$this->birthdate = $birthdate;

		return $this;
	}

	/**
	 * Get birthdate
	 *
	 * @return string 
	 */
	public function getBirthdate()
	{
		return $this->birthdate;
	}

}
