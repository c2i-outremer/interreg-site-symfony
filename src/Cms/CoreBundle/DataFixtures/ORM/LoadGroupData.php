<?php

namespace Cms\CoreBundle\DataFixtures\ORM;

use Application\Sonata\UserBundle\Entity\Group;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadGroupData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $types = array('EDIT', 'LIST', 'CREATE', 'VIEW', 'DELETE', 'EXPORT', 'OPERATOR');

        $datas = array(
            array(
                'name' => 'Média',
                'roles' => array(
                    'ROLE_SONATA_MEDIA_ADMIN_MEDIA',
                    'ROLE_CMS_CORE_ADMIN_TAG',
                )
            ),
            array(
                'name' => 'Utilisateur',
                'roles' => array(
                    'ROLE_SONATA_USER_ADMIN_USER',
                    'ROLE_SONATA_USER_ADMIN_GROUP',
                )
            ),
            array(
                'name' => 'Préhome',
                'roles' => array(
                    'ROLE_SONATA_MEDIA_ADMIN_MEDIA',
                    'ROLE_CMS_PREHOME_ADMIN_PREHOME',
                )
            ),
            array(
                'name' => 'Configuration',
                'roles' => array(
                    'ROLE_CMS_CONFIG_ADMIN_CONFIG',
                    'ROLE_CMS_CONFIG_ADMIN_VARIABLE',
                    'ROLE_CMS_CONFIG_ADMIN_HTML',
                )
            ),
            array(
                'name' => 'Commentaire',
                'roles' => array(
                    'ROLE_CMS_COMMENT_ADMIN_COMMENT',
                )
            ),
            array(
                'name' => 'Navigation',
                'roles' => array(
                    'ROLE_CMS_NAV_ADMIN_MENU',
                    'ROLE_CMS_NAV_ADMIN_LINK',
                )
            ),
            array(
                'name' => 'Page et bloc',
                'roles' => array(
                    'ROLE_CMS_PAGE_ADMIN_PAGE',
                    'ROLE_CMS_PAGE_ADMIN_PAGE_BLOC',
                    'ROLE_CMS_PAGE_ADMIN_FOLDER',
                    'ROLE_CMS_BLOC_ADMIN_BLOC_CATEGORY',
                    'ROLE_CMS_GROUP_ADMIN_GROUP',
                    'ROLE_CMS_GROUP_ADMIN_ITEM',
                ),
                'extraRoles' => array(
                    'ROLE_CMS_BLOC_ADMIN_BLOC_LIST',
                    'ROLE_CMS_BLOC_ADMIN_BLOC_EDIT',
                )
            ),
            array(
                'name' => 'Configuration avancée',
                'roles' => array(
                    'ROLE_CMS_CONTROLLER_ADMIN_CONTROLLER',
                    'ROLE_CMS_CONTROLLER_ADMIN_QUERY',
                    'ROLE_CMS_CORE_ADMIN_ATTRIBUTE'
                )
            ),
            array(
                'name' => 'Intégration',
                'roles' => array(
                    'ROLE_CMS_CORE_ADMIN_CK_TEMPLATE',
                    'ROLE_CMS_NAV_ADMIN_MENU_TYPE',
                    'ROLE_CMS_PAGE_ADMIN_PAGE_TYPE',
                    'ROLE_CMS_BLOC_ADMIN_BLOC_TYPE',
                    'ROLE_CMS_GROUP_ADMIN_GROUP_TYPE',
                    'ROLE_CMS_BLOG_ADMIN_ARTICLE_TYPE',
                    'ROLE_CMS_BLOG_ADMIN_BLOG_TYPE',
                    'ROLE_CMS_COLLECTION_ADMIN_COLLECTION_TYPE',
                )
            ),
            array(
                'name' => 'Contact',
                'roles' => array(
                    'ROLE_CMS_CONTACT_ADMIN_CONTACT',
                    'ROLE_CMS_CONTACT_ADMIN_NEWSLETTER',
                )
            ),
            array(
                'name' => 'Collection',
                'roles' => array(
                    'ROLE_CMS_COLLECTION_ADMIN_ITEM',
                    'ROLE_CMS_COLLECTION_ADMIN_COLLECTION',
                    'ROLE_CMS_COLLECTION_ADMIN_COLLECTION_ITEM',
                    'ROLE_CMS_COLLECTION_ADMIN_LINK',
                    'ROLE_CMS_COLLECTION_ADMIN_VIDEO',
                    'ROLE_CMS_COLLECTION_ADMIN_PICTURE',
                    'ROLE_CMS_COLLECTION_ADMIN_FILE',
                    'ROLE_CMS_COLLECTION_ADMIN_CONTENT',
                    'ROLE_CMS_COLLECTION_ADMIN_NAME',
                ),
                'extraRoles' => array(
                    'ROLE_CMS_BLOC_ADMIN_BLOC_LIST',
                )
            ),
            array(
                'name' => 'Journaliste',
                'roles' => array(
                    'ROLE_SONATA_MEDIA_ADMIN_MEDIA',
                    'ROLE_CMS_BLOG_ADMIN_PICTURE',
                    'ROLE_CMS_BLOG_ADMIN_VIDEO',
                    'ROLE_CMS_BLOG_ADMIN_FILE',
                    'ROLE_CMS_BLOG_ADMIN_FIELD',
                ),
                'extraRoles' => array(
                    'ROLE_CMS_BLOG_ADMIN_ARTICLE_EDIT',
                    'ROLE_CMS_BLOG_ADMIN_ARTICLE_LIST',
                    'ROLE_CMS_BLOG_ADMIN_ARTICLE_CREATE',
                )
            ),
            array(
                'name' => 'Rédacteur en chef',
                'roles' => array(
                    'ROLE_SONATA_MEDIA_ADMIN_MEDIA',
                    'ROLE_CMS_CORE_ADMIN_TAG',
                    'ROLE_CMS_BLOG_ADMIN_PICTURE',
                    'ROLE_CMS_BLOG_ADMIN_VIDEO',
                    'ROLE_CMS_BLOG_ADMIN_FILE',
                    'ROLE_CMS_BLOG_ADMIN_FIELD',
                    'ROLE_CMS_BLOG_ADMIN_ARTICLE',
                    'ROLE_CMS_BLOG_ADMIN_CATEGORY',
                    'ROLE_CMS_BLOG_ADMIN_TAG',
                ),
                'extraRoles' => array(
                    'ROLE_CMS_BLOG_ADMIN_ARTICLE_MASTER',
                )
            ),
        );

        foreach ($datas as $data) {
            $roles = array();
            $roles[] = 'ROLE_ADMIN';

            foreach ($data['roles'] as $role) {
                foreach ($types as $type) {
                    $roles[] = $role.'_'.$type;
                }
            }

            if (isset($data['extraRoles'])) {
                foreach ($data['extraRoles'] as $role) {
                    $roles[] = $role;
                }
            }

            $group = new Group($data['name'], $roles);
            $manager->persist($group);
            $manager->flush();
        }
    }

}
