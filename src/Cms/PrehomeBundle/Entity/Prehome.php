<?php

namespace Cms\PrehomeBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\PublishTranslatable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;


use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Prehome
 *
 * @ORM\Table("cms_prehome_prehome")
 * @ORM\Entity(repositoryClass="Cms\PrehomeBundle\Entity\PrehomeRepository")
 * @Gedmo\TranslationEntity(class="Cms\PrehomeBundle\Entity\PrehomeTranslation")
 */
class Prehome extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use Timestampable;
    use NameTranslatable;
    use PublishTranslatable;
    use Positionable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\PrehomeBundle\Entity\PrehomeTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="start", type="datetime", nullable=true)
	 */
	private $start;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="end", type="datetime", nullable=true)
	 */
	private $end;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="short_content", type="text", nullable=true)
	 */
	private $shortContent;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="link", type="text", nullable=true)
	 */
	private $link;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="link_anchor", type="text", nullable=true)
	 */
	private $linkAnchor;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
	 * @ORM\JoinColumn(name="cover", referencedColumnName="id", nullable=true)
	 */
	protected $cover;
	
	
	public function __construct()
	{
        parent::__construct();
		$this->setStart(new \DateTime);
		$this->setEnd(new \DateTime);
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set start
	 *
	 * @param \DateTime $start
	 * @return Prehome
	 */
	public function setStart($start)
	{
		$this->start = $start;

		return $this;
	}

	/**
	 * Get start
	 *
	 * @return \DateTime 
	 */
	public function getStart()
	{
		return $this->start;
	}

	/**
	 * Set end
	 *
	 * @param \DateTime $end
	 * @return Prehome
	 */
	public function setEnd($end)
	{
		$this->end = $end;

		return $this;
	}

	/**
	 * Get end
	 *
	 * @return \DateTime 
	 */
	public function getEnd()
	{
		return $this->end;
	}

	/**
	 * Set shortContent
	 *
	 * @param string $shortContent
	 * @return Prehome
	 */
	public function setShortContent($shortContent)
	{
		$this->shortContent = $shortContent;

		return $this;
	}

	/**
	 * Get shortContent
	 *
	 * @return string 
	 */
	public function getShortContent()
	{
		return $this->shortContent;
	}

	/**
	 * Set content
	 *
	 * @param string $content
	 * @return Prehome
	 */
	public function setContent($content)
	{
		$this->content = $content;

		return $this;
	}

	/**
	 * Get content
	 *
	 * @return string 
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * Set link
	 *
	 * @param string $link
	 * @return Prehome
	 */
	public function setLink($link)
	{
		$this->link = $link;

		return $this;
	}

	/**
	 * Get link
	 *
	 * @return string 
	 */
	public function getLink()
	{
		return $this->link;
	}

	/**
	 * Set cover
	 *
	 * @param \Application\Sonata\MediaBundle\Entity\Media $cover
	 * @return Prehome
	 */
	public function setCover(\Application\Sonata\MediaBundle\Entity\Media $cover = null)
	{
		$this->cover = $cover;

		return $this;
	}

	/**
	 * Get cover
	 *
	 * @return \Application\Sonata\MediaBundle\Entity\Media 
	 */
	public function getCover()
	{
		return $this->cover;
	}


    /**
     * Set linkAnchor
     *
     * @param string $linkAnchor
     *
     * @return Prehome
     */
    public function setLinkAnchor($linkAnchor)
    {
        $this->linkAnchor = $linkAnchor;

        return $this;
    }

    /**
     * Get linkAnchor
     *
     * @return string
     */
    public function getLinkAnchor()
    {
        return $this->linkAnchor;
    }

}
