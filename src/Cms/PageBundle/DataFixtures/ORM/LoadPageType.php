<?php

namespace Cms\PageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Cms\PageBundle\Entity\PageType;

class LoadPageType implements FixtureInterface
{

	public function load(ObjectManager $manager)
	{
		$type = array(
			'name' => 'Page par defaut',
			'templatePath' => ':Layouts:default.html.twig',
		);

		$PageType = new PageType;
		$PageType->setName($type['name']);
		$PageType->setTemplatePath($type['templatePath']);
		
		$manager->persist($PageType);

		$manager->flush();
	}

}
