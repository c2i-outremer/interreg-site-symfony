<?php

namespace Cms\ConfigBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class HtmlAdmin extends Admin
{

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array('label' => 'Nom'))
            ->add('value', null, array('label' => 'Valeur'))
            ->add('description')
            ->add('published', null, array('label' => 'Publication'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array('label' => 'Nom', 'editable' => true))
            ->add('value', 'textarea', array('label' => 'Valeur', 'editable' => true))
            ->add('description', 'textarea', array('editable' => true))
            ->add('published', null, array('label' => 'Publication', 'editable' => true))
            ->add('language', 'string', array( 'label' => 'Langue', 'template' => 'CmsCoreBundle:Admin:selected_lang.html.twig'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('label' => 'Nom', 'help' => 'trans', 'attr' => array('autofocus' => 'autofocus')))
            ->add('value', null, array('label' => 'Valeur', 'help' => 'trans'))
            ->add('description')
            ->add('published', null, array('label' => 'Publication', 'label_attr' => array('class' => 'help-trans')))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name', null, array('label' => 'Nom'))
            ->add('value', null, array('label' => 'Valeur'))
            ->add('description')
            ->add('published', null, array('label' => 'Publication'))
            ->add('language', 'string', array('template' => 'CmsCoreBundle:Admin:selected_lang.html.twig', 'label' => 'Langue'))
        ;
    }

    public function getTemplate($name)
    {
        switch ($name)
        {
            case 'edit':
                return 'CmsConfigBundle:Admin:html_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }


}
