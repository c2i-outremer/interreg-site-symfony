<?php

namespace Cms\GroupBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * item
 *
 * @ORM\Table("cms_group_item")
 * @ORM\Entity(repositoryClass="Cms\GroupBundle\Entity\ItemRepository")
 */
class Item
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="position", type="integer")
	 */
	private $position;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\GroupBundle\Entity\Group", inversedBy="items",cascade={"persist"})
	 * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=false)
	 * */
	private $group;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\BlocBundle\Entity\Bloc", inversedBy="items",cascade={"persist"})
	 * @ORM\JoinColumn(name="bloc", referencedColumnName="id", nullable=false)
	 * */
	private $bloc;

	public function __toString()
	{
		return \md5($this->id);
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set position
	 *
	 * @param integer $position
	 * @return Item
	 */
	public function setPosition($position)
	{
		$this->position = $position;

		return $this;
	}

	/**
	 * Get position
	 *
	 * @return integer 
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * Set group
	 *
	 * @param \Cms\GroupBundle\Entity\Group $group
	 * @return Item
	 */
	public function setGroup(\Cms\GroupBundle\Entity\Group $group)
	{
		$this->group = $group;

		return $this;
	}

	/**
	 * Get group
	 *
	 * @return \Cms\GroupBundle\Entity\Group 
	 */
	public function getGroup()
	{
		return $this->group;
	}

	/**
	 * Set bloc
	 *
	 * @param \Cms\BlocBundle\Entity\Bloc $bloc
	 * @return Item
	 */
	public function setBloc(\Cms\BlocBundle\Entity\Bloc $bloc)
	{
		$this->bloc = $bloc;

		return $this;
	}

	/**
	 * Get bloc
	 *
	 * @return \Cms\BlocBundle\Entity\Bloc 
	 */
	public function getBloc()
	{
		return $this->bloc;
	}

}
