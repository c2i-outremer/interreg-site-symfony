<?php
/**
 * User: ed
 * Date: 30/07/15
 * Time: 15:47
 */

namespace Cms\CoreBundle\Services;


use Doctrine\ORM\EntityManager;

class CountUnpublished extends \Twig_Extension
{

    public $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function getName()
    {
        return 'CountUnpublished';
    }

    public function getFunctions()
    {
        return array(
            'CountUnpublished' => new \Twig_Function_Method($this, 'CountUnpublished'),
        );
    }

    public function CountUnpublished($entity)
    {
        return count($this->manager->getRepository($entity)->findBy(array('published' => false)));
    }
}