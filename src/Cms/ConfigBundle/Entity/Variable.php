<?php

namespace Cms\ConfigBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * variable
 *
 * @ORM\Table("cms_config_variable")
 * @ORM\Entity(repositoryClass="Cms\ConfigBundle\Entity\VariableRepository")
 */
class Variable
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="text")
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="value", type="text")
	 */
	private $value;

	/**
	 *
	 * @var boolean
	 * @ORM\Column(name="system", type="boolean", nullable=true) 
	 */
	private $system;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	function getName()
	{
		return $this->name;
	}

	function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * Set value
	 *
	 * @param string $value
	 * @return Variable
	 */
	public function setValue($value)
	{
		$this->value = $value;

		return $this;
	}

	/**
	 * Get value
	 *
	 * @return string 
	 */
	public function getValue()
	{
		return $this->value;
	}

	public function getSystem()
	{
		return $this->system;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setSystem($system)
	{
		$this->system = $system;
		return $this;
	}

	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

}
