<?php

namespace Extra\HouseBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Publishable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reservation
 *
 * @ORM\Table("extra_house_reservation")
 * @ORM\Entity(repositoryClass="Extra\HouseBundle\Repository\ReservationRepository")
 */
class Reservation
{

    use Publishable;
    use Timestampable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @Assert\Date
     * @ORM\Column(name="start", type="datetime")
     */
    private $start;

    /**
     * @var \DateTime
     * @Assert\Date
     * @ORM\Column(name="end", type="datetime")
     */
    private $end;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrAdult", type="integer")
     */
    private $nbrAdult;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrChild", type="integer")
     */
    private $nbrChild;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var string
     * @Assert\Email
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="Extra\HouseBundle\Entity\House", inversedBy="reservations")
     * @ORM\JoinColumn(name="house", referencedColumnName="id", nullable=false)
     *
     */
    protected $house;

    public function __construct() {
        $this->setPublished(false);
        $this->nbrAdult = 0;
        $this->nbrChild = 0;
    }

    public function __toString() {
        return $this->getHouse()->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Reservation
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return Reservation
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set nbrAdult
     *
     * @param integer $nbrAdult
     *
     * @return Reservation
     */
    public function setNbrAdult($nbrAdult)
    {
        $this->nbrAdult = $nbrAdult;

        return $this;
    }

    /**
     * Get nbrAdult
     *
     * @return integer
     */
    public function getNbrAdult()
    {
        return $this->nbrAdult;
    }

    /**
     * Set nbrChild
     *
     * @param integer $nbrChild
     *
     * @return Reservation
     */
    public function setNbrChild($nbrChild)
    {
        $this->nbrChild = $nbrChild;

        return $this;
    }

    /**
     * Get nbrChild
     *
     * @return integer
     */
    public function getNbrChild()
    {
        return $this->nbrChild;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Reservation
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Reservation
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Reservation
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Reservation
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set house
     *
     * @param \Extra\HouseBundle\Entity\House $house
     *
     * @return Reservation
     */
    public function setHouse(\Extra\HouseBundle\Entity\House $house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return \Extra\HouseBundle\Entity\House
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Reservation
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}
