<?php

namespace Cms\BlogBundle\Controller;

use Pix\SortableBehaviorBundle\Controller\SortableAdminController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\Request;

class ArticleAdminController extends SortableAdminController
{

    public function dashboardAction() {
        $articles = $this->getDoctrine()->getManager()->getRepository('CmsBlogBundle:Article')->findBy(
            array(),
            array('created' => 'desc'),
            30,
            0
        );
        return $this->render('CmsBlogBundle:Admin:article_dashboard_bloc.html.twig', array(
            'articles' => $articles,
        ));
    }

    public function batchActionPublish(ProxyQueryInterface $query)
    {
        $em = $this->getDoctrine()->getEntityManager();

        foreach ($query->getQuery()->iterate() as $entity) {
            $entity[0]->setPublished(!$entity[0]->getPublished());
            $em->persist($entity[0]);
        }
        $em->flush();

        return $this->redirect($this->admin->generateUrl('list'));
    }

    public function preCreate(Request $request, $object)
    {
        $this->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);
        $securityContext = $this->get('security.context');
        $user = $securityContext->getToken()->getUser();
        $object->setAuthor($user->getFirstname().' '.$user->getLastname());
        $object->setUser($user);
    }

    public function preEdit(Request $request, $object)
    {
        $this->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);
        $id = $request->get($this->admin->getIdParameter());
        $securityContext = $this->get('security.context');
        if (!$securityContext->isGranted('ROLE_CMS_BLOG_ADMIN_ARTICLE_MASTER') and !$securityContext->isGranted('ROLE_SUPER_ADMIN')) {
            $adminId = $securityContext->getToken()->getUser()->getId();
            $post = $this->getDoctrine()->getManager()->getRepository('CmsBlogBundle:Article')->find($id);
            if ($post->getUser()->getId() != $adminId) {
                return $this->redirect($this->generateUrl('admin_cms_blog_article_list'));
            }
        }
    }


    //------------------------------------------------------------------------------------------------------------------
    //	statAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     *
     * @param type $id
     * @return type
     */
    public function statAction($id)
    {
        return $this->render('CmsCoreBundle:Admin:stat_date.html.twig', array(
                'id' => (int)$id,
                'path' => 'admin_cms_blog_article_result'
            ));
    }

    //------------------------------------------------------------------------------------------------------------------
    //	resultAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param type $id
     * @return type
     */
    public function resultAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $visiteRepo = $em->getRepository('CmsCoreBundle:Visite');

        $start = $this->convertDateToObject($request->request->get('start'));
        $end = $this->convertDateToObject($request->request->get('end'));
        $periodValue = $request->request->get('periodValue');

        $dateInterval = \DateInterval::createFromDateString($periodValue);
        $datePeriod = new \DatePeriod($start, $dateInterval, $end);

        $days = array();

        foreach ($datePeriod as $day) {
            $data = array();
            $data['start'] = $day;
            $start = clone $day;
            $end = $start->add(\DateInterval::createFromDateString($periodValue));
            $data['end'] = $end;
            $data['cpt'] = count($visiteRepo->getStatsOfDay(array('dataType' => 'article', 'typeId' => (int)$id, 'start' => $day, 'end' => $end)));
            $days[] = $data;
        }

        return $this->render(
            'CmsCoreBundle:Admin:stat_result.html.twig',
            array(
                'days' => $days,
            )
        );
    }

    //------------------------------------------------------------------------------------------------------------------
    //	convertDateToObject
    //------------------------------------------------------------------------------------------------------------------

    /**
     *
     * @param type $string
     * @return \DateTime
     */
    private function convertDateToObject($string)
    {
        $date = explode('-', $string);
        $object = new \DateTime();
        $object->setDate($date[0], $date[1], $date[2]);
        $object->setTime(0, 0, 0);

        return $object;
    }

}
