<?php

namespace Extra\HouseBundle\Services;

use Doctrine\ORM\EntityManager;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Symfony\Component\Routing\RouterInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;

class SitemapListener implements SitemapListenerInterface
{

    private $router;
    private $manager;
    private $container;

    public function __construct(RouterInterface $router, EntityManager $manager)
    {
        $this->router = $router;
        $this->manager = $manager;
    }


    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function populateSitemap(SitemapPopulateEvent $event)
    {

        $section = $event->getSection();
        if (is_null($section) || $section == 'default') {

            $locales = $this->container->getParameter('locales');
            $default_locale = $this->container->getParameter('locale');

            $houses = $this->manager->getRepository('ExtraHouseBundle:House')->findAll();

            foreach ($houses as $house) {
                foreach ($locales as $locale) {
                    if ($house->getTranslation('published', $locale) or ($locale == $default_locale and $house->getPublished())) {
                        $houseSlug = ($locale == $default_locale) ? $house->getSlug() : $house->getTranslation('slug', $locale);
                        $url = $this->router->generate('extra_house_single', array('slug' => $houseSlug, '_locale' => $locale), true);
                        $event->getGenerator()->addUrl(new UrlConcrete($url, new \DateTime(), UrlConcrete::CHANGEFREQ_HOURLY, 1), 'house');
                    }
                }
            }
        }
    }


}
