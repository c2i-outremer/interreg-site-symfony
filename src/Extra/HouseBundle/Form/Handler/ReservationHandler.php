<?php

namespace Extra\HouseBundle\Form\Handler;

use Extra\HouseBundle\Manager\ReservationManager;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class ReservationHandler
{

    protected $form;
    protected $request;
    protected $manager;
    protected $mailer;

    public function __construct(Form $form, Request $request, ReservationManager $manager, $mailer)
    {
        $this->form = $form;
        $this->request = $request;
        $this->manager = $manager;
        $this->mailer = $mailer;
    }

    public function process()
    {
        $this->form->handleRequest($this->request);
        if($this->request->isMethod('post') && $this->form->isValid()) {
            return true;
        }
        return false;
    }

    protected function onSuccess()
    {

    }

    public function getForm() {
        return $this->form;
    }

}