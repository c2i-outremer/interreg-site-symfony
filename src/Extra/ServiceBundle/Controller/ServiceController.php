<?php

namespace Extra\ServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ServiceController extends Controller
{
    public function listAction(Request $request, $_locale)
    {
        if (!in_array($_locale, $this->container->getParameter('locales'))) {
            throw new NotFoundHttpException($this->get('translator')->trans('core.not_found.langage'));
        }

        $request->setLocale($_locale);

        $services = $this->get('extra_service.manager.service_manager')->getList($_locale);

        return $this->render(
            ':Service:List/list.html.twig',
            array(
                'localSwitcherExtraService' => true,
                'services' => $services,
                '_locale' => $_locale
            )
        );
    }

    public function singleAction(Request $request, $slug, $_locale)
    {

        $serviceManager = $this->get('extra_service.manager.service_manager');
        $service = $serviceManager->find($slug, $_locale);

        if(!$service) {
            throw new NotFoundHttpException($this->get('translator')->trans('service.error.not_found'));
        }

        $serviceManager->updateViste($service);

        return $this->render(
            ':Service:Single/single.html.twig',
            array(
                'localSwitcherExtraServiceSingle' => true,
                'service' => $service,
                '_locale' => $_locale
            )
        );
    }

}
