<?php

namespace Cms\BlogBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Publishable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Inscription
 *
 * @ORM\Table("cms_blog_inscription")
 * @ORM\Entity(repositoryClass="Cms\BlogBundle\Entity\InscriptionRepository")
 * @UniqueEntity(fields={"email","article"})
 */
class Inscription
{

    use Publishable;
    use Timestampable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull()
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * @var string
     * @Assert\NotNull()
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var string
     * @Assert\NotNull()
     * @Assert\Email
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\BlogBundle\Entity\Article", inversedBy="inscriptions")
     * @ORM\JoinColumn(name="article", referencedColumnName="id", nullable=false)
     *
     */
    protected $article;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\BlogBundle\Entity\Lieu")
     * @ORM\JoinColumn(name="lieu", referencedColumnName="id", nullable=true)
     * */
    protected $lieu;

    public function __construct()
    {
        $this->setPublished(false);
    }

    public function __toString()
    {
        return $this->getArticle()->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Inscription
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Inscription
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Inscription
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Inscription
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Inscription
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set article
     *
     * @param \Cms\BlogBundle\Entity\Article $article
     *
     * @return Inscription
     */
    public function setArticle(\Cms\BlogBundle\Entity\Article $article)
    {
        $this->article = $article;
        $article->addInscription($this);

        return $this;
    }

    /**
     * Get article
     *
     * @return \Cms\BlogBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set lieu
     *
     * @param \Cms\BlogBundle\Entity\Lieu $lieu
     *
     * @return Inscription
     */
    public function setLieu(\Cms\BlogBundle\Entity\Lieu $lieu = null)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return \Cms\BlogBundle\Entity\Lieu
     */
    public function getLieu()
    {
        return $this->lieu;
    }
}
