<?php

namespace Cms\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Attribute
 *
 * @ORM\Table("cms_core_attribute_translatable")
 * @ORM\Entity(repositoryClass="Cms\CoreBundle\Entity\AttributeRepository")
 * @Gedmo\TranslationEntity(class="Cms\CoreBundle\Entity\AttributeTranslatableTranslation")
 */
class AttributeTranslatable extends AbstractPersonalTranslatable implements TranslatableInterface
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\CoreBundle\Entity\AttributeTranslatableTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="value", type="string", length=255)
	 */
	private $value;
	
	public function __toString()
	{
		return $this->getName().' => '.$this->getValue();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Attribute
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set value
	 *
	 * @param string $value
	 * @return Attribute
	 */
	public function setValue($value)
	{
		$this->value = $value;

		return $this;
	}

	/**
	 * Get value
	 *
	 * @return string 
	 */
	public function getValue()
	{
		return $this->value;
	}

}
