<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{

	public function __construct($environment, $debug)
	{
		parent::__construct($environment, $debug);
		date_default_timezone_set('America/Guadeloupe');
	}

	public function registerBundles()
	{
		$bundles = array(
			new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
			new Symfony\Bundle\SecurityBundle\SecurityBundle(),
			new Symfony\Bundle\TwigBundle\TwigBundle(),
			new Symfony\Bundle\MonologBundle\MonologBundle(),
			new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
			new Symfony\Bundle\AsseticBundle\AsseticBundle(),
			new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
			new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
			//------------------------------------------------------------------------------------------------------------------
			//	Extensions
			//------------------------------------------------------------------------------------------------------------------
			//	sonata
			new Sonata\CoreBundle\SonataCoreBundle(),
			new Sonata\NotificationBundle\SonataNotificationBundle(),
			new Application\Sonata\NotificationBundle\ApplicationSonataNotificationBundle(),
			new Sonata\BlockBundle\SonataBlockBundle(),
			new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
			new Sonata\AdminBundle\SonataAdminBundle(),
			new Sonata\MediaBundle\SonataMediaBundle(),
			new Application\Sonata\MediaBundle\ApplicationSonataMediaBundle(),
			new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
			new Sonata\IntlBundle\SonataIntlBundle(),
			new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),
			new Application\Sonata\UserBundle\ApplicationSonataUserBundle(),
			new Sonata\ClassificationBundle\SonataClassificationBundle(),
			new Application\Sonata\ClassificationBundle\ApplicationSonataClassificationBundle(),
			new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
			new Sonata\FormatterBundle\SonataFormatterBundle(),
            new Sonata\TranslationBundle\SonataTranslationBundle(),
			//	Menu
			new Knp\Bundle\MenuBundle\KnpMenuBundle(),
			//	Rest
			new FOS\RestBundle\FOSRestBundle(),
			//	Comment
			new FOS\CommentBundle\FOSCommentBundle(),
			//	serializer
			new JMS\SerializerBundle\JMSSerializerBundle(),
			//	User
			new FOS\UserBundle\FOSUserBundle(),
			//	Sortable
			new Pix\SortableBehaviorBundle\PixSortableBehaviorBundle(),
			//	doctrine extensions
			new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
			//	ckeditor
			new CoopTilleuls\Bundle\CKEditorSonataMediaBundle\CoopTilleulsCKEditorSonataMediaBundle(),
			new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
			//	debugger
			new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle(),
			//	braincrafted-bootstrap
			new Braincrafted\Bundle\BootstrapBundle\BraincraftedBootstrapBundle(),
			//	JsRouting
			new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
			//	fixtures
			new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
			//	sitemap
			new Presta\SitemapBundle\PrestaSitemapBundle(),
			//	Liip
			new Liip\ImagineBundle\LiipImagineBundle(),
			//	HWIOBundle
			new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
			//	DizdaCloud
			new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
			new Dizda\CloudBackupBundle\DizdaCloudBackupBundle(),
			//------------------------------------------------------------------------------------------------------------------
			//	CmsBundle
			//------------------------------------------------------------------------------------------------------------------
			new Cms\CoreBundle\CmsCoreBundle(),
			new Cms\ConfigBundle\CmsConfigBundle(),
			new Cms\NavBundle\CmsNavBundle(),
			new Cms\PageBundle\CmsPageBundle(),
			new Cms\BlocBundle\CmsBlocBundle(),
			new Cms\GroupBundle\CmsGroupBundle(),
			new Cms\BlogBundle\CmsBlogBundle(),
			new Cms\CommentBundle\CmsCommentBundle(),
			new Cms\ContactBundle\CmsContactBundle(),
			new Cms\PrehomeBundle\CmsPrehomeBundle(),
            new Cms\ControllerBundle\CmsControllerBundle(),
            new Cms\CollectionBundle\CmsCollectionBundle(),
			//------------------------------------------------------------------------------------------------------------------
			//	Extra
			//------------------------------------------------------------------------------------------------------------------
//            new Extra\HouseBundle\ExtraHouseBundle(),
//            new Extra\ServiceBundle\ExtraServiceBundle(),
            //------------------------------------------------------------------------------------------------------------------
            //	AppBundle
            //------------------------------------------------------------------------------------------------------------------
            new AppBundle\AppBundle(),
		);

		if (in_array($this->getEnvironment(), array('dev', 'test', 'integr')))
		{
			$bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
			$bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
			$bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
		}

		return $bundles;
	}

	public function registerContainerConfiguration(LoaderInterface $loader)
	{
		$loader->load(__DIR__ . '/config/config_' . $this->getEnvironment() . '.yml');
	}

}
