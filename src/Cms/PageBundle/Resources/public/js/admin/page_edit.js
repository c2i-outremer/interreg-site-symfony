function pageModel() {
    $('div[class|="page"]').hide();
    var selectpageModel = $('select[id$="pageModel"]').val();
    console.log(selectpageModel);
    if(selectpageModel == 'BUILDER') {
        $('.page-builder').show();
    }
    if(selectpageModel == 'CONTROLLER') {
        $('.page-controller').show();
    }
    if(selectpageModel == 'TEMPLATE') {
        $('.page-template').show();
    }
}

$(function () {
    pageModel();
    $("select[id$='pageModel']").on('change', function (event) {
        pageModel();
    });

    $( "input[id$='_name']").on('keyup', function() {
        $( "textarea[id$='_metaTitle']").val($(this).val());
        $( "textarea[id$='_metaH1']").val($(this).val());
    });

});
