<?php

namespace Cms\BlocBundle\Services;

use Cms\ControllerBundle\Services\GetDataQuery;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class GetDataBloc
{

    protected $em;
    protected $router;
    protected $dataQuery;

    public function __construct(EntityManager $em, Router $router, GetDataQuery $dataQuery)
    {
        $this->em = $em;
        $this->router = $router;
        $this->dataQuery = $dataQuery;
    }


    //------------------------------------------------------------------------------------------------------------------
    //	getContentByCategory
    //------------------------------------------------------------------------------------------------------------------

    /**
     *
     * @param type $bloc
     * @param type $category
     * @return string
     */
    public function getContentByCategory($bloc)
    {
        switch ($bloc->getBlocCategory()->getId()) {
            case 1 :
                return $this->getContentText($bloc);
            case 2 :
                return $this->getContentWysiwyg($bloc);
            case 3 :
                return $this->getContentController($bloc);
            case 4 :
                return $this->getContentGroup($bloc);
            case 5 :
                return $this->getContentMenu($bloc);
            case 6 :
                return $this->getContentCollection($bloc);
            case 7 :
                return $this->getContentBlog($bloc);
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    //	getContentText
    //------------------------------------------------------------------------------------------------------------------

    /**
     *
     * @param type $bloc
     * @return type
     */
    private function getContentText($bloc)
    {
        return $bloc->getContentText();
    }

    //------------------------------------------------------------------------------------------------------------------
    //	getContentWysiwyg
    //------------------------------------------------------------------------------------------------------------------

    /**
     *
     * @param type $bloc
     * @return type
     */
    private function getContentWysiwyg($bloc)
    {
        return $bloc->getContentHtml();
    }

    //------------------------------------------------------------------------------------------------------------------
    //	getContentWysiwyg
    //------------------------------------------------------------------------------------------------------------------

    private function getContentController($bloc)
    {
        $arguments = (!is_object($bloc->getQuery())) ? null : $this->dataQuery->getDataQuery($bloc->getQuery()->getArguments()->toArray());
        if (!empty($arguments)) {
            return "{{ render(controller('" . $bloc->getController()->getPath() . "', " . json_encode($arguments) . ")) }}";
        } else {
            return "{{ render(controller('" . $bloc->getController()->getPath() . "')) }}";
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    //	getContentMenu
    //------------------------------------------------------------------------------------------------------------------

    private function getContentMenu($bloc)
    {
        return "{{ showMenu('" . $bloc->getMenu()->getSlug() ."')|raw }}";
    }

    //------------------------------------------------------------------------------------------------------------------
    //	getContentGroup
    //------------------------------------------------------------------------------------------------------------------

    public function getContentGroup($bloc)
    {
        return "{{ render(controller('CmsGroupBundle:Group:showInBloc', {'bloc':" . $bloc->getId() . "})) }}";
    }

    //------------------------------------------------------------------------------------------------------------------
    //	getContentSlider
    //------------------------------------------------------------------------------------------------------------------

    public function getContentCollection($bloc)
    {
        return "{{ showCollection('".$bloc->getCollection()->getSlug()."')|raw }}";
    }

    //------------------------------------------------------------------------------------------------------------------
    //	getContentMenu
    //------------------------------------------------------------------------------------------------------------------

    public function getContentBlog($bloc)
    {
        return "{{ include(template_from_string(showBlog('" . $bloc->getId() . "'))) }}";
    }
}
