<?php

namespace Cms\BlocBundle\Services\Twig;

use Cms\BlocBundle\Manager\BlocManager;
use Cms\BlocBundle\Services\GetDataBloc;
use Doctrine\ORM\EntityManager;

class showBloc extends \Twig_Extension
{
    protected $getDataBloc;
    protected $em;
    protected $manager;

    function __construct(GetDataBloc $getDataBloc, EntityManager $em, BlocManager $manager)
    {
        $this->getDataBloc = $getDataBloc;
        $this->em = $em;
        $this->manager = $manager;
    }

    public function getFunctions()
    {
        return array(
            'showBloc' => new \Twig_Function_Method($this, 'showBloc'),
            'getBlocs' => new \Twig_Function_Method($this, 'getBlocs'),
        );
    }


    public function getName()
    {
        return 'showBloc';
    }

    public function getBlocs(array $params, $findBySlug = true) {
        return $this->manager->getBlocs($params, $findBySlug);
    }


    public function showBloc($bloc)
    {
        $bloc = (is_object($bloc)) ? $bloc : $this->em->getRepository('CmsBlocBundle:Bloc')->getBloc($bloc);

        if ($bloc) {
            $html = '';
            if ($bloc->getPublished()) {
                $html .= '<div '.$this->getParams($bloc).'>';
                $html .= $this->getDataBloc->getContentByCategory($bloc);
                $html .= '</div>';
            }

            return $html;
        }
    }

    private function getParams($bloc)
    {
        $params = array();
        $params[] = 'data-block="'.$bloc->getId().'"';

        if (!is_null($bloc->getIdentifier())) {
            $params[] = 'id="'.$bloc->getIdentifier().'"';
        }


        $class = array();
        $class[] = 'block';

        if (!is_null($bloc->getClass())) {
            $class[] = $bloc->getClass();
        }

        $class[] = $this->getClassBootstrap($bloc);

        $params[] = 'class="'.trim(implode(' ', $class)).'"';

        return implode(' ', $params);
    }

    private function getClassBootstrap($bloc)
    {
        $class = [];
        $boostraps = array('ColXs', 'ColSm', 'ColMd', 'ColLg', 'OffsetXs', 'OffsetSm', 'OffsetMd', 'OffsetLg');
        foreach ($boostraps as $boostrap) {
            $method = 'get'.$boostrap;
            if (!is_null($bloc->$method())) {
                $class[] = $bloc->$method();
            }
        }

        return trim(implode(' ', $class));
    }
}