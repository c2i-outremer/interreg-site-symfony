<?php

namespace Cms\NavBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{

	//------------------------------------------------------------------------------------------------------------------
	//	showInBlocAction
	//------------------------------------------------------------------------------------------------------------------
	
	/**
	 * render a menu in a block
	 * @param array $bloc
	 * @return type
	 */

	public function showInBlocAction($bloc)
	{
		$em = $this->getDoctrine()->getManager();

		$menuRepo = $em->getRepository('CmsNavBundle:Menu');
		$menu = $menuRepo->getMenuAndType($bloc['bloc']->getMenu()->getSlug());

		$content = $this->render('CmsNavBundle:Type:knp.html.twig', array(
			'menu' => $menu,
		));

		$bloc['content'] = $content->getContent();

		$templatePath = ($bloc['templatePath']) ? $bloc['templatePath'] : 'CmsBlocBundle:Type:default.html.twig';

		return $this->render($templatePath, array(
					'bloc' => $bloc,
		));
	}

}
