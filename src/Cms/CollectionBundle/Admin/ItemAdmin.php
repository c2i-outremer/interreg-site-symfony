<?php

namespace Cms\CollectionBundle\Admin;

use Cms\CollectionBundle\Entity\ItemTranslation;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ItemAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('created')
            ->add('updated')
            ->add('name')
            ->add('slug')
            ->add('published')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $auth = $this->getConfigurationPool()->getContainer()->get('cms_core.allow_access')->check('ROLE_CMS_COLLECTION_ADMIN_ITEM_MASTER');

        $formMapper
            ->tab('Général')
                ->with('Nom', array('class' => 'col-sm-4'))
                    ->add('name', null, array('help' => 'trans','label' => ' '))
                    ->add('published', null, array('label' => ' ', 'label_attr' => array('class' => 'help-trans')))
                ->end()
                ->with('Noms', array('class' => 'col-sm-8'))
                    ->add('names', 'sonata_type_collection', array('help' => 'trans','required' => false, 'by_reference' => false, 'label' => ' ', 'type_options' => array('delete' => true)), array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
                ->end()
            ->end()
            ->tab('Contenu')
                ->with('Contenu', array('class' => 'col-sm-6'))
                    ->add('content', 'ckeditor', array('help' => 'trans','required' => false, 'label' => ' '))
                    ->add('bloc', 'sonata_type_model_list', array('required' => false, 'label' => 'Bloc'))
                ->end()
                ->with('Contenus', array('class' => 'col-sm-6'))
                    ->add('contents', 'sonata_type_collection', array('help' => 'trans', 'required' => false, 'by_reference' => false, 'label' => ' ', 'type_options' => array('delete' => true)), array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
                ->end()
            ->end()
            ->tab('Multimédia')
                ->with('Couverture', array('class' => 'col-sm-12'))
                    ->add('cover', 'sonata_type_model_list', array('required' => false, 'label' => false), array('link_parameters' => array('context' => 'picture', 'provider' => 'sonata.media.provider.image')))
                ->end()
                ->with('Autre images', array('class' => 'col-sm-6'))
                    ->add('pictures', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)), array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
                ->end()
                ->with('Vidéos', array('class' => 'col-sm-6'))
                    ->add('videos', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)), array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
                ->end()
                ->with('Fichiers', array('class' => 'col-sm-6'))
                    ->add('files', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)), array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
                ->end()
                ->with('Liens', array('class' => 'col-sm-6'))
                    ->add('links', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)), array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
                ->end()
            ->end();
        if($auth) {
            $formMapper
                ->tab('Programmation')
                    ->with('Contenu TWIG aditionnel', array('class' => 'col-sm-6'))
                        ->add('html', null, array('required' => false, 'label' => false))
                    ->end()
                    ->with('JavaScript', array('class' => 'col-sm-6'))
                        ->add('script', null, array('label' => false))
                    ->end()
                ->end()
            ;
        }

    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('created')
            ->add('updated')
            ->add('name')
            ->add('slug')
            ->add('published')
        ;
    }

    public function getTemplate($name)
    {
        switch ($name)
        {
            case 'edit':
                return 'CmsCollectionBundle:Admin:collection_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $locales = $container->getParameter('locales');
        $defaultLocale = $container->getParameter('locale');
        $em = $container->get('doctrine.orm.entity_manager');

        $fields = array(
            'published' => 0,
        );

        foreach ($locales as $locale) {
            foreach ($fields as $key => $value) {
                if ($locale != $defaultLocale) {
                    $translation = new ItemTranslation($locale, $key, $value);
                    $translation->setObject($object);
                    $em->persist($translation);
                }
            }
        }

        $em->flush();

    }
}
