<?php

namespace Cms\PageBundle\Controller;

use Cms\CoreBundle\Entity\Visite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends Controller
{

    //------------------------------------------------------------------------------------------------------------------
    //	homepageAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     * show homepage
     */

    public function homepageAction(Request $request, $_locale)
    {
        $em = $this->getDoctrine()->getManager();
        $homepage = $em->getRepository('CmsPageBundle:Page')->find(1);

        $request->setLocale($_locale);

        return $this->forward('CmsPageBundle:Page:show', array('_locale' => $_locale, 'pageLink' => $homepage->getSlug()));
    }

    //------------------------------------------------------------------------------------------------------------------
    //	showAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Show a page
     * @param type $pageLink
     * @return type
     */
    public function showAction(Request $request, $_locale, $pageLink, $otherParams = null)
    {
        if (!in_array($_locale, $this->container->getParameter('locales'))) {
            throw new NotFoundHttpException($this->get('translator')->trans('core.not_found.langage'));
        }

        $request->setLocale($_locale);

        $em = $this->getDoctrine()->getManager();

        $folderSlug = $this->get('cms_page.GetPageData')->getFolderSlug($pageLink);

        // role super_admin ?
        $isAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');

        // get data page
        $page = $em->getRepository('CmsPageBundle:Page')->getPage($folderSlug['folder'], $folderSlug['slug'], $isAdmin, $_locale);


        if (!$page) {
            throw new NotFoundHttpException($this->get('translator')->trans('page.error.not_found').' : '.$pageLink);
        }

        if (!$page->getPublished()) { // @todo => finish this
            throw new NotFoundHttpException($this->get('translator')->trans('page.error.unpublished').' : '.$pageLink);
        }

        // stats
        $page->setNbrViews($page->getNbrViews() + 1);
        $visite = new Visite('page', $request->getPathInfo(), $page->getId());
        $em->persist($visite);
        $em->flush();



        if ($page->getPageModel() == 'BUILDER') { // BUILDER
            $content = $this->render(':Page/Builder:builder.html.twig', array(
                'showMetaH1' => $page->getShowMetaH1(),
                'metaH1' => $page->getMetaH1(),
                'pageBlocs' => $em->getRepository('CmsPageBundle:PageBloc')->getPageBlocks($page),
            ))->getContent();
        }

        if ($page->getPageModel() == 'CONTROLLER') { // CONTROLLER
            $content = $this->render(':Page/Controller:controller.html.twig', array(
                'controller' => $page->getController(),
                'arguments' => (!is_object($page->getQuery())) ? null : $this->get('cms_controller.GetDataQuery')->getDataQuery($page->getQuery()->getArguments()->toArray()),
            ))->getContent();
        }

        if ($page->getPageModel() == 'TEMPLATE') { // TEMPLATE
            $content = $this->render(':Page/Template:template.html.twig', array(
                'showMetaH1' => $page->getShowMetaH1(),
                'metaH1' => $page->getMetaH1(),
                'metaDescription' => $page->getMetaDescription(),
                'template' => $page->getPageType(),
            ))->getContent();
        }

        return $this->render(
            ':Page:page.html.twig',
            array(
                'model' => $page->getPageModel(),
                'content' => $content,
                'pageBlocs' => $em->getRepository('CmsPageBundle:PageBloc')->getPageBlocks($page),
                'localSwitcherPage' => true,
                '_locale' => $_locale,
                'page' => $page,
                'isAdmin' => $isAdmin,
                'otherParams' => $otherParams,
            )
        );



    }

}
