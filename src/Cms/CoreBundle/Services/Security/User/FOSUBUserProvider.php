<?php

namespace Cms\CoreBundle\Services\Security\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Description of FOSUBUserProvider
 *
 * @author marc
 */
class FOSUBUserProvider extends BaseClass
{

	/**
	 * {@inheritDoc}
	 */
	public function connect(UserInterface $user, UserResponseInterface $response)
	{
		$r = $response->getResponse();

		//we "disconnect" previously connected users
		if (null !== $previousUser = $this->userManager->findUserBy(array("email" => $r["email"])))
		{
			$previousUser->setFacebookUId(null);
			$previousUser->setToken(null);
			$this->userManager->updateUser($previousUser);
		}

		//we connect current user
		$user->setFacebookUId($r["id"]);
		$user->setToken($response->getAccessToken());

		$this->userManager->updateUser($user);
	}

	/**
	 * {@inheritdoc}
	 */
	public function loadUserByOAuthUserResponse(UserResponseInterface $response)
	{

		$r = $response->getResponse();
		$user = $this->userManager->findUserBy(array("email" => $r["email"]));
		if (null === $user)
		{
			$user = $this->userManager->createUser();
			$user->setFacebookUId($r["id"]);
			$user->setToken($response->getAccessToken());
			$user->setUsername($r["name"]);
			$user->setEmail($r["email"]);
			$user->setPlainPassword($r["email"]);
			$user->setFirstName($r["first_name"]);
			$user->setLastName($r["last_name"]);
			$user->setEnabled(true);
			$this->userManager->updateUser($user);
			return $user;
		}

		$user->setFacebookUId($r["id"]);
		$user->setToken($response->getAccessToken());

		return $user;
	}

}
