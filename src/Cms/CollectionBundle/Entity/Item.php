<?php

namespace Cms\CollectionBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Cms\CoreBundle\Entity\Traits\PublishTranslatable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Item
 *
 * @ORM\Table("cms_collection_item")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="Cms\CollectionBundle\Entity\ItemTranslation")
 */
class Item extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use Timestampable;
    use NameTranslatable;
    use PublishTranslatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\ItemTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;


    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;

    /**
     * @var string
     *
     * @ORM\Column(name="html", type="text", nullable=true)
     */
    protected $html;

    /**
     * @var string
     *
     * @ORM\Column(name="script", type="text", nullable=true)
     */
    protected $script;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="cover", referencedColumnName="id", nullable=true)
     */
    protected $cover;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\BlocBundle\Entity\Bloc")
     * @ORM\JoinColumn(name="bloc", referencedColumnName="id", nullable=true)
     */
    protected $bloc;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\Picture", mappedBy="item", cascade={"persist"}, orphanRemoval=true)
     */
    protected $pictures;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\Video", mappedBy="item", cascade={"persist"}, orphanRemoval=true)
     */
    protected $videos;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\File", mappedBy="item", cascade={"persist"}, orphanRemoval=true)
     */
    protected $files;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\Link", mappedBy="item", cascade={"persist"}, orphanRemoval=true)
     */
    protected $links;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\Name", mappedBy="item", cascade={"persist"}, orphanRemoval=true)
     */
    protected $names;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\Content", mappedBy="item", cascade={"persist"}, orphanRemoval=true)
     */
    protected $contents;


    /**
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\CollectionItem", mappedBy="item")
     */
    private $collectionItems;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->collectionItems = new ArrayCollection();
        $this->pictures = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->links = new ArrayCollection();
        $this->fields = new ArrayCollection();
        $this->contents = new ArrayCollection();
        $this->names = new ArrayCollection();
    }

    /**
     * Add collectionItem
     *
     * @param \Cms\CollectionBundle\Entity\CollectionItem $collectionItem
     *
     * @return Item
     */
    public function addCollectionItem(\Cms\CollectionBundle\Entity\CollectionItem $collectionItem)
    {
        $this->collectionItems[] = $collectionItem;
        $collectionItem->setItem($this);
        return $this;
    }

    /**
     * Remove collectionItem
     *
     * @param \Cms\CollectionBundle\Entity\CollectionItem $collectionItem
     */
    public function removeCollectionItem(\Cms\CollectionBundle\Entity\CollectionItem $collectionItem)
    {
        $this->collectionItems->removeElement($collectionItem);
    }

    /**
     * Get collectionItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollectionItems()
    {
        return $this->collectionItems;
    }


    /**
     * Set content
     *
     * @param string $content
     *
     * @return Item
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set script
     *
     * @param string $script
     *
     * @return Item
     */
    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script
     *
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * Set cover
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $cover
     *
     * @return Item
     */
    public function setCover(\Application\Sonata\MediaBundle\Entity\Media $cover = null)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Add picture
     *
     * @param \Cms\CollectionBundle\Entity\Picture $picture
     *
     * @return Item
     */
    public function addPicture(\Cms\CollectionBundle\Entity\Picture $picture)
    {
        $this->pictures[] = $picture;
        $picture->setItem($this);
        return $this;
    }

    /**
     * Remove picture
     *
     * @param \Cms\CollectionBundle\Entity\Picture $picture
     */
    public function removePicture(\Cms\CollectionBundle\Entity\Picture $picture)
    {
        $this->pictures->removeElement($picture);
    }

    /**
     * Get pictures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * Add video
     *
     * @param \Cms\CollectionBundle\Entity\Video $video
     *
     * @return Item
     */
    public function addVideo(\Cms\CollectionBundle\Entity\Video $video)
    {
        $this->videos[] = $video;
        $video->setItem($this);
        return $this;
    }

    /**
     * Remove video
     *
     * @param \Cms\CollectionBundle\Entity\Video $video
     */
    public function removeVideo(\Cms\CollectionBundle\Entity\Video $video)
    {
        $this->videos->removeElement($video);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Add file
     *
     * @param \Cms\CollectionBundle\Entity\File $file
     *
     * @return Item
     */
    public function addFile(\Cms\CollectionBundle\Entity\File $file)
    {
        $this->files[] = $file;
        $file->setItem($this);
        return $this;
    }

    /**
     * Remove file
     *
     * @param \Cms\CollectionBundle\Entity\File $file
     */
    public function removeFile(\Cms\CollectionBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add link
     *
     * @param \Cms\CollectionBundle\Entity\Link $link
     *
     * @return Item
     */
    public function addLink(\Cms\CollectionBundle\Entity\Link $link)
    {
        $this->links[] = $link;
        $link->setItem($this);
        return $this;
    }

    /**
     * Remove link
     *
     * @param \Cms\CollectionBundle\Entity\Link $link
     */
    public function removeLink(\Cms\CollectionBundle\Entity\Link $link)
    {
        $this->links->removeElement($link);
    }

    /**
     * Get links
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinks()
    {
        return $this->links;
    }


    /**
     * Add content
     *
     * @param \Cms\CollectionBundle\Entity\Content $content
     *
     * @return Item
     */
    public function addContent(\Cms\CollectionBundle\Entity\Content $content)
    {
        $this->contents[] = $content;
        $content->setItem($this);
        return $this;
    }

    /**
     * Remove content
     *
     * @param \Cms\CollectionBundle\Entity\Content $content
     */
    public function removeContent(\Cms\CollectionBundle\Entity\Content $content)
    {
        $this->contents->removeElement($content);
    }

    /**
     * Get contents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * Set html
     *
     * @param string $html
     *
     * @return Item
     */
    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Get html
     *
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Add name
     *
     * @param \Cms\CollectionBundle\Entity\Name $name
     *
     * @return Item
     */
    public function addName(\Cms\CollectionBundle\Entity\Name $name)
    {
        $this->names[] = $name;
        $name->setItem($this);
        return $this;
    }

    /**
     * Remove name
     *
     * @param \Cms\CollectionBundle\Entity\Name $name
     */
    public function removeName(\Cms\CollectionBundle\Entity\Name $name)
    {
        $this->names->removeElement($name);
    }

    /**
     * Get names
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * Set bloc
     *
     * @param \Cms\BlocBundle\Entity\Bloc $bloc
     *
     * @return Item
     */
    public function setBloc(\Cms\BlocBundle\Entity\Bloc $bloc = null)
    {
        $this->bloc = $bloc;

        return $this;
    }

    /**
     * Get bloc
     *
     * @return \Cms\BlocBundle\Entity\Bloc
     */
    public function getBloc()
    {
        return $this->bloc;
    }

    /**
     * Remove translation
     *
     * @param \Cms\CollectionBundle\Entity\ItemTranslation $translation
     */
    public function removeTranslation(\Cms\CollectionBundle\Entity\ItemTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }
}
