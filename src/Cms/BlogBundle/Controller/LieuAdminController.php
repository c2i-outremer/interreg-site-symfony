<?php

namespace Cms\BlogBundle\Controller;

use Pix\SortableBehaviorBundle\Controller\SortableAdminController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\Request;

class LieuAdminController extends SortableAdminController
{

    public function preCreate(Request $request, $object)
    {
        $this->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);
    }

    public function preEdit(Request $request, $object)
    {
        $this->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);
    }
}
