<?php

namespace Cms\ConfigBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Cms\ConfigBundle\Entity\Config;

class LoadConfig implements FixtureInterface
{

	public function load(ObjectManager $manager)
	{
		$configs = array(
			'siteName' => 'Digitom CMS',
			'siteOnline'=> true,
			'editBloc' => false,
			'theme'=> 'base',
			'nbrArticles' => 12,
			'googleAnalytics' => '',
			'allowBlogComment' => true,
			'nbrComments' => 20,
			'mailStatus' => false,
			'mailSender' => 'contact@digitom.com',
			'mailName' => 'Contact',
		);

        $config = new Config();


        foreach($configs as $key => $value)
		{
            $method = 'set'.ucfirst($key);
            $config->$method($value);
		}

		$manager->persist($config);
		$manager->flush();
	}

}
