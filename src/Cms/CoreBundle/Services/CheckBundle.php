<?php


namespace Cms\CoreBundle\Services;


use Symfony\Component\DependencyInjection\ContainerInterface;

class CheckBundle extends \Twig_Extension
{

    protected $container;

    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function CheckBundle($bundleName) {
        return array_key_exists(
            $bundleName,
            $this->container->getParameter('kernel.bundles')
        );
    }


    public function getName()
    {
        return 'CheckBundle';
    }

    public function getFunctions()
    {
        return array(
            'CheckBundle' => new \Twig_Function_Method($this, 'CheckBundle'),
        );
    }
}