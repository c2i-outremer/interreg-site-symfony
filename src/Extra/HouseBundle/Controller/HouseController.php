<?php

namespace Extra\HouseBundle\Controller;

use Extra\HouseBundle\Entity\Reservation;
use Extra\HouseBundle\Form\ReservationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HouseController extends Controller
{
    public function listAction(Request $request, $_locale)
    {
        if (!in_array($_locale, $this->container->getParameter('locales'))) {
            throw new NotFoundHttpException($this->get('translator')->trans('core.not_found.langage'));
        }

        $request->setLocale($_locale);

        $houses = $this->get('extra_house.manager.house_manager')->getList($_locale);

        return $this->render(
            ':House:List/list.html.twig',
            array(
                'localSwitcherExtraHouse' => true,
                'houses' => $houses,
                '_locale' => $_locale
            )
        );
    }

    public function singleAction(Request $request, $slug, $_locale)
    {

        $house = $this->get('extra_house.manager.house_manager')->find($slug, $_locale);

        if (!$house) {
            throw new NotFoundHttpException($this->get('translator')->trans('house.error.not_found'));
        }

        $this->get('extra_house.manager.house_manager')->updateViste($house, $_locale);

        return $this->render(
            ':House:Single/single.html.twig',
            array(
                'threadComments' => $this->get('cms_comment.services.comment_tools')->getThreadComment('house-'.$house->getSlug()),
                'localSwitcherExtraHouseSingle' => true,
                'house' => $house,
                '_locale' => $_locale
            )
        );
    }

    public function reservationAction(Request $request, $_locale, $house)
    {

        $house = $this->get('extra_house.manager.house_manager')->find($house, $_locale);

        $reservation = new Reservation();
        $reservation->setHouse($house);

        $form = $this->get('form.factory')->create(new ReservationType(), $reservation);

        $form->handleRequest($request);

        if($form->isValid()) {

            $reservation->setStart(new \DateTime($reservation->getStart()));
            $reservation->setEnd(new \DateTime($reservation->getEnd()));
            $this->get('extra_house.manager.reservation_manager')->save($reservation);

            // email
            $config = $this->get('cms_config.GetConfig')->getConfig();
            $em = $this->getDoctrine()->getManager();
            if ($config->getMailStatus()) {

                $users = $em->getRepository('ApplicationSonataUserBundle:User')->getByGroup('Contact');

                $emails = array();
                foreach ($users as $user) {
                    $emails[] = $user->getEmail();
                }


                $message = \Swift_Message::newInstance()
                    ->setSubject('Demande de résérvation: ' . $reservation->getHouse()->getName())
                    ->setFrom(array($config->getMailSender() => $config->getMailName()))
                    ->setTo($emails)
                    ->setBody($this->renderView(':House/Reservation:send_email.html.twig', array('reservation' => $reservation)), 'text/html');

                $this->get('mailer')->send($message);
            }

            $this->get('session')->getFlashBag()->add('reservation', $this->get('translator')->trans('house.form.flash_message'));
            return $this->redirect($this->generateUrl('extra_house_single', array('slug' => $house->getSlug(), '_locale' => $_locale)));
        }

        return $this->render(':House/Single/Partials:reservation_form.html.twig', array(
            'house' => $house,
            'form' => $form->createView(),
        ));

    }

}
