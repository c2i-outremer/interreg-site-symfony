<?php

namespace Cms\ConfigBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Cms\CoreBundle\Entity\BaseEntity;

/**
 * lang
 *
 * @ORM\Table("cms_config_lang")
 * @ORM\Entity(repositoryClass="Cms\ConfigBundle\Entity\LangRepository")
 */
class Lang extends BaseEntity
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\PageBundle\Entity\Page", mappedBy="lang")
	 * */
	private $pages;

	function __construct()
	{
		parent::__construct();
		$this->pages = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Add pages
	 *
	 * @param \Cms\PageBundle\Entity\Page $pages
	 * @return Lang
	 */
	public function addPage(\Cms\PageBundle\Entity\Page $pages)
	{
		$this->pages[] = $pages;
		$pages->setLang($this);
		return $this;
	}

	/**
	 * Remove pages
	 *
	 * @param \Cms\PageBundle\Entity\Page $pages
	 */
	public function removePage(\Cms\PageBundle\Entity\Page $pages)
	{
		$this->pages->removeElement($pages);
	}

	/**
	 * Get pages
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getPages()
	{
		return $this->pages;
	}

}
