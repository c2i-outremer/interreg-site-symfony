<?php

namespace Cms\BlogBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tag
 *
 * @ORM\Table("cms_blog_tag")
 * @ORM\Entity(repositoryClass="Cms\BlogBundle\Entity\TagRepository")
 * @Gedmo\TranslationEntity(class="Cms\BlogBundle\Entity\TagTranslation")
 */
class Tag extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use NameTranslatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\TagTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

