<?php

namespace Cms\BlogBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Attribute
 *
 * @ORM\Table("cms_blog_field")
 * @ORM\Entity(repositoryClass="Cms\CoreBundle\Entity\AttributeRepository")
 * @Gedmo\TranslationEntity(class="Cms\BlogBundle\Entity\FieldTranslation")
 */
class Field  extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use Positionable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\BlogBundle\Entity\FieldTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Cms\BlogBundle\Entity\Article", inversedBy="fields")
     * @ORM\JoinColumn(name="article", referencedColumnName="id")
     */

    protected $article;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="name", type="string", length=255, nullable=true)
	 */
	private $name;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="value", type="text", nullable=true)
	 */
	private $value;

    public function __construct() {
        $this->translations = new ArrayCollection();
    }


	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}



    /**
     * Set name
     *
     * @param string $name
     *
     * @return Field
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Field
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set article
     *
     * @param \Cms\BlogBundle\Entity\Article $article
     *
     * @return Field
     */
    public function setArticle(\Cms\BlogBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Cms\BlogBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }
}
