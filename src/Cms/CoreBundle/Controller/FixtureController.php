<?php

namespace Cms\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class FixtureController extends Controller
{
    public function loadFixturesAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $locales = $this->container->getParameter('locales');
            foreach ($locales as $locale) {
                $this->get('cms_core.provider.fixture_test_provider')->loadArticle($locale);
                $this->get('cms_core.provider.fixture_test_provider')->loadPage($locale);
                $this->get('cms_core.provider.fixture_test_provider')->loadHouse($locale);
            }
            return new Response('Ok');
        }
        return new Response('Not Ok');
    }
}