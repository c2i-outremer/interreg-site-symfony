var endEditBloc = 0;

$(function () {
    $('body').on('mouseover', '.block', function () {
        var now = new Date().getTime();
        var diff = parseInt(now) - parseInt(endEditBloc);
        if (diff > 1000) {
            var saveThis = $(this);
            var data = {
                'id': saveThis.data('block'),
                'lang': $('html').attr('lang')
            };
            if (!saveThis.hasClass('edit-bloc-admin-dashed')) {
                $.ajax({
                    type: 'POST',
                    url: Routing.generate('bloc_edit_bloc_admin'),
                    data: data,
                    dataType: 'json'
                }).done(function (json) {
                    if (json['editBloc']) {
                        saveThis.closest('.block').addClass('edit-bloc-admin-dashed');
                        if (json['wysiwyg']) {
                            saveThis.html(json['wysiwyg']);
                        }
                        saveThis.prepend('<div class="edit-bloc-admin"></div>');
                        var editLink = '<a href="' + json['editLink'] + '" target="_blank" class="editLink" title="Edit this block ' + json['blocCategory'] + '"><i class="fa fa-wrench"></i></a>';
                        saveThis.find('.edit-bloc-admin').html(editLink);
                    }
                });
            }
            endEditBloc = new Date().getTime();
        }
    });
});

