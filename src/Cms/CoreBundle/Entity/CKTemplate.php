<?php

namespace Cms\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CKTemplate
 *
 * @ORM\Table("cms_core_cktemplate")
 * @ORM\Entity()
 */
class CKTemplate
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=255)
	 */
	protected $title;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"all"})
	 * @ORM\JoinColumn(name="image", referencedColumnName="id")
	 */
	protected $image;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255)
	 */
	protected $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="html", type="string", length=255)
	 */
	protected $html;
	
	public function __toString()
	{
		return $this->getTitle();
	}
	
	public function getId()
	{
		return $this->id;
	}

	
	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	public function getImage()
	{
		return $this->image;
	}

	public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
	{
		$this->image = $image;
		return $this;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	public function getHtml()
	{
		return $this->html;
	}

	public function setHtml($html)
	{
		$this->html = $html;
		return $this;
	}

}
