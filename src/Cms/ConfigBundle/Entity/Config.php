<?php

namespace Cms\ConfigBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Config
 *
 * @ORM\Table("cms_confg_config")
 * @ORM\Entity
 */
class Config
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="site_name", type="string", length=255, nullable=true)
     */
    private $siteName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="meta_robot", type="boolean", nullable=true)
     */
    private $metaRobot;

    /**
     * @var boolean
     *
     * @ORM\Column(name="site_online", type="boolean", nullable=true)
     */
    private $siteOnline;

    /**
     * @var boolean
     *
     * @ORM\Column(name="edit_bloc", type="boolean", nullable=true)
     */
    private $editBloc;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=255, nullable=true)
     */
    private $theme;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_articles", type="integer", nullable=true)
     * @Assert\Range(min=0)
     */
    private $nbrArticles;

    /**
     * @var string
     *
     * @ORM\Column(name="google_analytics", type="text", nullable=true)
     */
    private $googleAnalytics;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_blog_comment", type="boolean", nullable=true)
     */
    private $allowBlogComment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_publish", type="boolean", nullable=true)
     */
    private $allowPublish;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_comments", type="integer", nullable=true)
     */
    private $nbrComments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mail_status", type="boolean", nullable=true)
     */
    private $mailStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_sender", type="string", length=255, nullable=true)
     */
    private $mailSender;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_name", type="string", length=255, nullable=true)
     */
    private $mailName;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString() {
        return 'Configuration';
    }

    /**
     * Set siteName
     *
     * @param string $siteName
     *
     * @return Config
     */
    public function setSiteName($siteName)
    {
        $this->siteName = $siteName;

        return $this;
    }

    /**
     * Get siteName
     *
     * @return string
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * @return boolean
     */
    public function isMetaRobot()
    {
        return $this->metaRobot;
    }

    /**
     * @param boolean $metaRobot
     */
    public function setMetaRobot($metaRobot)
    {
        $this->metaRobot = $metaRobot;

        return $this;
    }



    /**
     * Set siteOnline
     *
     * @param boolean $siteOnline
     *
     * @return Config
     */
    public function setSiteOnline($siteOnline)
    {
        $this->siteOnline = $siteOnline;

        return $this;
    }

    /**
     * Get siteOnline
     *
     * @return boolean
     */
    public function getSiteOnline()
    {
        return $this->siteOnline;
    }

    /**
     * Set editBloc
     *
     * @param boolean $editBloc
     *
     * @return Config
     */
    public function setEditBloc($editBloc)
    {
        $this->editBloc = $editBloc;

        return $this;
    }

    /**
     * Get editBloc
     *
     * @return boolean
     */
    public function getEditBloc()
    {
        return $this->editBloc;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return Config
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set nbrArticles
     *
     * @param integer $nbrArticles
     *
     * @return Config
     */
    public function setNbrArticles($nbrArticles)
    {
        $this->nbrArticles = $nbrArticles;

        return $this;
    }

    /**
     * Get nbrArticles
     *
     * @return integer
     */
    public function getNbrArticles()
    {
        return $this->nbrArticles;
    }

    /**
     * Set googleAnalytics
     *
     * @param string $googleAnalytics
     *
     * @return Config
     */
    public function setGoogleAnalytics($googleAnalytics)
    {
        $this->googleAnalytics = $googleAnalytics;

        return $this;
    }

    /**
     * Get googleAnalytics
     *
     * @return string
     */
    public function getGoogleAnalytics()
    {
        return $this->googleAnalytics;
    }

    /**
     * Set allowBlogComment
     *
     * @param boolean $allowBlogComment
     *
     * @return Config
     */
    public function setAllowBlogComment($allowBlogComment)
    {
        $this->allowBlogComment = $allowBlogComment;

        return $this;
    }

    /**
     * Get allowBlogComment
     *
     * @return boolean
     */
    public function getAllowBlogComment()
    {
        return $this->allowBlogComment;
    }

    /**
     * Set nbrComments
     *
     * @param integer $nbrComments
     *
     * @return Config
     */
    public function setNbrComments($nbrComments)
    {
        $this->nbrComments = $nbrComments;

        return $this;
    }

    /**
     * Get nbrComments
     *
     * @return integer
     */
    public function getNbrComments()
    {
        return $this->nbrComments;
    }

    /**
     * Set mailStatus
     *
     * @param boolean $mailStatus
     *
     * @return Config
     */
    public function setMailStatus($mailStatus)
    {
        $this->mailStatus = $mailStatus;

        return $this;
    }

    /**
     * Get mailStatus
     *
     * @return boolean
     */
    public function getMailStatus()
    {
        return $this->mailStatus;
    }

    /**
     * Set mailSender
     *
     * @param string $mailSender
     *
     * @return Config
     */
    public function setMailSender($mailSender)
    {
        $this->mailSender = $mailSender;

        return $this;
    }

    /**
     * Get mailSender
     *
     * @return string
     */
    public function getMailSender()
    {
        return $this->mailSender;
    }

    /**
     * Set mailName
     *
     * @param string $mailName
     *
     * @return Config
     */
    public function setMailName($mailName)
    {
        $this->mailName = $mailName;

        return $this;
    }

    /**
     * Get mailName
     *
     * @return string
     */
    public function getMailName()
    {
        return $this->mailName;
    }

    /**
     * @return boolean
     */
    public function getAllowPublish()
    {
        return $this->allowPublish;
    }

    /**
     * @param boolean $allowPublish
     */
    public function setAllowPublish($allowPublish)
    {
        $this->allowPublish = $allowPublish;

        return $this;
    }




}

