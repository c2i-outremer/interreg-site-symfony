<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomepageController extends Controller
{
    public function pageAction()
    {
        return $this->render(':Layouts:homepage.html.twig', array(
                // ...
            ));    }

}
