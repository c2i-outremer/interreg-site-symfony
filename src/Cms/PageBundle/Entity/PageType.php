<?php

namespace Cms\PageBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Doctrine\ORM\Mapping as ORM;

/**
 * pageType
 *
 * @ORM\Table("cms_page_page_type")
 * @ORM\Entity(repositoryClass="Cms\PageBundle\Entity\PageTypeRepository")
 */
class PageType
{

    use Nameable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="template_path", type="text", nullable=true)
	 */
	private $templatePath;

    /**
     * @var boolean
     * @ORM\Column(name="dynamic", type="boolean", nullable=true)
     */
    private $dynamic;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;
    /**
     * @var string
     *
     * @ORM\Column(name="script", type="text", nullable=true)
     */
    private $script;

    /**
     * @var string
     *
     * @ORM\Column(name="style", type="text", nullable=true)
     */
    private $style;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\PageBundle\Entity\Page", mappedBy="pageType")
	 */
	private $pages;

	function __construct()
	{
		$this->pages = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set templatePath
	 *
	 * @param string $templatePath
	 * @return PageType
	 */
	public function setTemplatePath($templatePath)
	{
		$this->templatePath = $templatePath;

		return $this;
	}

	/**
	 * Get templatePath
	 *
	 * @return string 
	 */
	public function getTemplatePath()
	{
		return $this->templatePath;
	}

	/**
	 * Add pages
	 *
	 * @param \Cms\PageBundle\Entity\Page $pages
	 * @return PageType
	 */
	public function addPage(\Cms\PageBundle\Entity\Page $pages)
	{
		$this->pages[] = $pages;
		$pages->setPageType($this);
		return $this;
	}

	/**
	 * Remove pages
	 *
	 * @param \Cms\PageBundle\Entity\Page $pages
	 */
	public function removePage(\Cms\PageBundle\Entity\Page $pages)
	{
		$this->pages->removeElement($pages);
	}

	/**
	 * Get pages
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getPages()
	{
		return $this->pages;
	}


    /**
     * Set dynamic
     *
     * @param boolean $dynamic
     *
     * @return PageType
     */
    public function setDynamic($dynamic)
    {
        $this->dynamic = $dynamic;

        return $this;
    }

    /**
     * Get dynamic
     *
     * @return boolean
     */
    public function getDynamic()
    {
        return $this->dynamic;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return PageType
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set script
     *
     * @param string $script
     *
     * @return PageType
     */
    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script
     *
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * Set style
     *
     * @param string $style
     *
     * @return PageType
     */
    public function setStyle($style)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return string
     */
    public function getStyle()
    {
        return $this->style;
    }
}
