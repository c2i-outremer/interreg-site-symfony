<?php

namespace Cms\ConfigBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Cms\CoreBundle\Entity\Traits\PublishTranslatable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Html
 *
 * @ORM\Table("cms_config_html")
 * @ORM\Entity(repositoryClass="Cms\ConfigBundle\Entity\HtmlRepository")
 * @Gedmo\TranslationEntity(class="Cms\ConfigBundle\Entity\HtmlTranslation")
 */
class Html extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use Nameable;
    use PublishTranslatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\ConfigBundle\Entity\HtmlTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Html
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Html
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Remove translation
     *
     * @param \Cms\ConfigBundle\Entity\HtmlTranslation $translation
     */
    public function removeTranslation(\Cms\ConfigBundle\Entity\HtmlTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }
}
