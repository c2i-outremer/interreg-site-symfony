<?php

namespace Cms\CoreBundle\Provider;

use Cms\BlogBundle\Entity\Article;
use Doctrine\ORM\EntityManager;

class FixtureTestProvider
{

    protected $em;
    protected $dico = 'Integer elementum ipsum ac arcu elementum, suscipit aliquam est pellentesque. Vivamus est nunc, hendrerit ut volutpat vel, blandit eleifend elit. Ut ac nunc ultricies, accumsan ipsum eu, porttitor nisi. Nam ultricies lectus in iaculis dictum. Vestibulum gravida mattis nisi, in feugiat quam faucibus cursus. Mauris efficitur, urna nec eleifend lobortis, mi mi rutrum quam, vel blandit magna magna ut tellus.';

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function loadArticle($locale) {
        $art = array();

        for($i = 0; $i <= 2; $i++) {
            $art[$i] = new Article($locale);
            $art[$i]->setLocale();
            $art[$i]->setName($this->random_lipsum('stext'));
            $art[$i]->setShortContent($this->random_lipsum('mtext'));
            $art[$i]->setContent($this->random_lipsum('ltext'));
            $art[$i]->setPublished(true);
            $this->em->persist($art[$i]);
        }
        $art[0]->setTop(true);
//        $art[0]->

        $this->em->flush();
    }

    public function loadPage($locale) {

    }

    public function loadHouse($locale) {

    }

    public function random_lipsum($what = 'word') {

        $dictio = explode(' ', $this->dico);

        switch ($what)
        {
            case 'mot':
                $nbMots = 1;
                break;

            case 'stext':
                $nbMots = rand(2,6);
                break;

            case 'mtext':
                $nbMots = rand(8,16);
                break;

            case 'ltext':
                $nbMots = rand(32,64);
                break;
        }

        $tailleDico = count($dictio);
        $str ='';

        for($i = 1; $i <= $nbMots; $i++)
        {
            $str .= $dictio[rand(0,$tailleDico - 1)] . ' ';
        }
        return $str;
    }


}