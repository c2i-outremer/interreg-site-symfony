<?php

namespace Cms\PageBundle\DataFixtures\ORM;

use Cms\CoreBundle\Entity\AttributeTranslatable;
use Cms\NavBundle\Entity\Link;
use Cms\PageBundle\Entity\Page;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadPage implements FixtureInterface, ContainerAwareInterface
{

    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {


        $pages = array(
            array(
                'name' => 'Accueil',
                'metaTitle' => 'Accueil',
                'metaH1' => 'Accueil',
                'link' => array(
                    'linkType' => 'ROUTE',
                    'routeName' => 'page_homepage',
                    'routeArgs' => array(
                        '_locale' => $this->container->getParameter('locale'),
                    )
                )
            ),
            array(
                'name' => 'Contact',
                'metaTitle' => 'Contact',
                'metaH1' => 'Contact',
                'link' => array(
                    'linkType' => 'ROUTE',
                    'routeName' => 'cms_contact_contact',
                    'routeArgs' => array(
                        '_locale' => $this->container->getParameter('locale'),
                    )
                )
            ),
        );

        $menu = $manager->getRepository('CmsNavBundle:Menu')->find(1);

        foreach ($pages as $type) {
            $this->container->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(true);
            $page = new Page;
            $page->setName($type['name']);
            $page->setMetaTitle($type['metaTitle']);
            $page->setMetaH1($type['metaH1']);
            $page->setPageModel('BUILDER');
            $manager->persist($page);
            $manager->flush();
            $this->container->get('gedmo.listener.translatable')->setPersistDefaultLocaleTranslation(false);

            $link = new Link;
            $link->setName($type['name']);

            if ($type['link']['linkType'] == 'ROUTE') {
                $link->setLinkType($type['link']['linkType']);
                $link->setRouteName($type['link']['routeName']);
                foreach ($type['link']['routeArgs'] as $name => $value) {
                    $attribute = new AttributeTranslatable();
                    $attribute->setName($name);
                    $attribute->setValue($value);
                    $link->addRouteArg($attribute);
                }
            } else {
                if ($type['link']['linkType'] == 'PAGE') {
                    $link->setPage($page);
                }
            }

            $link->setParent($menu->getRootLink());

            $manager->persist($link);
            $manager->flush();
        }

    }

}
