<?php

namespace Cms\CollectionBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Doctrine\ORM\Mapping as ORM;

/**
 * CollectionType
 *
 * @ORM\Table("cms_collection_collection_type")
 * @ORM\Entity
 */
class CollectionType
{

    use Nameable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(name="dynamic", type="boolean", nullable=true)
     */
    private $dynamic = true;

    /**
     * @var string
     *
     * @ORM\Column(name="template_path", type="text", nullable=true)
     */
    private $templatePath;

    /**
     * @var string
     *
     * @ORM\Column(name="collection_content", type="text", nullable=true)
     */
    private $collectionContent;

    /**
     * @var string
     *
     * @ORM\Column(name="item_content", type="text", nullable=true)
     */
    private $itemContent;

    /**
     * @var string
     *
     * @ORM\Column(name="script", type="text", nullable=true)
     */
    private $script;

    /**
     * @var string
     *
     * @ORM\Column(name="style", type="text", nullable=true)
     */
    private $style;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set collectionContent
     *
     * @param string $collectionContent
     *
     * @return CollectionType
     */
    public function setCollectionContent($collectionContent)
    {
        $this->collectionContent = $collectionContent;

        return $this;
    }

    /**
     * Get collectionContent
     *
     * @return string
     */
    public function getCollectionContent()
    {
        return $this->collectionContent;
    }

    /**
     * Set itemContent
     *
     * @param string $itemContent
     *
     * @return CollectionType
     */
    public function setItemContent($itemContent)
    {
        $this->itemContent = $itemContent;

        return $this;
    }

    /**
     * Get itemContent
     *
     * @return string
     */
    public function getItemContent()
    {
        return $this->itemContent;
    }

    /**
     * Set script
     *
     * @param string $script
     *
     * @return CollectionType
     */
    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script
     *
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * Set style
     *
     * @param string $style
     *
     * @return CollectionType
     */
    public function setStyle($style)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return string
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Set dynamic
     *
     * @param boolean $dynamic
     *
     * @return CollectionType
     */
    public function setDynamic($dynamic)
    {
        $this->dynamic = $dynamic;

        return $this;
    }

    /**
     * Get dynamic
     *
     * @return boolean
     */
    public function getDynamic()
    {
        return $this->dynamic;
    }

    /**
     * Set templatePath
     *
     * @param string $templatePath
     *
     * @return CollectionType
     */
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;

        return $this;
    }

    /**
     * Get templatePath
     *
     * @return string
     */
    public function getTemplatePath()
    {
        return $this->templatePath;
    }
}
