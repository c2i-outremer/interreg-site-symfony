<?php

namespace Cms\BlogBundle\Form;

use Cms\BlogBundle\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InscriptionType extends AbstractType
{
    private $choices;

    public function __construct(Article $article = null){
        if($article and !$article->getLieux()->isEmpty()){
            $this->choices = $article->getLieux();
        }
        else{
            $this->choices = null;
        }
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', 'text' , array('required' => true))
            ->add('lastname', 'text', array('required' => true))
            ->add('email', 'email', array('required' => true))
            ->add('mobile', 'text', array('required' => false));
        if($this->choices){
            $builder->add('lieu', 'entity',array('class' => 'CmsBlogBundle:Lieu','choices' => $this->choices, 'placeholder'=>'Choisissez un lieu','required' => true));
        }
         else{
             $builder->add('lieu', 'entity', array('class' => 'CmsBlogBundle:Lieu', 'placeholder'=>'Choisissez un lieu','required' => false));
         }
        $builder
            ->add('message', 'textarea', array('required' => false))
            ->add('save', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\BlogBundle\Entity\Inscription'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cms_blogbundle_inscription_form';
    }
}
