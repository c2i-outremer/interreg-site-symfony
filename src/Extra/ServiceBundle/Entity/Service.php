<?php

namespace Extra\ServiceBundle\Entity;

use Cms\CoreBundle\Entity\Traits\NameTranslatable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\PublishTranslatable;
use Cms\CoreBundle\Entity\Traits\Timestampable;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Service
 *
 * @ORM\Table("extra_service_service")
 * @ORM\Entity(repositoryClass="Extra\ServiceBundle\Repository\ServiceRepository")
 * @Gedmo\TranslationEntity(class="Extra\ServiceBundle\Entity\ServiceTranslation")
 */
class Service extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use Timestampable;
    use NameTranslatable;
    use PublishTranslatable;
    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="top", type="boolean", nullable=true)
     */
    protected $top;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="cover", referencedColumnName="id", nullable=true)
     */
    protected $cover;

    /**
     * @ORM\ManyToOne(targetEntity="Cms\ConfigBundle\Entity\Html")
     * @ORM\JoinColumn(name="html", referencedColumnName="id", nullable=true)
     */
    protected $html;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="shortcontent", type="text", nullable=true)
     */
    private $shortcontent;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Extra\ServiceBundle\Entity\Field", mappedBy="service", cascade={"persist"}, orphanRemoval=true)
     */
    protected $fields;

    /**
     * @var integer
     *
     * @ORM\Column(name="numberViews", type="integer")
     */
    private $numberViews;

    /**
     * @ORM\OneToMany(targetEntity="Extra\ServiceBundle\Entity\ServiceTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Extra\ServiceBundle\Entity\Picture", mappedBy="service", cascade={"persist"}, orphanRemoval=true)
     */
    protected $pictures;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Extra\ServiceBundle\Entity\Video", mappedBy="service", cascade={"persist"}, orphanRemoval=true)
     */
    protected $videos;

    /**
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity="Extra\ServiceBundle\Entity\File", mappedBy="service", cascade={"persist"}, orphanRemoval=true)
     */
    protected $files;

    public function __construct() {
        parent::__construct();
        $this->pictures = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->fields = new ArrayCollection();
        $this->numberViews = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set top
     *
     * @param boolean $top
     *
     * @return Service
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Get top
     *
     * @return boolean
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * Set shortcontent
     *
     * @param string $shortcontent
     *
     * @return Service
     */
    public function setShortcontent($shortcontent)
    {
        $this->shortcontent = $shortcontent;

        return $this;
    }

    /**
     * Get shortcontent
     *
     * @return string
     */
    public function getShortcontent()
    {
        return $this->shortcontent;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Service
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set numberViews
     *
     * @param integer $numberViews
     *
     * @return Service
     */
    public function setNumberViews($numberViews)
    {
        $this->numberViews = $numberViews;

        return $this;
    }

    /**
     * Get numberViews
     *
     * @return integer
     */
    public function getNumberViews()
    {
        return $this->numberViews;
    }

    /**
     * Set cover
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $cover
     *
     * @return Service
     */
    public function setCover(\Application\Sonata\MediaBundle\Entity\Media $cover = null)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set html
     *
     * @param \Cms\ConfigBundle\Entity\Html $html
     *
     * @return Service
     */
    public function setHtml(\Cms\ConfigBundle\Entity\Html $html = null)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Get html
     *
     * @return \Cms\ConfigBundle\Entity\Html
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Remove translation
     *
     * @param \Extra\ServiceBundle\Entity\ServiceTranslation $translation
     */
    public function removeTranslation(\Extra\ServiceBundle\Entity\ServiceTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Add picture
     *
     * @param \Extra\ServiceBundle\Entity\Picture $picture
     *
     * @return Service
     */
    public function addPicture(\Extra\ServiceBundle\Entity\Picture $picture)
    {
        $this->pictures[] = $picture;
        $picture->setService($this);
        return $this;
    }

    /**
     * Remove picture
     *
     * @param \Extra\ServiceBundle\Entity\Picture $picture
     */
    public function removePicture(\Extra\ServiceBundle\Entity\Picture $picture)
    {
        $this->pictures->removeElement($picture);
    }

    /**
     * Get pictures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * Add video
     *
     * @param \Extra\ServiceBundle\Entity\Video $video
     *
     * @return Service
     */
    public function addVideo(\Extra\ServiceBundle\Entity\Video $video)
    {
        $this->videos[] = $video;
        $video->setService($this);
        return $this;
    }

    /**
     * Remove video
     *
     * @param \Extra\ServiceBundle\Entity\Video $video
     */
    public function removeVideo(\Extra\ServiceBundle\Entity\Video $video)
    {
        $this->videos->removeElement($video);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Add file
     *
     * @param \Extra\ServiceBundle\Entity\File $file
     *
     * @return Service
     */
    public function addFile(\Extra\ServiceBundle\Entity\File $file)
    {
        $this->files[] = $file;
        $file->setService($this);
        return $this;
    }

    /**
     * Remove file
     *
     * @param \Extra\ServiceBundle\Entity\File $file
     */
    public function removeFile(\Extra\ServiceBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add field
     *
     * @param \Extra\ServiceBundle\Entity\Field $field
     *
     * @return Service
     */
    public function addField(\Extra\ServiceBundle\Entity\Field $field)
    {
        $this->fields[] = $field;
        $field->setService($this);
        return $this;
    }

    /**
     * Remove field
     *
     * @param \Extra\ServiceBundle\Entity\Field $field
     */
    public function removeField(\Extra\ServiceBundle\Entity\Field $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFields()
    {
        return $this->fields;
    }
}
