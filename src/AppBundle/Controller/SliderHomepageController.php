<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SliderHomepageController extends Controller
{
    public function displayAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render(':SliderHomepage:display.html.twig', array(
                'topNews' => $em->getRepository('CmsBlogBundle:Article')->findArticle(null, null, null, 10, $request->getLocale(),true,array('top' => true)),
            ));    }

}
