<?php

namespace Extra\HouseBundle\Repository;

use Gedmo\Translatable\Entity\Repository\TranslationRepository;

/**
 * HouseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class HouseRepository extends TranslationRepository
{

    public function getHouses($_locale, $extra = array())
    {

        $qb = $this
            ->createQueryBuilder('house')
            ->leftJoin('house.cover', 'cover')->addSelect('cover')
            ->leftJoin('house.translations', 'translations')->addSelect('translations')
            ->andWhere('house.published = :published')->setParameter('published', true)
            ->orderBy('house.position', 'DESC');

        if(isset($extra['top'])) {
            $qb->andWhere('house.top = :top')->setParameter('top', $extra['top']);
        }

        $query = $qb->getQuery();

        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $_locale);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1);

        return $query->getResult();
    }

    public function findHouse($identifier, $_locale)
    {
        $qb = $this
            ->createQueryBuilder('house')
            ->leftJoin('house.cover', 'cover')->addSelect('cover')
            ->leftJoin('house.translations', 'translations')->addSelect('translations')
            ->leftJoin('house.contents', 'contents')->addSelect('contents')
            ->leftJoin('house.equipments', 'equipments')->addSelect('equipments')
            ->leftJoin('house.pictures', 'pictures')->addSelect('pictures')
            ->leftJoin('pictures.media', 'picturesMedia')->addSelect('picturesMedia')
            ->leftJoin('house.videos', 'videos')->addSelect('videos')
            ->leftJoin('house.files', 'files')->addSelect('files')
            ->leftJoin('house.prices', 'prices')->addSelect('prices')
            ->andWhere('house.published = :published')->setParameter('published', true)
         ;

        if(is_integer($identifier)) {
            $qb->andWhere('house.id = :id')->setParameter('id', $identifier);
        } else {
            $qb->andWhere('house.slug = :slug')->setParameter('slug', $identifier);
        }

        $query = $qb->getQuery();

        $query->setHint(\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker');
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $_locale);
        $query->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1);

        return $query->getOneOrNullResult();
    }
}
