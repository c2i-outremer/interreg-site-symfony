<?php

namespace Cms\PageBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PageTypeAdmin extends Admin
{

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('templatePath')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array('editable' => true, 'label' => 'Nom'))
            ->add('templatePath', null, array('editable' => true, 'label' => 'Template Twig'))
            ->add('dynamic', null, array('label' => 'Layout dynamique'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array('label'=> 'Nom', 'attr' => array('autofocus' => 'autofocus')))
            ->add('dynamic', null, array('label' => 'Page dynamique'))
            ->add('templatePath', null, array('label'=> 'Chemin vers le template TWIG'))
            ->add('content', null, array('label' => 'Contenu TWIG'))
            ->add('script', null, array('label' => 'JavaScript'))
            ->add('style', null, array('label' => 'CSS'))
        ;
    }

	/**
	 * @param ShowMapper $showMapper
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
				->add('id')
				->add('templatePath')
				->add('created')
				->add('updated')
				->add('position')
				->add('published')
				->add('name')
				->add('slug')
		;
	}

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CmsPageBundle:Admin:page_type_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

}
