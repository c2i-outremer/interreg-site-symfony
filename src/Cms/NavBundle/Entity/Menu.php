<?php

namespace Cms\NavBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\Publishable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * menu
 *
 * @ORM\Table("cms_nav_menu")
 * @ORM\Entity(repositoryClass="Cms\NavBundle\Entity\MenuRepository")
 */
class Menu
{

    use Timestampable;
    use Nameable;
    use Publishable;
    use Positionable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;


	/**
	 * @ORM\ManyToOne(targetEntity="Cms\NavBundle\Entity\MenuType", inversedBy="menus")
	 * @ORM\JoinColumn(name="menuType", referencedColumnName="id", nullable=true)
	 * */
	private $menuType;

	/**
	 * @ORM\OneToOne(targetEntity="Cms\NavBundle\Entity\Link", cascade={"persist"})
	 * *@ORM\JoinColumn(name="rootLink", referencedColumnName="id", nullable=true)
	 * */
	private $rootLink;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\BlocBundle\Entity\Bloc", mappedBy="menu")
	 * */
	private $blocs;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->blocs = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set menuType
	 *
	 * @param \Cms\NavBundle\Entity\MenuType $menuType
	 * @return Menu
	 */
	public function setMenuType(\Cms\NavBundle\Entity\MenuType $menuType)
	{
		$this->menuType = $menuType;

		return $this;
	}

	/**
	 * Get menuType
	 *
	 * @return \Cms\NavBundle\Entity\MenuType 
	 */
	public function getMenuType()
	{
		return $this->menuType;
	}

	/**
	 * Add blocs
	 *
	 * @param \Cms\BlocBundle\Entity\Bloc $blocs
	 * @return Menu
	 */
	public function addBloc(\Cms\BlocBundle\Entity\Bloc $blocs)
	{
		$this->blocs[] = $blocs;
		$blocs->setMenu($this);
		return $this;
	}

	/**
	 * Remove blocs
	 *
	 * @param \Cms\BlocBundle\Entity\Bloc $blocs
	 */
	public function removeBloc(\Cms\BlocBundle\Entity\Bloc $blocs)
	{
		$this->blocs->removeElement($blocs);
	}

	/**
	 * Get blocs
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getBlocs()
	{
		return $this->blocs;
	}

	/**
	 * Set rootLink
	 *
	 * @param \Cms\NavBundle\Entity\Link $rootLink
	 * @return Menu
	 */
	public function setRootLink(\Cms\NavBundle\Entity\Link $rootLink = null)
	{
		$this->rootLink = $rootLink;

		return $this;
	}

	/**
	 * Get rootLink
	 *
	 * @return \Cms\NavBundle\Entity\Link 
	 */
	public function getRootLink()
	{
		return $this->rootLink;
	}

}
