<?php

namespace Cms\BlogBundle\Services\Twig;

use Cms\ConfigBundle\Services\GetConfig;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;

class ShowBlog extends \Twig_Extension
{
    protected $manager;
    protected $twig;
    protected $config;
    protected $request;

    function __construct(EntityManager $manager, \Twig_Environment $twig, GetConfig $config, RequestStack $request)
    {
        $this->manager = $manager;
        $this->config = $config;
        $this->twig = $twig;
        $this->request = $request;
    }


    public function getName()
    {
        return 'showBlog';
    }

    public function getFunctions()
    {
        return array(
            'showBlog' => new \Twig_Function_Method($this, 'showBlog')
        );
    }

    public function showBlog($bloc) {

        $nbrArticles = (int) $this->config->getConfig()->getNbrArticles();

        $bloc = $this->manager->getRepository('CmsBlocBundle:Bloc')->getBloc((int) $bloc);

        $posts = $this->manager->getRepository('CmsBlogBundle:Article')->findArticle($bloc->getBlogCategorys()->toArray(), null, 0, $nbrArticles, $this->request->getCurrentRequest()->getLocale());

        return $this->twig->render(
            ':Blog:bloc.html.twig',
            array(
                'bloc' => $bloc,
                'posts' => $posts
            )
        );

    }
}