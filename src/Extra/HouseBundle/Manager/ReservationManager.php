<?php

namespace Extra\HouseBundle\Manager;

use Cms\CoreBundle\Manager\BaseManager;

class ReservationManager extends BaseManager
{

    public function save($reservation)
    {
        $this->persistAndFlush($reservation);
    }

}