<?php

namespace Cms\BlogBundle\Admin;

use Cms\BlogBundle\Entity\ArticleTranslation;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\SecurityContextInterface;

class ArticleAdmin extends Admin
{

    public $last_position = 0;
    private $container;
    private $positionService;
    protected $securityContext;

    public function setSecurityContext(SecurityContextInterface $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'position',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('created', null, array('label' => 'Date'))
            ->add('name', null, array('label' => 'Nom'))
            ->add('slug')
            ->add('author', null, array('label' => 'Auteur'))
            ->add('user', null, array('label' => 'Utilisateur'))
            ->add('category', null, array('label' => 'Catégorie'))
            ->add('articleType', null, array('label' => 'Template'))
            ->add('published', null, array('label' => 'Publication'))
            ->add('top', null, array('label' => 'Top'))
            ->add('enableComment', null, array('label' => 'Commentaire'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        $container = $this->getConfigurationPool()->getContainer();

        $config = $container->get('cms_config.GetConfig')->getConfig();
        $user = $container->get('security.token_storage')->getToken()->getUser();

        $listMapper
            //->add('created', null, array('label' => 'Date'))
            ->add('cover', null, array('template' => 'CmsCoreBundle:Admin:list_image.html.twig', 'label' => 'Couverture'))
            ->add('name', null, array('label' => 'Nom', 'editable' => true))
            ->add('date_event', 'date', array('label' => "Date de l'évènement"))
            ->add('user', null, array('label' => 'User'))
            ->add('category', null, array('label' => 'Catégorie'))
            ->add('tags', null, array('label' => 'Mots-cés'))
            ->add('articleType', null, array('label' => 'Template'));
        if ($user->hasGroup('Journaliste')) {
            if ($config->getAllowPublish()) {
                $listMapper->add('published', null, array('label' => 'Publication', 'editable' => true));
            }
        } else {
            $listMapper->add('published', null, array('label' => 'Publication', 'editable' => true));
        }

        $listMapper->add('top', null, array('label' => 'Top', 'editable' => true))
            ->add('allowInscriptions', null, array('label' => 'Activer la possibilité de s\'inscrire', 'editable' => true))
            ->add('enableComment', null, array('label' => 'Commentable', 'editable' => true))
            ->add('numberView', null, array('label' => 'Visite(s)'))
            ->add('nbrComment', 'string', array('label' => 'Commentaire(s)', 'template' => 'CmsBlogBundle:Admin:nbr_comments.html.twig'))
            ->add('language', 'string', array('template' => 'CmsCoreBundle:Admin:selected_lang.html.twig', 'label' => 'Langue'))
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                        'move' => array('template' => 'CmsCoreBundle:Admin:sort_reverse.html.twig'),
                        'stat' => array('template' => 'CmsCoreBundle:Admin:button_stat.html.twig'),
                    )
                )
            );
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $authorisationChecker = $container->get('security.authorization_checker');
        $config = $container->get('cms_config.GetConfig')->getConfig();
        $user = $container->get('security.token_storage')->getToken()->getUser();

        $formMapper
            ->with('Contenu', array('class' => 'col-md-8 left-col'))
            ->add('name', 'text', array('label' => 'Nom de l\'article', 'help' => 'trans', 'attr' => array('autofocus' => 'autofocus')))
            ->add('date_event', 'datetime', array('required' => false,'label' => "Date de l'évènement"))
            ->add('shortContent', 'ckeditor', array('required' => false, 'help' => 'trans', 'label' => 'Chapeau'))
            ->add('content', 'ckeditor', array('required' => false, 'help' => 'trans', 'label' => 'Contenu'));
        if ($authorisationChecker->isGranted('ROLE_CMS_BLOG_ADMIN_ARTICLE_MASTER') || $authorisationChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $formMapper->add('extraContent', null, array('required' => false, 'help' => 'trans', 'label' => 'Contenu TWIG aditionnel'));
        }
        $formMapper->add(
            'fields',
            'sonata_type_collection',
            array('required' => false, 'by_reference' => false, 'help' => 'trans', 'label' => 'Champs supplémentaires', 'type_options' => array('delete' => true)),
            array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',)
        );
        $formMapper->end()
            ->with('Propriété', array('class' => 'col-md-4 right-col'))
            ->add(
                'cover',
                'sonata_type_model_list',
                array('required' => false, 'label' => false),
                array('link_parameters' => array('context' => 'picture', 'provider' => 'sonata.media.provider.image'))
            )
            ->add('slug', null, array('help' => 'trans', 'attr' => array('placeholder' => 'Laissez vide par défaut')))
            ->add('top', null, array('label' => 'Article à la une', 'required' => false))
            ->add('lieux', 'sonata_type_model', array('required' => false, 'label' => 'Lieux', 'multiple' => true))
            ->add('allowInscriptions', null, array('label' => 'Activer la possibilité de s\'inscrire', 'required' => false));
        if ($user->hasGroup('Journaliste')) {
            if ($config->getAllowPublish()) {
                $formMapper->add('published', null, array('label' => 'Publication', 'label_attr' => array('class' => 'help-trans')));
            }
        } else {
            $formMapper->add('published', null, array('label' => 'Publication', 'label_attr' => array('class' => 'help-trans')));

        }

        $formMapper->add('publishStartDate', 'datetime', array('label' => 'Date de publication'))
            ->add('enableComment', null, array('label' => 'Activer les commentaires'))
            ->add('articleType', null, array('label' => 'Template', 'required' => true))
            ->add('user', null, array('required' => false, 'label' => 'Utilisateur système', 'disabled' => true))
            ->add('category', null, array('label' => 'Catégorie'))
            ->add('author', 'text', array('required' => false, 'label' => 'Auteur'))
            ->add('numberView', null, array('label' => 'Nombre de vues', 'disabled' => true))
            ->add(
                'pictures',
                'sonata_type_collection',
                array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)),
                array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',)
            )
            ->add(
                'videos',
                'sonata_type_collection',
                array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)),
                array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',)
            )
            ->add(
                'files',
                'sonata_type_collection',
                array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)),
                array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',)
            )
            ->add('tags', 'sonata_type_model', array('required' => false, 'label' => 'Mots clés', 'multiple' => true))
            ->end();

        $formMapper->with('Inscriptions', array('class' => 'col-xs-12'))

            ->add(
                'inscriptions',
                'sonata_type_collection',
                array('required' => false, 'by_reference' => false, 'label' => 'Inscriptions', 'type_options' => array('delete' => true)),
                array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',)
            )
            ->end();

    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('shortContent')
            ->add('content')
            ->add('extraContent')
            ->add('author')
            ->add('created')
            ->add('updated')
            ->add('position')
            ->add('published')
            ->add('name')
            ->add('slug');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
        $collection->add('stat', $this->getRouterIdParameter().'/stat');
        $collection->add('result', $this->getRouterIdParameter().'/result');
        $collection->add('publish', $this->getRouterIdParameter().'/publish');
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        if ($context == 'list') {
            if (!$this->securityContext->isGranted('ROLE_CMS_BLOG_ADMIN_ARTICLE_MASTER') and !$this->securityContext->isGranted('ROLE_SUPER_ADMIN')) {
                $id = $this->securityContext->getToken()->getUser()->getId();

                $query
                    ->andWhere($query->expr()->eq('o.user', ':id'))
                    ->setParameter('id', $id);
            }
        }

        return $query;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        $actions['publish'] = array('label' => 'Publier', 'ask_confirmation' => false);

        return $actions;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CmsBlogBundle:Admin:article_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $locales = $container->getParameter('locales');
        $defaultLocale = $container->getParameter('locale');
        $em = $container->get('doctrine.orm.entity_manager');

        $fields = array(
            'published' => 0,
        );

        foreach ($locales as $locale) {
            foreach ($fields as $key => $value) {
                if ($locale != $defaultLocale) {
                    $translation = new ArticleTranslation($locale, $key, $value);
                    $translation->setObject($object);
                    $em->persist($translation);
                }
            }
        }

        $em->flush();

    }

}
