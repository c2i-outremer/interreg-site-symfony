<?php

namespace Extra\HouseBundle\Manager;

use Cms\CoreBundle\Entity\Visite;
use Cms\CoreBundle\Manager\BaseManager;
use Extra\HouseBundle\Entity\House;


class HouseManager extends BaseManager
{

    public function getList($_locale, $extra = array())
    {
        return $this->repo->getHouses($_locale, $extra);
    }

    public function find($identifier, $_locale)
    {
        return $this->repo->findHouse($identifier, $_locale);
    }

    public function updateViste(House $house, $_locale)
    {
        $house->setLocale($_locale);
        $house->setNumberView($house->getNumberView() + 1);
        $this->persistAndFlush($house);
        $visite = new Visite('house', $house->getSlug(), $house->getId());
        $this->persistAndFlush($visite);
    }

}