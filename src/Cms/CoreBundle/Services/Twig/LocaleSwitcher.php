<?php

namespace Cms\CoreBundle\Services\Twig;

use Cms\BlogBundle\Entity\Article;
use Cms\BlogBundle\Entity\Category;
use Cms\BlogBundle\Entity\Tag;
use Cms\PageBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\TwigBundle\Extension\AssetsExtension;

class LocaleSwitcher extends \Twig_Extension
{

    protected $router;
    protected $asset;
    protected $locales;

    function __construct(Router $router, AssetsExtension $asset)
    {
        $this->router = $router;
        $this->asset = $asset;
    }

    public function setLocales($locales)
    {
        $this->locales = $locales;
    }

    public function getName()
    {
        return 'localeSwitcher';
    }

    public function getFunctions()
    {
        return array(
            'localeSwitcher' => new \Twig_Function_Method($this, 'localeSwitcher'),
        );
    }

    public function localeSwitcher($type, $data)
    {
        switch ($type) {
            case 'page' :
                return $this->getLocalePage($data);
            case 'article' :
                return $this->getLocaleArticle($data);
            case 'category' :
                return $this->getLocaleCategory($data);
            case 'tag' :
                return $this->getLocaleTag($data);
        }
    }


    private function getLocalePage(Page $page)
    {
        $html = '';

        foreach ($this->locales as $_locale) {
            if ($page->getTranslation('published', $_locale)) {
                $pageLink = ($page->getFolder())
                    ? $page->getFolder()->getTranslation('slug', $_locale).'/'.$page->getTranslation('slug', $_locale)
                    : $page->getTranslation('slug', $_locale);
                $href = $this->router->generate('page_show', array('pageLink' => $pageLink, '_locale' => $_locale));
                $src = $this->asset->getAssetUrl('assets/img/flags/'.$_locale.'.png');
                $html .= '<a href="'.$href.'"><img src="'.$src.'" /></a>';
            }
        }

        return $html;
    }

    private function getLocaleArticle(Article $article)
    {
        $html = '';

        foreach ($this->locales as $_locale) {
            if ($article->getTranslation('published', $_locale)) {
                $articleLink = ($article->getCategory())
                    ? $article->getCategory()->getTranslation('slug', $_locale).'/'.$article->getTranslation('slug', $_locale)
                    : $article->getTranslation('slug', $_locale);
                $href = $this->router->generate('blog_article_show', array('articleLink' => $articleLink, '_locale' => $_locale));
                $src = $this->asset->getAssetUrl('assets/img/flags/'.$_locale.'.png');
                $html .= '<a href="'.$href.'"><img src="'.$src.'" /></a>';
            }
        }

        return $html;
    }

    private function getLocaleCategory(Category $category)
    {
        $html = '';

        foreach ($this->locales as $_locale) {
            if(!is_null($category->getTranslation('slug', $_locale))) {
                $href = $this->router->generate('cms_blog_list_category', array('categorySlug' => $category->getTranslation('slug', $_locale), '_locale' => $_locale));
                $src = $this->asset->getAssetUrl('assets/img/flags/'.$_locale.'.png');
                $html .= '<a href="'.$href.'"><img src="'.$src.'" /></a>';
            }
        }

        return $html;
    }
    private function getLocaleTag(Tag $tag)
    {
        $html = '';

        foreach ($this->locales as $_locale) {
            if(!is_null($tag->getTranslation('slug', $_locale))) {
                $href = $this->router->generate('cms_blog_list_tag', array('tagSlug' => $tag->getTranslation('slug', $_locale), '_locale' => $_locale));
                $src = $this->asset->getAssetUrl('assets/img/flags/'.$_locale.'.png');
                $html .= '<a href="'.$href.'"><img src="'.$src.'" /></a>';
            }
        }

        return $html;
    }
}