<?php

namespace Extra\HouseBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ReservationAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('start')
            ->add('end')
            ->add('nbrAdult')
            ->add('nbrChild')
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('mobile')
            ->add('published')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('house', null, array('label' => 'Logement'))
            ->add('start', null, array('label' => 'Début'))
            ->add('end', null, array('label' => 'Fin'))
            ->add('nbrAdult', null, array('label' => 'Adulte'))
            ->add('nbrChild', null, array('label' => 'Enfant'))
            ->add('firstname', null, array('label' => 'Prénom'))
            ->add('lastname', null, array('label' => 'Nom'))
            ->add('email', null, array('label' => 'Email'))
            ->add('mobile', null, array('label' => 'Mobile'))
            ->add('message', null, array('label' => 'Message'))
            ->add('published', null, array('label' => 'Traité ?', 'editable' => true))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Logement', array('class' => 'col-md-4'))
                ->add('house', null, array('label' => 'Logement', 'empty_value' => 'Choisissez un logement',))
                ->add('published', null, array('label' => 'Traité ?'))
            ->end()
            ->with('Systeme', array('class' => 'col-md-4'))
                ->add('created', 'date', array('widget' => 'single_text', 'label' => 'Création', 'disabled' => true))
                ->add('updated', 'date', array('widget' => 'single_text', 'label' => 'Modification', 'disabled' => true))
            ->end()
            ->with('Date', array('class' => 'col-md-4 clearfix'))
                ->add('start', 'date', array('widget' => 'single_text', 'label' => 'Début'))
                ->add('end', 'date', array('widget' => 'single_text'))
            ->end()
            ->with('Personne', array('class' => 'col-md-4'))
                ->add('nbrAdult', null, array('label' => 'Adulte'))
                ->add('nbrChild', null, array('label' => 'Enfant'))
            ->end()
            ->with('Civilité', array('class' => 'col-md-4'))
                ->add('firstname', null, array('label' => 'Prénom'))
                ->add('lastname', null, array('label' => 'Nom'))
            ->end()
            ->with('Coordonnée', array('class' => 'col-md-4'))
                ->add('email', null, array('label' => 'Email'))
                ->add('mobile', null, array('label' => 'Mobile'))
            ->end()
            ->with('Message', array('class' => 'col-md-12'))
                ->add('message', null, array('label' => 'Message'))
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('start')
            ->add('end')
            ->add('nbrAdult')
            ->add('nbrChild')
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('mobile')
            ->add('published')
        ;
    }
}
