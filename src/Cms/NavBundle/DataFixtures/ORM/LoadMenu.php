<?php

namespace Cms\NavBundle\DataFixtures\ORM;

use Cms\NavBundle\Entity\LinkType;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Cms\NavBundle\Entity\Menu;
use Cms\NavBundle\Entity\MenuType;

class LoadMenu implements FixtureInterface
{

	public function load(ObjectManager $manager)
	{

		// les type

		$menusType = array(
			array('name' => 'Aucun', 'templatePath' => '',),
			array('name' => 'Tabs', 'templatePath' => 'tabs',),
			array('name' => 'Stacked Tabs', 'templatePath' => 'stacked-tabs',),
			array('name' => 'Justified Tabs', 'templatePath' => 'justified-tabs',),
			array('name' => 'Pills', 'templatePath' => 'pills',),
			array('name' => 'Stacked pills', 'templatePath' => 'stacked-pills',),
			array('name' => 'Justified pills', 'templatePath' => 'justified-pills',),
			array('name' => 'List', 'templatePath' => 'list',),
			array('name' => 'Navbar', 'templatePath' => 'navbar',),
			array('name' => 'Navbar Right', 'templatePath' => 'navbar-right',),
		);

		foreach ($menusType as $key => $value)
		{
			$MenuType[$key] = new MenuType;
			$MenuType[$key]->setName($value['name']);
			$MenuType[$key]->setTemplatePath($value['templatePath']);
			$manager->persist($MenuType[$key]);
		}

		$data = array(
			'name' => 'Main menu',
			'style' => 'justified-pills',
		);

		$entity = new Menu;
		$entity->setName($data['name']);
		$entity->setMenuType($MenuType[9]);

		$manager->persist($entity);

        $manager->flush();

	}

}
