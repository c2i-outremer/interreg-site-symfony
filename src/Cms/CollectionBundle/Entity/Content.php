<?php

namespace Cms\CollectionBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Content
 *
 * @ORM\Table("cms_collection_content")
 * @ORM\Entity
 * @Gedmo\TranslationEntity(class="Cms\CollectionBundle\Entity\ContentTranslation")
 */
class Content extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\CollectionBundle\Entity\ContentTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Cms\CollectionBundle\Entity\Item", inversedBy="contents")
     * @ORM\JoinColumn(name="item", referencedColumnName="id")
     */

    protected $item;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Content
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set item
     *
     * @param \Cms\CollectionBundle\Entity\Item $item
     *
     * @return Content
     */
    public function setItem(\Cms\CollectionBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Cms\CollectionBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
