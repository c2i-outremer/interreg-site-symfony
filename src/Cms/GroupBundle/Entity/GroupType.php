<?php

namespace Cms\GroupBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\Publishable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * groupType
 *
 * @ORM\Table("cms_group_group_type")
 * @ORM\Entity(repositoryClass="Cms\GroupBundle\Entity\GroupTypeRepository")
 */
class GroupType
{

    use Timestampable;
    use Nameable;
    use Publishable;
    use Positionable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="templatePath", type="string", length=255)
	 */
	private $templatePath;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\GroupBundle\Entity\Group", mappedBy="groupType")
	 * */
	private $groups;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->groups = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set templatePath
	 *
	 * @param string $templatePath
	 * @return GroupType
	 */
	public function setTemplatePath($templatePath)
	{
		$this->templatePath = $templatePath;

		return $this;
	}

	/**
	 * Get templatePath
	 *
	 * @return string 
	 */
	public function getTemplatePath()
	{
		return $this->templatePath;
	}

	/**
	 * Add groups
	 *
	 * @param \Cms\GroupBundle\Entity\Group $groups
	 * @return GroupType
	 */
	public function addGroup(\Cms\GroupBundle\Entity\Group $groups)
	{
		$this->groups[] = $groups;
		$groups->setGroupType($this);
		return $this;
	}

	/**
	 * Remove groups
	 *
	 * @param \Cms\GroupBundle\Entity\Group $groups
	 */
	public function removeGroup(\Cms\GroupBundle\Entity\Group $groups)
	{
		$this->groups->removeElement($groups);
	}

	/**
	 * Get groups
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getGroups()
	{
		return $this->groups;
	}

}
