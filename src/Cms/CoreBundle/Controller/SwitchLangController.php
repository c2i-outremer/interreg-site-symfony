<?php

namespace Cms\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SwitchLangController extends Controller
{

    public function adminAction(Request $request, $_locale)
    {
        if ($request->isXmlHttpRequest()) {
            $request->setLocale($_locale);
            return new JsonResponse(true);
        }
    }

}
