<?php

namespace Extra\HouseBundle\Admin;

use Extra\HouseBundle\Entity\HouseTranslation;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class HouseAdmin extends Admin
{

    public $last_position = 0;
    private $container;
    private $positionService;

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function setPositionService(\Pix\SortableBehaviorBundle\Services\PositionHandler $positionHandler)
    {
        $this->positionService = $positionHandler;
    }

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'position',
    );

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('numberView')
            ->add('content')
            ->add('name')
            ->add('slug')
            ->add('published')
            ->add('created')
            ->add('updated')
            ->add('position')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('cover', null, array('template' => 'CmsCoreBundle:Admin:list_image.html.twig', 'label' => 'Couverture'))
            ->add('name', null, array('label' => 'Nom'))
            ->add('numberView', 'string', array('template' => 'CmsCoreBundle:Admin:numb_visit_by_lang.html.twig', 'label' => 'Nombre de visites'))
            ->add('top', null, array('label' => 'A la une', 'editable' => true))
            ->add('language', 'string', array('template' => 'CmsCoreBundle:Admin:selected_lang.html.twig', 'label' => 'Langue(s) de publication'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'move' => array('template' => 'CmsCoreBundle:Admin:sort_reverse.html.twig'),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Identification', array('class' => 'col-sm-4'))
                ->add('name', null, array('label' => 'Nom', 'help' => 'trans'))
                ->add('slug', null, array('label' => 'Slug', 'help' => 'trans', 'disabled' => true))
            ->end()
            ->with('Publication', array('class' => 'col-sm-4'))
                ->add('published', null, array('label' => 'Publication', 'label_attr' => array('class' => 'help-trans')))
                ->add('top', null, array('label' => 'A la une'))
                ->add('numberView', null, array('label' => 'Nombre de visites', 'disabled' => true, 'help' => 'trans'))
                ->add('allowComment', null, array('label' => 'Activer les commentaires'))
            ->end()
            ->with('Couverture', array('class' => 'col-sm-4'))
                ->add('cover', 'sonata_type_model_list',array('required' => false, 'label' => false),array('link_parameters' => array('context' => 'picture', 'provider' => 'sonata.media.provider.image')))
            ->end()
            ->with('Contenu', array('class' => 'col-sm-12'))
                ->add('content', 'ckeditor', array('label' => ' ', 'help' => 'trans'))
            ->end()
            ->with('Champs supplémentaires', array('class' => 'col-sm-12', 'help' => 'trans'))
                ->add('contents', 'sonata_type_collection', array('required' => false, 'help' => 'trans', 'by_reference' => false, 'label' => ' ', 'type_options' => array('delete' => true)), array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            ->end()
            ->with('Fichiers', array('class' => 'col-sm-6'))
                ->add('files','sonata_type_collection',array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            ->end()
            ->with('Vidéos', array('class' => 'col-sm-6'))
                ->add('videos','sonata_type_collection',array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            ->end()
            ->with('Images', array('class' => 'col-sm-6'))
                ->add('pictures', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => false, 'type_options' => array('delete' => true)),array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            ->end()
            ->with('Equipements', array('class' => 'col-sm-6'))
                ->add('equipments', 'sonata_type_model', array('required' => false, 'label' => false, 'multiple' => true, 'expanded' => true))
            ->end()
            ->with('Prix', array('class' => 'col-sm-12'))
                ->add('prices', 'sonata_type_collection', array('required' => false, 'by_reference' => false, 'label' => ' ', 'help' => 'trans', 'type_options' => array('delete' => true)), array('edit' => 'inline', 'inline' => 'table', 'sortable' => 'position',))
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('numberView')
            ->add('content')
            ->add('name')
            ->add('slug')
            ->add('published')
            ->add('created')
            ->add('updated')
            ->add('position')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $locales = $container->getParameter('locales');
        $defaultLocale = $container->getParameter('locale');
        $em = $container->get('doctrine.orm.entity_manager');

        $fields = array(
            'published' => 0,
        );

        foreach ($locales as $locale) {
            foreach ($fields as $key => $value) {
                if ($locale != $defaultLocale) {
                    $translation = new HouseTranslation($locale, $key, $value);
                    $translation->setObject($object);
                    $em->persist($translation);
                }
            }
        }

        $em->flush();

    }
}
