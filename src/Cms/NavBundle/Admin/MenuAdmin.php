<?php

namespace Cms\NavBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class MenuAdmin extends Admin
{

	/**
	 * @param DatagridMapper $datagridMapper
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
				->add('name')
				->add('slug')
				->add('menuType')				
		;
	}

	/**
	 * @param ListMapper $listMapper
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
				->add('name')
				->add('slug')
				->add('menuType')				
				->add('style')				
				->add('_action', 'actions', array(
					'actions' => array(
						'show' => array(),
						'edit' => array(),
						'delete' => array(),
					)
				))
		;
	}

	/**
	 * @param FormMapper $formMapper
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper
				->with('Identification', array('class' => 'col-md-6'))
				->add('name', null, array('label' => 'Nom', 'attr' => array('autofocus' => 'autofocus')))
				->add('slug')
				->end()
				->with('Intégration', array('class' => 'col-md-6'))
				->add('menuType', 'entity', array('class' => 'Cms\NavBundle\Entity\MenuType', 'required' => true, 'label' => 'Type Bootstrap'))
				->end()
		;
	}

	/**
	 * @param ShowMapper $showMapper
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
				->add('name')
				->add('slug')
		;
	}
	
	public function getTemplate($name) {
		switch ($name) {
			case 'edit':
				return 'CmsNavBundle:Admin:menu_edit.html.twig';
				break;
			default:
				return parent::getTemplate($name);
				break;
		}
	}

}
