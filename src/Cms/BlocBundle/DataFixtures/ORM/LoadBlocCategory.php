<?php

namespace Cms\BlocBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Cms\BlocBundle\Entity\BlocCategory;

class LoadBlocCategory implements FixtureInterface
{

	public function load(ObjectManager $manager)
	{
		$names = array(
			'Texte',
			'WYSIWYG',
			'Dynamique',
			'Groupe',
			'Menu',
			'Collection',
			'Blog',
		);

		foreach ($names as $name)
		{
			$item = new BlocCategory();
			$item->setName($name);
			$manager->persist($item);
		}

		$manager->flush();
	}

}
