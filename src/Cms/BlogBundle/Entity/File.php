<?php

namespace Cms\BlogBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Positionable;
use Doctrine\ORM\Mapping as ORM;

/**
 * File
 *
 * @ORM\Table("cms_blog_file")
 * @ORM\Entity
 */
class File
{

    use Positionable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="cover", referencedColumnName="id", nullable=true)
     */
    protected $media;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Cms\BlogBundle\Entity\Article", inversedBy="files")
     * @ORM\JoinColumn(name="article", referencedColumnName="id")
     */

    protected $article;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set article
     *
     * @param \Cms\BlogBundle\Entity\Article $article
     *
     * @return Picture
     */
    public function setArticle(\Cms\BlogBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Cms\BlogBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     *
     * @return Picture
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }
}