<?php

namespace Cms\BlocBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BlocController extends Controller
{
    //------------------------------------------------------------------------------------------------------------------
    //	showAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Show a bloc in good template
     * @param type $bloc
     * @return type
     */
    public function showAction($bloc, $getQuery, $otherParams)
    {
        $templatePath = ($bloc['templatePath']) ? $bloc['templatePath'] : 'CmsBlocBundle:Type:default.html.twig';

        return $this->render(
            $templatePath,
            array(
                'bloc' => $bloc,
                'getQuery' => $getQuery,
                'otherParams' => $otherParams,
            )
        );
    }

    //------------------------------------------------------------------------------------------------------------------
    //	editBlocAdminAction
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Edit Bloc from Front => AJAX
     * @param Request $request
     * @return JsonResponse
     */
    public function editBlocAdminAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();


            if ((bool)$this->get('cms_config.GetConfig')->getConfig()->getEditBloc()) {
                $blocId = (int)$request->request->get('id');
                $_locale = $request->request->get('lang');

                if (!in_array($_locale, $this->container->getParameter('locales'))) {
                    return false;
                }

                $request->setLocale($_locale);

                $wysiwyg = false;

                $editLink = $this->generateUrl('admin_cms_bloc_bloc_edit', array('id' => $blocId, 'tl' => $_locale));
                $bloc = $em->getRepository('CmsBlocBundle:Bloc')->find($blocId);

                if ($bloc->getBlocCategory()->getId() == 2) // Wysiwyg
                {
                    if ($bloc->getAllowEditFront()) {
                        $wysiwyg = $this->render(
                            'CmsBlocBundle:Admin:x_editable_wisywig.html.twig',
                            array(
                                'bloc' => $bloc,
                                '_locale' => $_locale
                            )
                        )->getContent();
                    }
                }

                if ($bloc->getBlocCategory()->getId() == 4) // group
                {
                    $editLink = $this->generateUrl('admin_cms_group_group_edit', array('id' => $bloc->getGroup()->getId(), 'tl' => $_locale));
                }

                if ($bloc->getBlocCategory()->getId() == 6) // collection
                {
                    $editLink = $this->generateUrl('admin_cms_collection_collection_edit', array('id' => $bloc->getCollection()->getId(), 'tl' => $_locale));
                }

                if ($bloc->getBlocCategory()->getId() == 5) // menu
                {
                    $editLink = $this->generateUrl('admin_cms_nav_menu_edit', array('id' => $bloc->getMenu()->getId(), 'tl' => $_locale));
                }

                return new JsonResponse(
                    array(
                        'blocCategory' => $bloc->getBlocCategory()->getName(),
                        'blocId' => $blocId,
                        'editLink' => $editLink,
                        'editBloc' => true,
                        'wysiwyg' => $wysiwyg,
                    )
                );
            } else {
                return new JsonResponse(
                    array(
                        'editBloc' => false,
                    )
                );
            }
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */

    public function editBlocWysiwygAction(Request $request, $_locale)
    {
        if (!in_array($_locale, $this->container->getParameter('locales'))) {
            return false;
        }

        $request->setLocale($_locale);

        $em = $this->getDoctrine()->getManager();
        $bloc = $em->getRepository('CmsBlocBundle:Bloc')->find((int)$request->request->get('pk'));
        $bloc->setContentHtml($request->request->get('value'));
        $em->flush();

        return new JsonResponse(array(true));
    }

}
