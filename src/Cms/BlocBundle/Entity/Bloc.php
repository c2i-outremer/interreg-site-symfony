<?php

namespace Cms\BlocBundle\Entity;

use Cms\ControllerBundle\Entity\Controller;
use Cms\ControllerBundle\Entity\Query;
use Cms\CoreBundle\Entity\Traits\Nameable;
use Cms\CoreBundle\Entity\Traits\Positionable;
use Cms\CoreBundle\Entity\Traits\PublishTranslatable;
use Cms\CoreBundle\Entity\Traits\Timestampable;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * bloc
 *
 * @ORM\Table("cms_bloc_bloc")
 * @ORM\Entity(repositoryClass="Cms\BlocBundle\Entity\BlocRepository")
 * @Gedmo\TranslationEntity(class="Cms\BlocBundle\Entity\BlocTranslation")
 */
class Bloc extends AbstractPersonalTranslatable implements TranslatableInterface
{

    use Timestampable;
    use Nameable;
    use PublishTranslatable;
    use Positionable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

    /**
     * @ORM\OneToMany(targetEntity="Cms\BlocBundle\Entity\BlocTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @var boolean
     * @ORM\Column(name="allow_edit_front", type="boolean", nullable=true)
     */
    private $allowEditFront;


    /**
	 * @var string
	 *
	 * @ORM\Column(name="css", type="text", nullable=true)
	 */
	private $css;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="script", type="text", nullable=true)
	 */
	private $script;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="identifier", type="text", nullable=true)
	 */
	private $identifier;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="class", type="text", nullable=true)
	 */
	private $class;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="col_xs", type="text", nullable=true)
	 */
	private $colXs;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="col_sm", type="text", nullable=true)
	 */
	private $colSm;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="col_md", type="text", nullable=true)
	 */
	private $colMd;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="col_lg", type="text", nullable=true)
	 */
	private $colLg;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="offset_xs", type="text", nullable=true)
	 */
	private $offsetXs;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="offset_sm", type="text", nullable=true)
	 */
	private $offsetSm;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="offset_md", type="text", nullable=true)
	 */
	private $offsetMd;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="offset_lg", type="text", nullable=true)
	 */
	private $offsetLg;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="content_text", type="text", nullable=true)
	 */
	private $contentText;

	/**
	 * @var string
	 * @Gedmo\Translatable
	 * @ORM\Column(name="content_html", type="text", nullable=true)
	 */
	private $contentHtml;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\ControllerBundle\Entity\Controller")
	 * @ORM\JoinColumn(name="controller", referencedColumnName="id")
	 * */
	private $controller;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\ControllerBundle\Entity\Query")
	 * @ORM\JoinColumn(name="query", referencedColumnName="id")
	 * */
	private $query;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\GroupBundle\Entity\Group", inversedBy="blocs")
	 * @ORM\JoinColumn(name="group_container", referencedColumnName="id")
	 * */
	private $group;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\NavBundle\Entity\Menu", inversedBy="blocs")
	 * @ORM\JoinColumn(name="menu", referencedColumnName="id")
	 * */
	private $menu;


	/**
	 * @ORM\ManyToOne(targetEntity="Cms\BlocBundle\Entity\BlocCategory", inversedBy="blocs")
	 * @ORM\JoinColumn(name="bloc_category", referencedColumnName="id")
	 * */
	private $blocCategory;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\CollectionBundle\Entity\Collection")
	 * @ORM\JoinColumn(name="collection", referencedColumnName="id")
	 * */
	private $collection;

	/**
	 * @ORM\ManyToMany(targetEntity="Cms\BlogBundle\Entity\Category")
	 * @ORM\JoinTable(name="cms_bloc_blog_categories",
	 *      joinColumns={@ORM\JoinColumn(name="bloc", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="category", referencedColumnName="id")}
	 *      )
	 * */
	private $blogCategorys;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\BlogBundle\Entity\BlogType")
	 * @ORM\JoinColumn(name="blog_type", referencedColumnName="id")
	 * */
	private $blogType;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\PageBundle\Entity\PageBloc", mappedBy="bloc")
	 * */
	private $pageBlocs;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\GroupBundle\Entity\Item", mappedBy="bloc")
	 * */
	private $items;

	function __construct()
	{
		$this->pageBlocs = new \Doctrine\Common\Collections\ArrayCollection();
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->blogCategorys = new \Doctrine\Common\Collections\ArrayCollection();
		$this->translations = new \Doctrine\Common\Collections\ArrayCollection();
		$this->allowEditFront = true;
	}

	public function __toString()
	{
		$r = '';
		if ($this->getBlocCategory())
		{
			$r .= $this->getBlocCategory()->getName() . ' - ';
		}
		$r .= $this->getName() . ' - ' . $this->getId();
		return $r;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set css
	 *
	 * @param string $css
	 * @return Bloc
	 */
	public function setCss($css)
	{
		$this->css = $css;

		return $this;
	}

	/**
	 * Get css
	 *
	 * @return string 
	 */
	public function getCss()
	{
		return $this->css;
	}

	/**
	 * Set script
	 *
	 * @param string $script
	 * @return Bloc
	 */
	public function setScript($script)
	{
		$this->script = $script;

		return $this;
	}

	/**
	 * Get script
	 *
	 * @return string 
	 */
	public function getScript()
	{
		return $this->script;
	}

	/**
	 * Set contentText
	 *
	 * @param string $contentText
	 * @return Bloc
	 */
	public function setContentText($contentText)
	{
		$this->contentText = $contentText;

		return $this;
	}

	/**
	 * Get contentText
	 *
	 * @return string 
	 */
	public function getContentText()
	{
		return $this->contentText;
	}

	/**
	 * Set contentHtml
	 *
	 * @param string $contentHtml
	 * @return Bloc
	 */
	public function setContentHtml($contentHtml)
	{
		$this->contentHtml = $contentHtml;

		return $this;
	}

	/**
	 * Get contentHtml
	 *
	 * @return string 
	 */
	public function getContentHtml()
	{
		return $this->contentHtml;
	}

	/**
	 * Set controller
	 *
	 * @param Controller $controller
	 * @return Bloc
	 */
	public function setController(Controller $controller = null)
	{
		$this->controller = $controller;

		return $this;
	}

	/**
	 * Get controller
	 *
	 * @return Controller
	 */
	public function getController()
	{
		return $this->controller;
	}

	/**
	 * Set query
	 *
	 * @param Query $query
	 * @return Bloc
	 */
	public function setQuery(Query $query = null)
	{
		$this->query = $query;

		return $this;
	}

	/**
	 * Get query
	 *
	 * @return Query
	 */
	public function getQuery()
	{
		return $this->query;
	}

	/**
	 * Set group
	 *
	 * @param \Cms\GroupBundle\Entity\Group $group
	 * @return Bloc
	 */
	public function setGroup(\Cms\GroupBundle\Entity\Group $group = null)
	{
		$this->group = $group;

		return $this;
	}

	/**
	 * Get group
	 *
	 * @return \Cms\GroupBundle\Entity\Group 
	 */
	public function getGroup()
	{
		return $this->group;
	}

	/**
	 * Set blocCategory
	 *
	 * @param \Cms\BlocBundle\Entity\BlocCategory $blocCategory
	 * @return Bloc
	 */
	public function setBlocCategory(\Cms\BlocBundle\Entity\BlocCategory $blocCategory = null)
	{
		$this->blocCategory = $blocCategory;

		return $this;
	}

	/**
	 * Get blocCategory
	 *
	 * @return \Cms\BlocBundle\Entity\BlocCategory 
	 */
	public function getBlocCategory()
	{
		return $this->blocCategory;
	}

	/**
	 * Add pageBlocs
	 *
	 * @param \Cms\PageBundle\Entity\PageBloc $pageBlocs
	 * @return Bloc
	 */
	public function addPageBloc(\Cms\PageBundle\Entity\PageBloc $pageBlocs)
	{
		$this->pageBlocs[] = $pageBlocs;
		$pageBlocs->setBloc($this);
		return $this;
	}

	/**
	 * Remove pageBlocs
	 *
	 * @param \Cms\PageBundle\Entity\PageBloc $pageBlocs
	 */
	public function removePageBloc(\Cms\PageBundle\Entity\PageBloc $pageBlocs)
	{
		$this->pageBlocs->removeElement($pageBlocs);
	}

	/**
	 * Get pageBlocs
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getPageBlocs()
	{
		return $this->pageBlocs;
	}

	/**
	 * Set menu
	 *
	 * @param \Cms\NavBundle\Entity\Menu $menu
	 * @return Bloc
	 */
	public function setMenu(\Cms\NavBundle\Entity\Menu $menu = null)
	{
		$this->menu = $menu;

		return $this;
	}

	/**
	 * Get menu
	 *
	 * @return \Cms\NavBundle\Entity\Menu 
	 */
	public function getMenu()
	{
		return $this->menu;
	}

	/**
	 * Set identifier
	 *
	 * @param string $identifier
	 * @return Bloc
	 */
	public function setIdentifier($identifier)
	{
		$this->identifier = $identifier;

		return $this;
	}

	/**
	 * Get identifier
	 *
	 * @return string 
	 */
	public function getIdentifier()
	{
		return $this->identifier;
	}

	/**
	 * Set class
	 *
	 * @param string $class
	 * @return Bloc
	 */
	public function setClass($class)
	{
		$this->class = $class;

		return $this;
	}

	/**
	 * Get class
	 *
	 * @return string 
	 */
	public function getClass()
	{
		return $this->class;
	}

	/**
	 * Add items
	 *
	 * @param \Cms\GroupBundle\Entity\Item $items
	 * @return Bloc
	 */
	public function addItem(\Cms\GroupBundle\Entity\Item $items)
	{
		$this->items[] = $items;
		$items->setBloc($this);
		return $this;
	}

	/**
	 * Remove items
	 *
	 * @param \Cms\GroupBundle\Entity\Item $items
	 */
	public function removeItem(\Cms\GroupBundle\Entity\Item $items)
	{
		$this->items->removeElement($items);
	}

	/**
	 * Get items
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * Set colXs
	 *
	 * @param string $colXs
	 * @return Bloc
	 */
	public function setColXs($colXs)
	{
		$this->colXs = $colXs;

		return $this;
	}

	/**
	 * Get colXs
	 *
	 * @return string 
	 */
	public function getColXs()
	{
		return $this->colXs;
	}

	/**
	 * Set colSm
	 *
	 * @param string $colSm
	 * @return Bloc
	 */
	public function setColSm($colSm)
	{
		$this->colSm = $colSm;

		return $this;
	}

	/**
	 * Get colSm
	 *
	 * @return string 
	 */
	public function getColSm()
	{
		return $this->colSm;
	}

	/**
	 * Set colMd
	 *
	 * @param string $colMd
	 * @return Bloc
	 */
	public function setColMd($colMd)
	{
		$this->colMd = $colMd;

		return $this;
	}

	/**
	 * Get colMd
	 *
	 * @return string 
	 */
	public function getColMd()
	{
		return $this->colMd;
	}

	/**
	 * Set colLg
	 *
	 * @param string $colLg
	 * @return Bloc
	 */
	public function setColLg($colLg)
	{
		$this->colLg = $colLg;

		return $this;
	}

	/**
	 * Get colLg
	 *
	 * @return string 
	 */
	public function getColLg()
	{
		return $this->colLg;
	}

	/**
	 * Set offsetXs
	 *
	 * @param string $offsetXs
	 * @return Bloc
	 */
	public function setOffsetXs($offsetXs)
	{
		$this->offsetXs = $offsetXs;

		return $this;
	}

	/**
	 * Get offsetXs
	 *
	 * @return string 
	 */
	public function getOffsetXs()
	{
		return $this->offsetXs;
	}

	/**
	 * Set offsetSm
	 *
	 * @param string $offsetSm
	 * @return Bloc
	 */
	public function setOffsetSm($offsetSm)
	{
		$this->offsetSm = $offsetSm;

		return $this;
	}

	/**
	 * Get offsetSm
	 *
	 * @return string 
	 */
	public function getOffsetSm()
	{
		return $this->offsetSm;
	}

	/**
	 * Set offsetMd
	 *
	 * @param string $offsetMd
	 * @return Bloc
	 */
	public function setOffsetMd($offsetMd)
	{
		$this->offsetMd = $offsetMd;

		return $this;
	}

	/**
	 * Get offsetMd
	 *
	 * @return string 
	 */
	public function getOffsetMd()
	{
		return $this->offsetMd;
	}

	/**
	 * Set offsetLg
	 *
	 * @param string $offsetLg
	 * @return Bloc
	 */
	public function setOffsetLg($offsetLg)
	{
		$this->offsetLg = $offsetLg;

		return $this;
	}

	/**
	 * Get offsetLg
	 *
	 * @return string 
	 */
	public function getOffsetLg()
	{
		return $this->offsetLg;
	}


	/**
	 * Set blogType
	 *
	 * @param \Cms\BlogBundle\Entity\BlogType $blogType
	 * @return Bloc
	 */
	public function setBlogType(\Cms\BlogBundle\Entity\BlogType $blogType = null)
	{
		$this->blogType = $blogType;

		return $this;
	}

	/**
	 * Get blogType
	 *
	 * @return \Cms\BlogBundle\Entity\BlogType 
	 */
	public function getBlogType()
	{
		return $this->blogType;
	}

	/**
	 * Add blogCategorys
	 *
	 * @param \Cms\BlogBundle\Entity\Category $blogCategorys
	 * @return Bloc
	 */
	public function addBlogCategory(\Cms\BlogBundle\Entity\Category $blogCategorys)
	{
		$this->blogCategorys[] = $blogCategorys;
		return $this;
	}

	/**
	 * Remove blogCategorys
	 *
	 * @param \Cms\BlogBundle\Entity\Category $blogCategorys
	 */
	public function removeBlogCategory(\Cms\BlogBundle\Entity\Category $blogCategorys)
	{
		$this->blogCategorys->removeElement($blogCategorys);
	}

	/**
	 * Get blogCategorys
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getBlogCategorys()
	{
		return $this->blogCategorys;
	}


    /**
     * Set collection
     *
     * @param \Cms\CollectionBundle\Entity\Collection $collection
     *
     * @return Bloc
     */
    public function setCollection(\Cms\CollectionBundle\Entity\Collection $collection = null)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get collection
     *
     * @return \Cms\CollectionBundle\Entity\Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    public function __clone()
    {
        $this->setCreated(null);
        $this->setUpdated(null);
        $this->setIdentifier(null);
        $this->setPosition(null);
        $this->setSlug(null);
    }

    /**
     * Set allowEditFront
     *
     * @param boolean $allowEditFront
     *
     * @return Bloc
     */
    public function setAllowEditFront($allowEditFront)
    {
        $this->allowEditFront = $allowEditFront;

        return $this;
    }

    /**
     * Get allowEditFront
     *
     * @return boolean
     */
    public function getAllowEditFront()
    {
        return $this->allowEditFront;
    }

}
