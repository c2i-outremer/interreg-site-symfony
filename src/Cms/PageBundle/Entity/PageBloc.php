<?php

namespace Cms\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * pageBloc
 *
 * @ORM\Table("cms_page_page_bloc")
 * @ORM\Entity(repositoryClass="Cms\PageBundle\Entity\PageBlocRepository")
 */
class PageBloc
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="position", type="integer")
	 */
	private $position;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\PageBundle\Entity\Page", inversedBy="pageBlocs",cascade={"persist"})
	 * @ORM\JoinColumn(name="page", referencedColumnName="id", nullable=false)
	 * */
	private $page;

	/**
	 * @ORM\ManyToOne(targetEntity="Cms\BlocBundle\Entity\Bloc", inversedBy="pageBlocs",cascade={"persist"})
	 * @ORM\JoinColumn(name="bloc", referencedColumnName="id", nullable=false)
	 * */
	private $bloc;
	
	public function __toString()
	{
		return \md5($this->id);
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set position
	 *
	 * @param integer $position
	 * @return PageBloc
	 */
	public function setPosition($position)
	{
		$this->position = $position;

		return $this;
	}

	/**
	 * Get position
	 *
	 * @return integer 
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * Set page
	 *
	 * @param \Cms\PageBundle\Entity\Page $page
	 * @return PageBloc
	 */
	public function setPage(\Cms\PageBundle\Entity\Page $page)
	{
		$this->page = $page;

		return $this;
	}

	/**
	 * Get page
	 *
	 * @return \Cms\PageBundle\Entity\Page 
	 */
	public function getPage()
	{
		return $this->page;
	}

	/**
	 * Set bloc
	 *
	 * @param \Cms\BlocBundle\Entity\Bloc $bloc
	 * @return PageBloc
	 */
	public function setBloc(\Cms\BlocBundle\Entity\Bloc $bloc)
	{
		$this->bloc = $bloc;

		return $this;
	}

	/**
	 * Get bloc
	 *
	 * @return \Cms\BlocBundle\Entity\Bloc 
	 */
	public function getBloc()
	{
		return $this->bloc;
	}

}
