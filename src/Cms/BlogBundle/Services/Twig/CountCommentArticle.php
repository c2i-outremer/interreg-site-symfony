<?php

namespace Cms\BlogBundle\Services\Twig;

use Doctrine\ORM\EntityManager;


class CountCommentArticle  extends \Twig_Extension
{

    public $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function getName()
    {
        return 'CountCommentArticle';
    }

    public function getFunctions()
    {
        return array(
            'CountCommentArticle' => new \Twig_Function_Method($this, 'CountCommentArticle'),
        );
    }

    public function CountCommentArticle($thread)
    {
        return count($this->manager->getRepository('CmsCommentBundle:Comment')->findBy(array('thread' => $thread)));
    }

}