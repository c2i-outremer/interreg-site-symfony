# Digitom CMS

----------------------------------------------------------------------

Système conçu avec le framework Symfony afin de faciliter les taches récurrentes de gestion de contenu (Menu, Page, Bloc, Média, Utilisateurs)

----------------------------------------------------------------------

## Prérequis technique et ressources utiles

* [Symfony book](http://symfony.com/fr/doc/current/book/index.html)
* [Symfony cookbook](http://symfony.com/fr/doc/current/cookbook/index.html)
* [Sonata Admin Bundle](http://sonata-project.org/bundles/admin/master/doc/index.html)
* [Sonata Media Bundle](http://sonata-project.org/bundles/user/2-2/doc/index.html)
* [Sonata User Bundle](http://sonata-project.org/bundles/user/master/doc/index.html)
* [Jquery](http://api.jquery.com/)
* [Jquery UI](http://jqueryui.com/)
* [Bootstrap](http://getbootstrap.com/)

----------------------------------------------------------------------

## Installation

* Créer un nouveau repository 
* Dans un invite de commande depuis le dossier cible 
* si le serveur a une limit de memoire (dans la plupart des cas), les commandes php doivent etre remplacées par `php -d memory_limit=2G`
* Penser à désactiver les extrabundle en fonction du type de site pour éviter la création de tables inutiles.

```
#!sh
git flow init
git remote add origin git@adresse-nouveau-repo.git
git remote add digitom git@bitbucket.org:c2i-outremer/tools-cms-symfony.git
git pull digitom develop
composer install
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
mkdir -pv web/uploads/media
mkdir -pv web/uploads/xml
chmod -R 777 web/uploads/
php app/console doctrine:schema:update --force
php app/console doctrine:fixtures:load 
php app/console assets:install --symlink
php app/console assetic:dump
```

  
* Afin de profiter de la gestion des médias, il est conseillé de créer un [virtualhost](http://doc.ubuntu-fr.org/tutoriel/virtualhosts_avec_apache2) pointant sur le dossier web du projet 

----------------------------------------------------------------------
 
## Après l'installation
 
* **Front-office** : http://mon-virtual-host.local/app_dev.php
* **Back-office** : http://mon-virtual-host.local/app_dev.php/admin/dashboard (login/pass : admin/admin => à changer en fin de développement)
* Changer le fuseau horaire par defaut dans app/appKernel.php
* Regler les parametres de my_parameters.yml
* Parameter un compte mailJet```