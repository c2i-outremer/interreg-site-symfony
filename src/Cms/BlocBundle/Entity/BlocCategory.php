<?php

namespace Cms\BlocBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Doctrine\ORM\Mapping as ORM;

/**
 * blocCategory
 *
 * @ORM\Table("cms_bloc_bloc_category")
 * @ORM\Entity(repositoryClass="Cms\BlocBundle\Entity\BlocCategoryRepository")
 */
class BlocCategory
{


    use Nameable;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\OneToMany(targetEntity="Cms\BlocBundle\Entity\Bloc", mappedBy="blocCategory")
	 * */
	private $blocs;
	
	function __toString()
	{
		return $this->getName();
	}

	function __construct()
	{
		$this->blocs = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Add blocs
	 *
	 * @param \Cms\BlocBundle\Entity\Bloc $blocs
	 * @return BlocCategory
	 */
	public function addBloc(\Cms\BlocBundle\Entity\Bloc $blocs)
	{
		$this->blocs[] = $blocs;
		$blocs->setBlocCategory($this);
		return $this;
	}

	/**
	 * Remove blocs
	 *
	 * @param \Cms\BlocBundle\Entity\Bloc $blocs
	 */
	public function removeBloc(\Cms\BlocBundle\Entity\Bloc $blocs)
	{
		$this->blocs->removeElement($blocs);
	}

	/**
	 * Get blocs
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getBlocs()
	{
		return $this->blocs;
	}

}
