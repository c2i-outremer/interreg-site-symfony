<?php

namespace Cms\ControllerBundle\Entity;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Doctrine\ORM\Mapping as ORM;

/**
 * controller
 *
 * @ORM\Table("cms_controller_controller")
 * @ORM\Entity(repositoryClass="Cms\ControllerBundle\Entity\ControllerRepository")
 */
class Controller
{

    use Nameable;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="path", type="string", length=255)
	 */
	private $path;


	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->blocs = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set path
	 *
	 * @param string $path
	 * @return Controller
	 */
	public function setPath($path)
	{
		$this->path = $path;

		return $this;
	}

	/**
	 * Get path
	 *
	 * @return string 
	 */
	public function getPath()
	{
		return $this->path;
	}


}
