<?php

namespace Cms\BlogBundle\Controller;

use Cms\BlogBundle\Entity\Article;
use Cms\BlogBundle\Entity\Inscription;
use Cms\BlogBundle\Form\InscriptionType;
use Cms\CoreBundle\Entity\Visite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleController extends Controller
{

    public function showAction(Request $request, $articleLink, $_locale, $form = null)
    {

        if (!in_array($_locale, $this->container->getParameter('locales'))) {
            throw new NotFoundHttpException($this->get('translator')->trans('core.not_found.langage'));
        }

        $request->setLocale($_locale);

        $folderSlug = $this->get('cms_page.GetPageData')->getFolderSlug($articleLink);

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('CmsBlogBundle:Article')->findOneArticle($folderSlug, $_locale);

        if (!$article or $article->getSlug() != $folderSlug['slug']) {
            throw new NotFoundHttpException($this->get('translator')->trans('blog.not_found.post').' : '.$articleLink);
        }

        $sameCategories = $em->getRepository('CmsBlogBundle:Article')->findArticle(array($article->getCategory()), null, 0, 4);
        $categories = $em->getRepository('CmsBlogBundle:Category')->findAll();

        // update visite
        $article->setNumberView($article->getNumberView() + 1);
        $visite = new Visite('article', $article->getSlug(), $article->getId());
        $em->persist($visite);
        $em->flush();


        return $this->render(
            $article->getArticleType()->getTemplatePath(),
            array(
                'threadComments' => $this->get('cms_comment.services.comment_tools')->getThreadComment('article-'.$article->getSlug()),
                'localSwitcherArticle' => true,
                '_locale' => $_locale,
                'article' => $article,
                'sameCategories' => $sameCategories,
                'categories' => $categories,
                'form' => $form,
            )
        );
    }

    /**
     * @param Request $request
     * @param $_locale
     * @param Article $article
     * @param Form $form
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function inscriptionAction(Request $request, $_locale, $article)
    {

        if ($request->isMethod('POST')) {
            $article = $this->getDoctrine()->getRepository('CmsBlogBundle:Article')->findOneById($article);
        }

        $inscription = new Inscription();
        $inscription->setArticle($article);

        $form = $this->get('form.factory')->create(new InscriptionType($article), $inscription);

        $form->handleRequest($request);

        if ($form->isValid()) {

            // email
            $config = $this->get('cms_config.GetConfig')->getConfig();
            $em = $this->getDoctrine()->getManager();

            $em->persist($inscription);
            $em->flush();

            if ($config->getMailStatus()) {

                $users = $em->getRepository('ApplicationSonataUserBundle:User')->getByGroup('Contact');

                $emails = array();
                foreach ($users as $user) {
                    $emails[] = $user->getEmail();
                }


                $message = \Swift_Message::newInstance()
                    ->setSubject('Demande d\'inscription: '.$inscription->getArticle()->getName())
                    ->setFrom(array($config->getMailSender() => $config->getMailName()))
                    ->setTo($emails)
                    ->setBody($this->renderView(':Blog/Single/Default:send_email.html.twig', array('inscription' => $inscription)), 'text/html');

                $this->get('mailer')->send($message);
            }

            $this->get('session')->getFlashBag()->add('inscriptionSuccess', $this->get('translator')->trans('blog.form.flash_message_success'));

            return $this->redirect($this->generateUrl('blog_article_show', array('articleLink' => $article->getArticleLink(), '_locale' => $_locale)));
        } //Form non valide
        else {

            if ($request->isMethod('POST')) {

                $this->get('session')->getFlashBag()->add('inscriptionFail', $this->get('translator')->trans('blog.form.flash_message_fail'));
                return $this->redirect($this->generateUrl('blog_article_show', array('articleLink' => $article->getArticleLink(), '_locale' => $_locale)));

            } elseif ($request->isMethod('GET')) {

                return
                    $this->render(
                        ':Blog/Single/Default:inscription_form.html.twig',
                        array(
                            'form' => $form->createView(),
                            'articleId' => $article->getId(),
                        )
                    );

            } else {

                return new Response('Error', 500);

            }
        }
    }

}
