<?php

namespace AppBundle\Entity\Project;

use Cms\CoreBundle\Entity\Traits\Nameable;
use Cms\CoreBundle\Entity\Traits\PublishTranslatable;
use Cms\CoreBundle\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;


/**
 * Project
 *
 * @ORM\Table("app_project_project")
 * @ORM\Entity
 */
class Project extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use Nameable;
    use Timestampable;
    use PublishTranslatable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project\Axe", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     * */
    protected $axe;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="cover", referencedColumnName="id", nullable=true)
     */
    protected $cover;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project\Attribute", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="app_project_project_attribute",
     *      joinColumns={@ORM\JoinColumn(name="project", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="attribute", referencedColumnName="id")}
     *      )
     * */
    private $projectAttributes;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project\Attribute", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="app_project_project_content",
     *      joinColumns={@ORM\JoinColumn(name="project", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="content", referencedColumnName="id")}
     *      )
     * */
    private $projectContents;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->projectAttributes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->projectContents = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cover
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $cover
     *
     * @return Project
     */
    public function setCover(\Application\Sonata\MediaBundle\Entity\Media $cover = null)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set axe
     *
     * @param \AppBundle\Entity\Project\Axe $axe
     *
     * @return Project
     */
    public function setAxe(\AppBundle\Entity\Project\Axe $axe = null)
    {
        $this->axe = $axe;

        return $this;
    }

    /**
     * Get axe
     *
     * @return \AppBundle\Entity\Project\Axe
     */
    public function getAxe()
    {
        return $this->axe;
    }

    /**
     * Add projectAttribute
     *
     * @param \AppBundle\Entity\Project\Attribute $projectAttribute
     *
     * @return Project
     */
    public function addProjectAttribute(\AppBundle\Entity\Project\Attribute $projectAttribute)
    {
        $this->projectAttributes[] = $projectAttribute;

        return $this;
    }

    /**
     * Remove projectAttribute
     *
     * @param \AppBundle\Entity\Project\Attribute $projectAttribute
     */
    public function removeProjectAttribute(\AppBundle\Entity\Project\Attribute $projectAttribute)
    {
        $this->projectAttributes->removeElement($projectAttribute);
    }

    /**
     * Get projectAttributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectAttributes()
    {
        return $this->projectAttributes;
    }

    /**
     * Add projectContent
     *
     * @param \AppBundle\Entity\Project\Attribute $projectContent
     *
     * @return Project
     */
    public function addProjectContent(\AppBundle\Entity\Project\Attribute $projectContent)
    {
        $this->projectContents[] = $projectContent;

        return $this;
    }

    /**
     * Remove projectContent
     *
     * @param \AppBundle\Entity\Project\Attribute $projectContent
     */
    public function removeProjectContent(\AppBundle\Entity\Project\Attribute $projectContent)
    {
        $this->projectContents->removeElement($projectContent);
    }

    /**
     * Get projectContents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectContents()
    {
        return $this->projectContents;
    }
}
